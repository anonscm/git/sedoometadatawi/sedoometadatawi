package fr.sedoo.metadata.server.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.commons.server.DefaultServerApplication;
import fr.sedoo.metadata.client.service.FormatService;
import fr.sedoo.metadata.server.service.dao.FormatDAO;
import fr.sedoo.metadata.server.service.tool.FormatDTOTools;
import fr.sedoo.metadata.shared.domain.dto.FormatDTO;
import fr.sedoo.metadata.shared.domain.entity.Format;

public class FormatServiceImpl extends RemoteServiceServlet implements FormatService {

	FormatDAO dao = null;

	public FormatServiceImpl() {
		dao = (FormatDAO) DefaultServerApplication.getSpringBeanFactory().getBeanByName(FormatDAO.BEAN_NAME);
	}

	@Override
	public ArrayList<FormatDTO> findAll() {
		List<Format> findAll = dao.findAll();
		ArrayList<FormatDTO> result = new ArrayList<FormatDTO>();
		Iterator<Format> iterator = findAll.iterator();
		while (iterator.hasNext()) {
			Format format = (Format) iterator.next();
			result.add(FormatDTOTools.toDTO(format));
		}
		return result;
	}

	@Override
	public void delete(Long id) throws Exception {
		dao.delete(id);
	}

	@Override
	public Long save(FormatDTO dto) throws Exception {
		Format format = FormatDTOTools.fromDTO(dto);
		return dao.save(format);
	}
}
