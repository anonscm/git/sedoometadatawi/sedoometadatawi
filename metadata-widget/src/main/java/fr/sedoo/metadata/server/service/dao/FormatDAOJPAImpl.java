package fr.sedoo.metadata.server.service.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.sedoo.metadata.shared.domain.entity.Format;

@Repository
public class FormatDAOJPAImpl implements FormatDAO {

	protected EntityManager em;

	public EntityManager getEntityManager() {
		return em;
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.em = entityManager;
	}

	@Transactional
	public void delete(Long id) throws Exception {

		Format site = getEntityManager().find(Format.class, id);
		if (site != null) {
			getEntityManager().remove(site);
		}
	}

	@Transactional
	public Long save(Format format) {
		if (format.getId() == null) {
			getEntityManager().persist(format);
			return format.getId();
		} else {
			getEntityManager().merge(format);
			return format.getId();
		}
	}

	@Override
	public List<Format> findAll() {
		List<Format> resultList = em.createQuery("select format from Format format").getResultList();
		return resultList;
	}

}
