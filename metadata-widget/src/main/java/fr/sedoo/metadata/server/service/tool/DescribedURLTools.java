package fr.sedoo.metadata.server.service.tool;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.metadata.utils.domain.DescribedURL;
import fr.sedoo.metadata.shared.domain.misc.IdentifiedDescribedString;

public class DescribedURLTools {
	private DescribedURLTools() {

	}

	public static List<DescribedURL> toDescribedURLList(List<IdentifiedDescribedString> resourceURL) {
		List<DescribedURL> result = new ArrayList<DescribedURL>();
		if (resourceURL != null) {
			Iterator<IdentifiedDescribedString> iterator = resourceURL.iterator();
			while (iterator.hasNext()) {
				IdentifiedDescribedString current = iterator.next();
				DescribedURL aux = new DescribedURL();
				aux.setLabel(current.getDescription());
				aux.setLink(current.getValue());
				result.add(aux);
			}
		}
		return result;
	}

	public static List<IdentifiedDescribedString> fromDescribedURLList(List<DescribedURL> resourceURL) {
		List<IdentifiedDescribedString> result = new ArrayList<IdentifiedDescribedString>();
		if (resourceURL != null) {
			Iterator<DescribedURL> iterator = resourceURL.iterator();
			Long id = 0L;
			while (iterator.hasNext()) {
				DescribedURL current = iterator.next();
				IdentifiedDescribedString aux = new IdentifiedDescribedString();
				aux.setDescription(StringUtil.trimToEmpty(current.getLabel()));
				aux.setValue(current.getLink());
				aux.setId(id);
				id++;
				result.add(aux);
			}
		}
		return result;
	}

}
