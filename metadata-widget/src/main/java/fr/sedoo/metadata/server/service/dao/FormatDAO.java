package fr.sedoo.metadata.server.service.dao;

import java.util.List;

import fr.sedoo.metadata.shared.domain.entity.Format;

public interface FormatDAO {

	static final String BEAN_NAME = "formatDAO";

	Long save(Format format) throws Exception;

	void delete(Long id) throws Exception;

	List<Format> findAll();
}
