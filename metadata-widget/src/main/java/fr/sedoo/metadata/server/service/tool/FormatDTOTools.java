package fr.sedoo.metadata.server.service.tool;

import fr.sedoo.metadata.shared.domain.dto.FormatDTO;
import fr.sedoo.metadata.shared.domain.entity.Format;

public class FormatDTOTools {

	public static FormatDTO toDTO(Format format) {
		FormatDTO dto = new FormatDTO();
		dto.setId(format.getId());
		dto.setName(format.getName());
		dto.setStringVersions(format.getVersions());
		return dto;
	}

	public static Format fromDTO(FormatDTO dto) {
		Format format = new Format();
		format.setId(dto.getId());
		format.setName(dto.getName());
		format.setVersions(dto.getStringVersions());
		return format;
	}
}
