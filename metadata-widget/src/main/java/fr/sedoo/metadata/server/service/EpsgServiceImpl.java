package fr.sedoo.metadata.server.service;

import java.util.ArrayList;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.commons.server.DefaultServerApplication;
import fr.sedoo.metadata.client.service.EpsgService;
import fr.sedoo.metadata.server.service.dao.EpsgDAO;
import fr.sedoo.metadata.shared.domain.entity.EpsgCode;

public class EpsgServiceImpl extends RemoteServiceServlet implements EpsgService {

	EpsgDAO dao = null;

	@Override
	public ArrayList<EpsgCode> findAll() throws ServiceException {
		initDao();
		try {
			return dao.findAll();
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}

	private void initDao() {
		if (dao == null) {
			dao = (EpsgDAO) DefaultServerApplication.getSpringBeanFactory().getBeanByName(EpsgDAO.BEAN_NAME);
		}
	}

}
