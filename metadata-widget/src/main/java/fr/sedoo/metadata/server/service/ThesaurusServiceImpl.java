package fr.sedoo.metadata.server.service;

import java.util.ArrayList;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.commons.server.DefaultServerApplication;
import fr.sedoo.metadata.client.service.ThesaurusService;
import fr.sedoo.metadata.server.service.thesaurus.ThesauriFactory;
import fr.sedoo.metadata.shared.domain.thesaurus.Thesaurus;
import fr.sedoo.metadata.shared.domain.thesaurus.ThesaurusItem;

public class ThesaurusServiceImpl extends RemoteServiceServlet implements ThesaurusService {

	ThesauriFactory factory = null;

	public ThesaurusServiceImpl() {
		factory = (ThesauriFactory) DefaultServerApplication.getSpringBeanFactory().getBeanByName(ThesauriFactory.BEAN_NAME);
	}

	@Override
	public ArrayList<ThesaurusItem> getItems(String thesaurusId, String language) throws ServiceException {
		return factory.getThesaurusItems(thesaurusId, language);
	}

	@Override
	public ArrayList<Thesaurus> getThesaurus(String language) throws ServiceException {
		return factory.getThesauri(language);
	}

}
