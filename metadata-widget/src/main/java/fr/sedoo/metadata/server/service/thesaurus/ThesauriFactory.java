package fr.sedoo.metadata.server.service.thesaurus;

import java.util.ArrayList;

import fr.sedoo.metadata.shared.domain.thesaurus.Thesaurus;
import fr.sedoo.metadata.shared.domain.thesaurus.ThesaurusItem;

public interface ThesauriFactory {

	String BEAN_NAME = "thesauriFactory";

	public ArrayList<Thesaurus> getThesauri(String language);

	public ArrayList<ThesaurusItem> getThesaurusItems(String id, String language);

}
