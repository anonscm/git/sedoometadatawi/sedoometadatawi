package fr.sedoo.metadata.server.service.dao;

import java.util.ArrayList;

import fr.sedoo.metadata.shared.domain.entity.EpsgCode;

public interface EpsgDAO {

	static final String BEAN_NAME = "epsgDAO";

	ArrayList<EpsgCode> findAll() throws Exception;

}
