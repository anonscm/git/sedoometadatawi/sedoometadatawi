package fr.sedoo.metadata.shared.domain.thesaurus;

import com.google.gwt.user.client.rpc.IsSerializable;

public abstract class DefaultThesaurus implements IsSerializable {

	private String shortLabel;
	private String id;
	private String url;
	

	public void setShortLabel(String shortLabel) {
		this.shortLabel = shortLabel;
	}
	
	public String getShortLabel() {
		return shortLabel;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
