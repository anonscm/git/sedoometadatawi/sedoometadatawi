package fr.sedoo.metadata.shared.domain.dto;

import java.util.ArrayList;
import java.util.List;

import fr.sedoo.commons.client.util.SeparatorUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.util.ValidationUtil;
import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.AbstractDTO;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.client.message.MetadataMessage;

public class PersonDTO extends AbstractDTO implements HasIdentifier {

	private String organisationName;
	private String personName;
	private String email;
	private String roles;
	private String address;
	private String zipCode;
	private String city;
	private String country;
	private String uuid;

	public final static String ROLE_SEPARATOR = SeparatorUtil.AROBAS_SEPARATOR;

	public String getOrganisationName() {
		return organisationName;
	}

	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	@Override
	public String getHash() {
		return "@" + StringUtil.trimToEmpty(getPersonName()) + "|" + StringUtil.trimToEmpty(getRoles()) + "|" + StringUtil.trimToEmpty(getOrganisationName()) + "|"
				+ StringUtil.trimToEmpty(getEmail());
	}

	@Override
	public List<ValidationAlert> validate() {
		List<ValidationAlert> result = new ArrayList<ValidationAlert>();
		String aux = StringUtil.trimToEmpty(getEmail());
		if (ValidationUtil.isValidEmail(aux) == false) {
			result.add(new ValidationAlert(MetadataMessage.INSTANCE.personEmail(), MetadataMessage.INSTANCE.emailData()));
		}
		return result;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCompleteAdress() {
		StringBuffer sb = new StringBuffer();
		if (StringUtil.isEmpty(address) == false) {
			sb.append(address.trim() + "|");
		}
		if (StringUtil.isEmpty(zipCode) == false) {
			sb.append(zipCode.trim() + " ");
		}
		if (StringUtil.isEmpty(city) == false) {
			sb.append(city.trim() + "|");
		}
		if (StringUtil.isEmpty(country) == false) {
			sb.append(country);
		}
		return sb.toString().trim();
	}

	@Override
	public String getIdentifier() {
		return getUuid();
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public void addRole(String value) {
		if (StringUtil.isNotEmpty(value)) {
			roles = roles + ROLE_SEPARATOR + StringUtil.trimToEmpty(value);
		}
	}

	public boolean containRole(String value) {
		String aux = StringUtil.trimToEmpty(getRoles());
		if (aux.toLowerCase().indexOf(value.toLowerCase()) >= 0) {
			return true;
		} else {
			return false;
		}
	}

	public static void clone(PersonDTO src, PersonDTO tgt) {
		tgt.setUuid(src.getUuid());
		tgt.setAddress(src.getAddress());
		tgt.setCity(src.getCity());
		tgt.setCountry(src.getCity());
		tgt.setEmail(src.getEmail());
		tgt.setOrganisationName(src.getOrganisationName());
		tgt.setPersonName(src.getPersonName());
		tgt.setRoles(src.getRoles());
		tgt.setZipCode(src.getZipCode());
	}
}
