package fr.sedoo.metadata.shared.domain.entity;

import com.google.gwt.user.client.rpc.IsSerializable;

public class EpsgCode implements Comparable<EpsgCode>, IsSerializable {

	private Integer code;
	private String label;

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public int compareTo(EpsgCode other) {

		if (other == null) {
			return 1;
		}

		// ascending order
		return this.code - other.code;
	}
}
