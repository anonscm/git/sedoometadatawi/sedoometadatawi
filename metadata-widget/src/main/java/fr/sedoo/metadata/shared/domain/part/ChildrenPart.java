package fr.sedoo.metadata.shared.domain.part;

import java.util.ArrayList;
import java.util.List;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;

public class ChildrenPart extends AbstractPart {

	private ArrayList<MetadataSummaryDTO> children = new ArrayList<MetadataSummaryDTO>();

	@Override
	public String getTabName() {
		return "";
	}

	@Override
	public String getHash() {

		return "";
	}

	@Override
	public List<ValidationAlert> validate() {
		List<ValidationAlert> alerts = new ArrayList<ValidationAlert>();
		return alerts;
	}

	public ArrayList<MetadataSummaryDTO> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<MetadataSummaryDTO> children) {
		this.children = children;
	}

}
