package fr.sedoo.metadata.shared.domain.thesaurus;

import com.google.gwt.user.client.rpc.IsSerializable;

public interface Thesaurus extends IsSerializable {

	String getShortLabel();
	String getId();
}
