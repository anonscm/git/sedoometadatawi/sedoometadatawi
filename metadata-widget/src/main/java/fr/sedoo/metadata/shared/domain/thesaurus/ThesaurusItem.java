package fr.sedoo.metadata.shared.domain.thesaurus;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.HasIdentifier;

public class ThesaurusItem implements IsSerializable, HasIdentifier, Comparable<ThesaurusItem> {

	private String id;
	private String value;

	public ThesaurusItem() {
	}

	public ThesaurusItem(String id, String value) {
		setId(id);
		setValue(value);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int compareTo(ThesaurusItem o) {
		if (o == null) {
			return 1;
		} else {
			return StringUtil.trimToEmpty(getValue()).compareTo(StringUtil.trimToEmpty(o.getValue()));
		}
	}

	@Override
	public String getIdentifier() {
		return getId();
	}

	@Override
	public List<ValidationAlert> validate() {
		return new ArrayList<ValidationAlert>();
	}

}
