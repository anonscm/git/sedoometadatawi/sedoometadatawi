package fr.sedoo.metadata.shared.domain.part;

import java.util.ArrayList;
import java.util.List;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.metadata.client.message.MetadataMessage;

public class MetadataPart extends AbstractPart {

	@Override
	public String getHash() {
		StringBuffer aux = new StringBuffer();
		// aux.append("@" );
		// Iterator<MetadataContactDTO> iterator = metadataContacts.iterator();
		// while (iterator.hasNext())
		// {
		// aux.append(iterator.next().getHash());
		// }
		return aux.toString();
	}

	@Override
	public List<ValidationAlert> validate() {
		List<ValidationAlert> result = new ArrayList<ValidationAlert>();
		// if (getMetadataContacts().isEmpty())
		// {
		// result.add(new
		// ValidationAlert(Message.INSTANCE.metadataContactList(),
		// Message.INSTANCE.atLeastOneElementNeeded()));
		// } else
		// {
		// Iterator<MetadataContactDTO> iterator =
		// getMetadataContacts().iterator();
		// result.addAll(iterator.next().validate());
		// }
		return result;
	}

	@Override
	public String getTabName() {
		return MetadataMessage.INSTANCE.metadataEditingMetadataTabHeader();
	}

}
