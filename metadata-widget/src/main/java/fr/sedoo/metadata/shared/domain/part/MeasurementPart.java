package fr.sedoo.metadata.shared.domain.part;

import java.util.ArrayList;
import java.util.List;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.shared.domain.dto.I18nString;

public class MeasurementPart extends AbstractPart {

	private I18nString resourceGenealogy = new I18nString();

	@Override
	public String getHash() {
		StringBuffer aux = new StringBuffer();
		aux.append("@" + getResourceGenealogy().getHash() + "|");
		return aux.toString();
	}

	@Override
	public List<ValidationAlert> validate() {
		List<ValidationAlert> result = new ArrayList<ValidationAlert>();
		return result;
	}

	@Override
	public String getTabName() {
		return MetadataMessage.INSTANCE.metadataEditingMeasurementTabHeader();
	}

	public I18nString getResourceGenealogy() {
		return resourceGenealogy;
	}

	public void setResourceGenealogy(I18nString resourceGenealogy) {
		this.resourceGenealogy = resourceGenealogy;
	}

}
