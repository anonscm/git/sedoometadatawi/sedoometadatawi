package fr.sedoo.metadata.shared.domain.dto;

import java.util.Comparator;

public class AlphabeticalComparator implements Comparator<String> {

	@Override
	public int compare(String s1, String s2) {
		if (s1 == null) {
			return +1;
		} else {
			return s1.compareToIgnoreCase(s2);
		}
	}

}
