package fr.sedoo.metadata.shared.domain.part;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.metadata.client.message.MetadataMessage;

public class KeywordPart extends AbstractPart {

	private List<String> keywords = new ArrayList<String>();

	@Override
	public String getTabName() {
		return MetadataMessage.INSTANCE.metadataEditingKeywordTabHeader();
	}

	@Override
	public String getHash() {
		StringBuffer sb = new StringBuffer();
		if (keywords != null) {
			Iterator<String> iterator = keywords.iterator();
			while (iterator.hasNext()) {
				sb.append(iterator.next() + "|");
			}
		}
		return sb.toString();
	}

	@Override
	public List<ValidationAlert> validate() {
		List<ValidationAlert> alerts = new ArrayList<ValidationAlert>();
		return alerts;
	}

	public List<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}

}
