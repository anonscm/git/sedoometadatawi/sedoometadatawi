package fr.sedoo.metadata.shared.domain.part;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.sedoo.commons.client.util.SeparatorUtil;
import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.GeographicBoundingBoxDTO;
import fr.sedoo.metadata.client.message.MetadataMessage;

public class GeographicalLocationPart extends AbstractPart {

	private ArrayList<GeographicBoundingBoxDTO> boxes = new ArrayList<GeographicBoundingBoxDTO>();

	@Override
	public String getHash() {
		StringBuilder sb = new StringBuilder(SeparatorUtil.AROBAS_SEPARATOR);
		Iterator<GeographicBoundingBoxDTO> iterator = getBoxes().iterator();
		while (iterator.hasNext()) {
			GeographicBoundingBoxDTO current = iterator.next();
			sb.append(current.getHash() + SeparatorUtil.AROBAS_SEPARATOR);
		}
		return sb.toString();
	}

	@Override
	public List<ValidationAlert> validate() {
		List<ValidationAlert> result = new ArrayList<ValidationAlert>();
		return result;
	}

	public ArrayList<GeographicBoundingBoxDTO> getBoxes() {
		return boxes;
	}

	public void setBoxes(ArrayList<GeographicBoundingBoxDTO> boxes) {
		this.boxes = boxes;
	}

	@Override
	public String getTabName() {
		return MetadataMessage.INSTANCE.metadataEditingGeographicalLocationTabHeader();
	}

}
