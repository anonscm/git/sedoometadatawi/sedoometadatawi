package fr.sedoo.metadata.shared.domain.misc;

import java.util.ArrayList;
import java.util.List;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.IsUrl;
import fr.sedoo.metadata.client.message.MetadataMessage;

public class IdentifiedDescribedString extends IdentifiedString implements IsUrl {

	public IdentifiedDescribedString() {
		super();
	}

	public IdentifiedDescribedString(Long id, String value, String description) {
		super();
		setId(id);
		setValue(value);
		setDescription(description);
	}

	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<ValidationAlert> validate() {
		ArrayList<ValidationAlert> result = new ArrayList<ValidationAlert>();
		if ((getValue() == null) || (getValue().trim().length() == 0)) {
			result.add(new ValidationAlert(MetadataMessage.INSTANCE.metadataEditingSnapshotTableURLHeader(), MetadataMessage.INSTANCE.mandatoryData()));
		}
		return result;
	}

	@Override
	public String getLink() {
		return getValue();
	}

	@Override
	public String getLabel() {
		return getDescription();
	}
}
