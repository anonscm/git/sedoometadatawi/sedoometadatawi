package fr.sedoo.metadata.shared.domain.misc;

import java.util.HashMap;
import java.util.Map;

import fr.sedoo.commons.client.message.LanguageMessages;
import fr.sedoo.commons.metadata.shared.utils.Iso6392LanguageConverter;

public class Iso6392LanguageProvider extends Iso6392LanguageConverter {

	static Map<String, String> labels = new HashMap<String, String>();
	static {
		labels.put(ENGLISH, LanguageMessages.INSTANCE.english());
		labels.put(FRENCH, LanguageMessages.INSTANCE.french());
		labels.put(PORTUGUESE, LanguageMessages.INSTANCE.portuguese());
		labels.put(SPANISH, LanguageMessages.INSTANCE.spanish());
	}

	public static String getDisplayNameFromCode(String key) {
		if (key == null) {
			return "";
		} else {
			String aux = labels.get(key);
			if (aux == null) {
				return "";
			} else {
				return aux;
			}
		}
	}

}
