package fr.sedoo.metadata.shared.domain.part;

import java.util.ArrayList;
import java.util.List;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.shared.domain.dto.I18nString;

public class ConstraintPart extends AbstractPart {

	private I18nString useConditions = new I18nString();
	private I18nString publicAccessLimitations = new I18nString();

	@Override
	public String getHash() {
		StringBuffer aux = new StringBuffer();
		aux.append("@" + getUseConditions().getHash() + "|" + getPublicAccessLimitations().getHash() + "|");
		return aux.toString();
	}

	@Override
	public List<ValidationAlert> validate() {
		List<ValidationAlert> result = new ArrayList<ValidationAlert>();
		if (getUseConditions().isEmpty()) {
			result.add(new ValidationAlert(MetadataMessage.INSTANCE.metadataEditingUseConditions(), MetadataMessage.INSTANCE.mandatoryData()));

		}
		if (getPublicAccessLimitations().isEmpty()) {
			result.add(new ValidationAlert(MetadataMessage.INSTANCE.metadataEditingPublicAccessLimitations(), MetadataMessage.INSTANCE.mandatoryData()));

		}
		return result;
	}

	@Override
	public String getTabName() {
		return MetadataMessage.INSTANCE.metadataEditingConstraintTabHeader();
	}

	public I18nString getUseConditions() {
		return useConditions;
	}

	public void setUseConditions(I18nString useConditions) {
		this.useConditions = useConditions;
	}

	public I18nString getPublicAccessLimitations() {
		return publicAccessLimitations;
	}

	public void setPublicAccessLimitations(I18nString publicAccessLimitations) {
		this.publicAccessLimitations = publicAccessLimitations;
	}

}
