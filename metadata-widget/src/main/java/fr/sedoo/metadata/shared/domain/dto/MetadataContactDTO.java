package fr.sedoo.metadata.shared.domain.dto;

import java.util.List;

import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.metadata.client.message.MetadataMessage;

public class MetadataContactDTO extends PersonDTO {
	public List<ValidationAlert> validate() {
		List<ValidationAlert> result = super.validate();

		return addMetadataContactvalidation(this, result);
	}

	public static MetadataContactDTO clone(MetadataContactDTO src) {
		MetadataContactDTO clone = new MetadataContactDTO();
		PersonDTO.clone(src, clone);
		return clone;
	}

	public static List<ValidationAlert> addMetadataContactvalidation(PersonDTO person, List<ValidationAlert> alerts) {
		if (StringUtil.isEmpty(person.getEmail())) {
			alerts.add(new ValidationAlert(MetadataMessage.INSTANCE.personEmail(), MetadataMessage.INSTANCE.mandatoryData()));
		}
		if (StringUtil.isEmpty(person.getOrganisationName())) {
			alerts.add(new ValidationAlert(MetadataMessage.INSTANCE.personOrganisationName(), MetadataMessage.INSTANCE.mandatoryData()));
		}
		if (StringUtil.isEmpty(person.getRoles())) {
			alerts.add(new ValidationAlert(MetadataMessage.INSTANCE.personRoles(), MetadataMessage.INSTANCE.atLeastOneElementNeeded()));
		}

		return alerts;
	}
}
