package fr.sedoo.metadata.shared.domain.part;

import fr.sedoo.commons.shared.domain.AbstractDTO;

public abstract class AbstractPart extends AbstractDTO {

	public abstract String getTabName();

}
