package fr.sedoo.metadata.shared.domain;

import java.util.ArrayList;
import java.util.List;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.AbstractDTO;
import fr.sedoo.metadata.shared.domain.part.ChildrenPart;
import fr.sedoo.metadata.shared.domain.part.ConstraintPart;
import fr.sedoo.metadata.shared.domain.part.ContactPart;
import fr.sedoo.metadata.shared.domain.part.GeographicalLocationPart;
import fr.sedoo.metadata.shared.domain.part.IdentificationPart;
import fr.sedoo.metadata.shared.domain.part.KeywordPart;
import fr.sedoo.metadata.shared.domain.part.MeasurementPart;
import fr.sedoo.metadata.shared.domain.part.MetadataPart;
import fr.sedoo.metadata.shared.domain.part.OtherPart;
import fr.sedoo.metadata.shared.domain.part.TemporalExtentPart;

public class MetadataDTO extends AbstractDTO {

	private MetadataPart metadataPart = new MetadataPart();
	private IdentificationPart identificationPart = new IdentificationPart();
	private MeasurementPart measurementPart = new MeasurementPart();
	private KeywordPart keywordPart = new KeywordPart();
	private ConstraintPart constraintPart = new ConstraintPart();
	private TemporalExtentPart temporalExtentPart = new TemporalExtentPart();
	private GeographicalLocationPart geographicalLocationPart = new GeographicalLocationPart();
	private OtherPart otherPart = new OtherPart();
	private ContactPart contactPart = new ContactPart();
	private ChildrenPart childrenPart = new ChildrenPart();

	@Override
	public String getHash() {
		return "@" + getIdentificationPart().getHash() + "|" + getKeywordPart().getHash() + "|" + getMetadataPart().getHash() + "|" + getConstraintPart().getHash() + "|"
				+ getTemporalExtentPart().getHash() + "|" + getGeographicalLocationPart().getHash() + "|" + getOtherPart().getHash() + "|" + getContactPart().getHash();
	}

	@Override
	public List<ValidationAlert> validate() {
		List<ValidationAlert> result = new ArrayList<ValidationAlert>();

		result.addAll(ValidationAlert.prefixField(getIdentificationPart().getTabName() + " - ", getIdentificationPart().validate()));
		result.addAll(ValidationAlert.prefixField(getKeywordPart().getTabName() + " - ", getKeywordPart().validate()));
		result.addAll(ValidationAlert.prefixField(getGeographicalLocationPart().getTabName() + " - ", getGeographicalLocationPart().validate()));
		result.addAll(ValidationAlert.prefixField(getTemporalExtentPart().getTabName() + " - ", getTemporalExtentPart().validate()));
		result.addAll(ValidationAlert.prefixField(getConstraintPart().getTabName() + " - ", getConstraintPart().validate()));
		result.addAll(ValidationAlert.prefixField(getMetadataPart().getTabName() + " - ", getMetadataPart().validate()));
		result.addAll(ValidationAlert.prefixField(getOtherPart().getTabName() + " - ", getOtherPart().validate()));
		result.addAll(ValidationAlert.prefixField(getContactPart().getTabName() + " - ", getContactPart().validate()));
		result.addAll(ValidationAlert.prefixField(getMeasurementPart().getTabName() + " - ", getMeasurementPart().validate()));
		return result;
	}

	public MetadataPart getMetadataPart() {
		return metadataPart;
	}

	public void setMetadataPart(MetadataPart part) {
		this.metadataPart = part;
	}

	public IdentificationPart getIdentificationPart() {
		return identificationPart;
	}

	public void setIdentificationPart(IdentificationPart identificationPart) {
		this.identificationPart = identificationPart;
	}

	public ConstraintPart getConstraintPart() {
		return constraintPart;
	}

	public void setConstraintPart(ConstraintPart constraintPart) {
		this.constraintPart = constraintPart;
	}

	public TemporalExtentPart getTemporalExtentPart() {
		return temporalExtentPart;
	}

	public void setTemporalExtentPart(TemporalExtentPart temporalExtentPart) {
		this.temporalExtentPart = temporalExtentPart;
	}

	public GeographicalLocationPart getGeographicalLocationPart() {
		return geographicalLocationPart;
	}

	public void setGeographicalLocationPart(GeographicalLocationPart geographicalLocationPart) {
		this.geographicalLocationPart = geographicalLocationPart;
	}

	public OtherPart getOtherPart() {
		return otherPart;
	}

	public void setOtherPart(OtherPart otherPart) {
		this.otherPart = otherPart;
	}

	public KeywordPart getKeywordPart() {
		return keywordPart;
	}

	public void setKeywordPart(KeywordPart keywordPart) {
		this.keywordPart = keywordPart;
	}

	public ContactPart getContactPart() {
		return contactPart;
	}

	public void setContactPart(ContactPart contactPart) {
		this.contactPart = contactPart;
	}

	public MeasurementPart getMeasurementPart() {
		return measurementPart;
	}

	public void setMeasurementPart(MeasurementPart measurementPart) {
		this.measurementPart = measurementPart;
	}

	public ChildrenPart getChildrenPart() {
		return childrenPart;
	}

	public void setChildrenPart(ChildrenPart childrenPart) {
		this.childrenPart = childrenPart;
	}

}
