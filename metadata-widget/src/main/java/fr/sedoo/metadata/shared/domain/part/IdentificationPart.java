package fr.sedoo.metadata.shared.domain.part;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.shared.domain.dto.I18nString;
import fr.sedoo.metadata.shared.domain.dto.IdentifiedResourceIdentifier;
import fr.sedoo.metadata.shared.domain.dto.InternetLink;
import fr.sedoo.metadata.shared.domain.misc.IdentifiedDescribedString;

public class IdentificationPart extends AbstractPart {

	private I18nString resourceTitle = new I18nString();
	private I18nString resourceAlternateTitle = new I18nString();
	private I18nString resourceAbstract = new I18nString();
	private I18nString purpose = new I18nString();
	private String resourceStatus;
	private String hierarchyLevel;
	private String hierarchyLevelName;
	private List<InternetLink> resourceURL = new ArrayList<InternetLink>();
	private List<IdentifiedResourceIdentifier> resourceIdentifiers = new ArrayList<IdentifiedResourceIdentifier>();
	private List<IdentifiedDescribedString> snapshots = new ArrayList<IdentifiedDescribedString>();

	@Override
	public String getHash() {
		StringBuffer aux = new StringBuffer();
		aux.append("@" + getResourceAbstract().getHash() + "|" + getResourceTitle().getHash() + "|" + getResourceAlternateTitle().getHash() + "|" + getPurpose().getHash() + "|" + "|"
				+ getResourceStatus());

		Iterator<InternetLink> urlIterator = getResourceURL().iterator();
		while (urlIterator.hasNext()) {
			InternetLink current = urlIterator.next();
			aux.append(current.getValue() + "|" + current.getDescription() + "|" + current.getProtocol() + "|");
		}

		Iterator<IdentifiedResourceIdentifier> identifierIterator = getResourceIdentifiers().iterator();
		while (identifierIterator.hasNext()) {
			IdentifiedResourceIdentifier current = identifierIterator.next();
			aux.append(current.getIdentifier() + "|" + current.getNameSpace() + "|");
		}

		Iterator<IdentifiedDescribedString> snapshotIterator = getSnapshots().iterator();
		while (snapshotIterator.hasNext()) {
			IdentifiedDescribedString current = snapshotIterator.next();
			aux.append(current.getValue() + "|" + current.getDescription() + "|");
		}

		return aux.toString();
	}

	@Override
	public List<ValidationAlert> validate() {
		List<ValidationAlert> result = new ArrayList<ValidationAlert>();
		if (getResourceTitle().isEmpty()) {
			result.add(new ValidationAlert(MetadataMessage.INSTANCE.metadataEditingResourceTitle(), MetadataMessage.INSTANCE.mandatoryData()));

		}
		if (getResourceURL().isEmpty()) {
			result.add(new ValidationAlert(MetadataMessage.INSTANCE.metadataEditingResourceURL(), MetadataMessage.INSTANCE.atLeastOneElementNeeded()));
		} else {
			Iterator<InternetLink> iterator = getResourceURL().iterator();
			result.addAll(iterator.next().validate());
		}

		if (getResourceIdentifiers().isEmpty()) {
			result.add(new ValidationAlert(MetadataMessage.INSTANCE.metadataEditingResourceIdentifier(), MetadataMessage.INSTANCE.atLeastOneElementNeeded()));
		}

		if (getSnapshots().isEmpty() == false) {
			Iterator<IdentifiedDescribedString> iterator = getSnapshots().iterator();
			result.addAll(iterator.next().validate());
		}

		return result;
	}

	public List<InternetLink> getResourceURL() {
		return resourceURL;
	}

	public void setResourceURL(List<InternetLink> resourceURL) {
		this.resourceURL = resourceURL;
	}

	public List<IdentifiedDescribedString> getSnapshots() {
		return snapshots;
	}

	public void setSnapshots(List<IdentifiedDescribedString> snapshots) {
		this.snapshots = snapshots;
	}

	public String getResourceStatus() {
		return resourceStatus;
	}

	public void setResourceStatus(String resourceStatus) {
		this.resourceStatus = resourceStatus;
	}

	@Override
	public String getTabName() {
		return MetadataMessage.INSTANCE.metadataEditingIdentificationTabHeader();
	}

	public I18nString getResourceTitle() {
		return resourceTitle;
	}

	public void setResourceTitle(I18nString resourceTitle) {
		this.resourceTitle = resourceTitle;
	}

	public I18nString getResourceAbstract() {
		return resourceAbstract;
	}

	public void setResourceAbstract(I18nString resourceAbstract) {
		this.resourceAbstract = resourceAbstract;
	}

	public List<IdentifiedResourceIdentifier> getResourceIdentifiers() {
		return resourceIdentifiers;
	}

	public void setResourceIdentifiers(List<IdentifiedResourceIdentifier> resourceIdentifiers) {
		this.resourceIdentifiers = resourceIdentifiers;
	}

	public I18nString getPurpose() {
		return purpose;
	}

	public void setPurpose(I18nString purpose) {
		this.purpose = purpose;
	}

	public I18nString getResourceAlternateTitle() {
		return resourceAlternateTitle;
	}

	public void setResourceAlternateTitle(I18nString resourceAlternateTitle) {
		this.resourceAlternateTitle = resourceAlternateTitle;
	}

	public String getHierarchyLevelName() {
		return hierarchyLevelName;
	}

	public void setHierarchyLevelName(String hierarchyLevelName) {
		this.hierarchyLevelName = hierarchyLevelName;
	}

	public String getHierarchyLevel() {
		return hierarchyLevel;
	}

	public void setHierarchyLevel(String hierarchyLevel) {
		this.hierarchyLevel = hierarchyLevel;
	}

}
