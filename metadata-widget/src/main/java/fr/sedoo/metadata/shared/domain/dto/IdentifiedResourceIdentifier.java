package fr.sedoo.metadata.shared.domain.dto;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

import fr.sedoo.commons.client.util.SeparatorUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.metadata.shared.DefaultResourceIdentifier;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.client.message.MetadataMessage;

public class IdentifiedResourceIdentifier extends DefaultResourceIdentifier implements HasIdentifier, IsSerializable {

	public IdentifiedResourceIdentifier() {
	}

	public IdentifiedResourceIdentifier(String code, String nameSpace) {
		setCode(code);
		setNameSpace(nameSpace);
	}

	@Override
	public List<ValidationAlert> validate() {
		ArrayList<ValidationAlert> result = new ArrayList<ValidationAlert>();
		if (StringUtil.isEmpty(getCode())) {
			result.add(new ValidationAlert(MetadataMessage.INSTANCE.metadataEditingResourceIdentifier(), MetadataMessage.INSTANCE.mandatoryData()));
		}
		return result;
	}

	@Override
	public String getIdentifier() {
		return StringUtil.trimToEmpty(getCode()) + SeparatorUtil.AROBAS_SEPARATOR + StringUtil.trimToEmpty(getNameSpace());
	}

}
