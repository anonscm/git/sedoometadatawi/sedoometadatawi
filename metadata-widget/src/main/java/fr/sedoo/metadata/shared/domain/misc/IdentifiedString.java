package fr.sedoo.metadata.shared.domain.misc;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.HasIdentifier;

public class IdentifiedString implements IsSerializable, HasIdentifier {

	protected Long id;
	protected String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String getIdentifier() {
		return "" + getId();
	}

	@Override
	public List<ValidationAlert> validate() {
		return new ArrayList<ValidationAlert>();
	}

}
