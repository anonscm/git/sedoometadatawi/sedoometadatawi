package fr.sedoo.metadata.shared.domain.part;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.metadata.shared.constant.Iso19139Constants;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.util.Iso19115DateUtil;

public class TemporalExtentPart extends AbstractPart {

	private String startDate;
	private String endDate;
	private String creationDate;
	private String lastRevisionDate;
	private String publicationDate;
	private String updateRythm;

	@Override
	public String getHash() {
		StringBuffer aux = new StringBuffer();
		aux.append("@" + getUpdateRythm() + "|" + StringUtil.trimToEmpty(getStartDate()) + "|" + StringUtil.trimToEmpty(getEndDate()) + "|" + StringUtil.trimToEmpty(getCreationDate()) + "|"
				+ StringUtil.trimToEmpty(getLastRevisionDate()) + "|" + StringUtil.trimToEmpty(getPublicationDate()));
		return aux.toString();
	}

	@Override
	public List<ValidationAlert> validate() {
		List<ValidationAlert> result = new ArrayList<ValidationAlert>();
		Date start = null;
		Date end = null;
		if (StringUtil.isEmpty(startDate)) {
			result.add(new ValidationAlert(MetadataMessage.INSTANCE.metadataEditingStartDate(), MetadataMessage.INSTANCE.mandatoryData()));
		} else {
			try {
				start = Iso19115DateUtil.getDateFormater().parse(startDate);
			} catch (IllegalArgumentException e) {
				result.add(new ValidationAlert(MetadataMessage.INSTANCE.metadataEditingStartDate(), MetadataMessage.INSTANCE.dateData()));
			}
		}

		if (StringUtil.isEmpty(endDate)) {
			result.add(new ValidationAlert(MetadataMessage.INSTANCE.metadataEditingEndDate(), MetadataMessage.INSTANCE.mandatoryData()));

		} else {
			if (endDate.compareTo(Iso19139Constants.NOW) != 0) {
				try {
					end = Iso19115DateUtil.getDateFormater().parse(endDate);
				} catch (IllegalArgumentException e) {
					result.add(new ValidationAlert(MetadataMessage.INSTANCE.metadataEditingEndDate(), MetadataMessage.INSTANCE.dateData()));
				}
			}
		}

		if ((start != null) && (end != null)) {
			if (Iso19115DateUtil.after(start, end) == false) {
				result.add(new ValidationAlert(MetadataMessage.INSTANCE.metadataEditingStartDate().replace(":", "") + "-" + MetadataMessage.INSTANCE.metadataEditingEndDate(), MetadataMessage.INSTANCE
						.dateUnconsistency()));
			}
		}

		if (StringUtil.isEmpty(getCreationDate())) {
			result.add(new ValidationAlert(MetadataMessage.INSTANCE.metadataEditingCreationDate(), MetadataMessage.INSTANCE.mandatoryData()));
		} else {
			try {
				Iso19115DateUtil.getDateFormater().parse(getCreationDate());
			} catch (IllegalArgumentException e) {
				result.add(new ValidationAlert(MetadataMessage.INSTANCE.metadataEditingCreationDate(), MetadataMessage.INSTANCE.dateData()));
			}
		}

		if (!(StringUtil.isEmpty(getLastRevisionDate()))) {
			try {
				Iso19115DateUtil.getDateFormater().parse(getLastRevisionDate());
			} catch (IllegalArgumentException e) {
				result.add(new ValidationAlert(MetadataMessage.INSTANCE.metadataEditingLastRevisionDate(), MetadataMessage.INSTANCE.dateData()));
			}
		}

		if (!(StringUtil.isEmpty(getPublicationDate()))) {
			try {
				Iso19115DateUtil.getDateFormater().parse(getPublicationDate());
			} catch (IllegalArgumentException e) {
				result.add(new ValidationAlert(MetadataMessage.INSTANCE.metadataEditingPublicationDate(), MetadataMessage.INSTANCE.dateData()));
			}
		}
		return result;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	@Override
	public String getTabName() {
		return MetadataMessage.INSTANCE.metadataEditingTemporalExtentHeader();
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getLastRevisionDate() {
		return lastRevisionDate;
	}

	public void setLastRevisionDate(String lastRevisionDate) {
		this.lastRevisionDate = lastRevisionDate;
	}

	public String getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(String publicationDate) {
		this.publicationDate = publicationDate;
	}

	public String getUpdateRythm() {
		return updateRythm;
	}

	public void setUpdateRythm(String updateRythm) {
		this.updateRythm = updateRythm;
	}
}
