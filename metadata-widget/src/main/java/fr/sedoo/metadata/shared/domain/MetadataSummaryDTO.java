package fr.sedoo.metadata.shared.domain;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.HasIdentifier;

public class MetadataSummaryDTO implements HasIdentifier, IsSerializable {

	private static final int MAX_ABSTRACT_LENGTH = 1000;

	private String uuid;

	private String name;

	private String displayAbstract;

	private String displayTitle;

	public MetadataSummaryDTO() {

	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDisplayAbstract(String displayAbstract) {
		this.displayAbstract = displayAbstract;
	}

	public String getName() {
		return name;
	}

	public String getDisplayAbstract() {
		String aux = StringUtil.trimToEmpty(displayAbstract);
		if (aux.length() > MAX_ABSTRACT_LENGTH) {
			return aux.substring(0, MAX_ABSTRACT_LENGTH) + "...";
		} else {
			return aux;
		}
	}

	public String getUuid() {
		return uuid;
	}

	@Override
	public String getIdentifier() {
		return getUuid();
	}

	@Override
	public List<ValidationAlert> validate() {
		return new ArrayList<ValidationAlert>();
	}

	public String getDisplayTitle() {
		return StringUtil.trimToEmpty(displayTitle);
	}

	public void setDisplayTitle(String displayTitle) {
		this.displayTitle = displayTitle;
	}

}
