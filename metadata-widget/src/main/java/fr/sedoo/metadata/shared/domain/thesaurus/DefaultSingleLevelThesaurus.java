package fr.sedoo.metadata.shared.domain.thesaurus;

import java.util.ArrayList;

public class DefaultSingleLevelThesaurus extends DefaultThesaurus implements SingleLevelThesaurus{

	ArrayList<ThesaurusItem> keywords;
	
	@Override
	public ArrayList<ThesaurusItem> getKeywords() {
		return keywords;
	}
	
	@Override
	public void setKeywords(ArrayList<ThesaurusItem> keywords) {
		this.keywords = keywords;
		
	}
}
