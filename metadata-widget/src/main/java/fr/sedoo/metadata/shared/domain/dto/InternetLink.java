package fr.sedoo.metadata.shared.domain.dto;

import fr.sedoo.commons.metadata.shared.ResourceLink;
import fr.sedoo.metadata.shared.domain.misc.IdentifiedDescribedString;

public class InternetLink extends IdentifiedDescribedString implements ResourceLink {

	private String protocol;

	public InternetLink() {
		super();
	}

	public InternetLink(Long id, String link, String label, String protocol) {
		super(id, link, label);
		this.protocol = protocol;

	}

	/**
	 * En cas d'absence de protocol, le LINK_PROTOCOL est utilisé
	 * 
	 * @param id
	 * @param link
	 * @param label
	 */
	public InternetLink(long id, String link, String label) {
		super(id, link, label);
		this.protocol = LINK_PROTOCOL;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

}
