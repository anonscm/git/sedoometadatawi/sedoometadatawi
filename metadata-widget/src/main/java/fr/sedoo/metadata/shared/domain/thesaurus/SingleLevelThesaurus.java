package fr.sedoo.metadata.shared.domain.thesaurus;

import java.util.ArrayList;


public interface SingleLevelThesaurus extends Thesaurus {
	
	ArrayList<ThesaurusItem> getKeywords();
	void setKeywords(ArrayList<ThesaurusItem> keywords);
}
