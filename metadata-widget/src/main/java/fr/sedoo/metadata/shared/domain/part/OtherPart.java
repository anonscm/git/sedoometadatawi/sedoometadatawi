package fr.sedoo.metadata.shared.domain.part;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.shared.domain.dto.FormatDTO;
import fr.sedoo.metadata.shared.domain.misc.Iso6392LanguageProvider;

public class OtherPart extends AbstractPart {

	private String charset;
	private String coordinateSystem;
	private FormatDTO format;
	private List<String> resourceLanguages = new ArrayList<String>();
	private String metadataLastModificationDate;
	private String metadataLanguage;
	private String uuid = "";

	@Override
	public String getHash() {
		StringBuffer aux = new StringBuffer();
		aux.append("@" + "|" + getCharset() + "|" + StringUtil.trimToEmpty(getResourceLanguageCodesAsString()) + metadataLanguage + "|" + metadataLastModificationDate + "|");
		if (format != null) {
			aux.append(format.getHash());
		}
		return aux.toString();
	}

	@Override
	public List<ValidationAlert> validate() {
		List<ValidationAlert> result = new ArrayList<ValidationAlert>();

		if (format != null) {
			result.addAll(format.validate());
		}

		return result;
	}

	public String getCharset() {
		return StringUtil.trimToEmpty(charset);
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public String getCoordinateSystem() {
		return coordinateSystem;
	}

	public void setCoordinateSystem(String coordinateSystem) {
		this.coordinateSystem = coordinateSystem;
	}

	public List<String> getResourceLanguages() {
		return resourceLanguages;
	}

	public void setResourceLanguages(List<String> resourceLanguages) {
		this.resourceLanguages = resourceLanguages;
	}

	public String getResourceLanguageCodesAsString() {
		StringBuffer sb = new StringBuffer();
		if (resourceLanguages != null) {
			Iterator<String> iterator = resourceLanguages.iterator();
			while (iterator.hasNext()) {
				sb.append(iterator.next());
				if (iterator.hasNext()) {
					sb.append("-");
				}
			}
		}
		return sb.toString();
	}

	public String getResourceLanguageLabelsAsString() {
		StringBuffer sb = new StringBuffer("");
		if (resourceLanguages != null) {
			Iterator<String> iterator = resourceLanguages.iterator();
			while (iterator.hasNext()) {
				sb.append(Iso6392LanguageProvider.getDisplayNameFromCode(iterator.next()));
				if (iterator.hasNext()) {
					sb.append(" - ");
				}
			}
		}
		return sb.toString();
	}

	public FormatDTO getFormat() {
		return format;
	}

	public void setFormat(FormatDTO format) {
		this.format = format;
	}

	@Override
	public String getTabName() {
		return MetadataMessage.INSTANCE.metadataEditingOtherTabHeader();
	}

	public String getMetadataLanguage() {
		return metadataLanguage;
	}

	public void setMetadataLanguage(String metadataLanguage) {
		this.metadataLanguage = metadataLanguage;
	}

	public String getMetadataLastModificationDate() {
		return metadataLastModificationDate;
	}

	public void setMetadataLastModificationDate(String metadataLastModificationDate) {
		this.metadataLastModificationDate = metadataLastModificationDate;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
