package fr.sedoo.metadata.shared.domain.part;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.shared.domain.dto.MetadataContactDTO;

public class ContactPart extends AbstractPart {

	private List<MetadataContactDTO> metadataContacts = new ArrayList<MetadataContactDTO>();
	private List<MetadataContactDTO> resourceContacts = new ArrayList<MetadataContactDTO>();

	@Override
	public String getTabName() {
		return MetadataMessage.INSTANCE.metadataEditingContactTabHeader();
	}

	@Override
	public String getHash() {

		// TODO A faire

		/*
		 * 
		 * Iterator<MetadataContactDTO> contactIterator =
		 * getResourceContacts().iterator(); while (contactIterator.hasNext()) {
		 * aux.append(contactIterator.next().getHash()); }
		 */
		return "";
	}

	@Override
	public List<ValidationAlert> validate() {
		List<ValidationAlert> alerts = new ArrayList<ValidationAlert>();

		// if (getResourceContacts().isEmpty()) {
		// alerts.add(new
		// ValidationAlert(MetadataMessage.INSTANCE.resourceContactList(),
		// ValidationMessages.INSTANCE.atLeastOneElementNeeded()));
		// } else {
		// Iterator<MetadataContactDTO> iterator =
		// getResourceContacts().iterator();
		// alerts.addAll(iterator.next().validate());
		// }
		//
		// if (getMetadataPointOfContacts().isEmpty()) {
		// alerts.add(new ValidationAlert(MetadataMessage.INSTANCE.me,
		// ValidationMessages.INSTANCE.atLeastOneElementNeeded()));
		// } else {
		// Iterator<MetadataContactDTO> iterator =
		// getResourceContacts().iterator();
		// alerts.addAll(iterator.next().validate());
		// }

		return alerts;
	}

	public List<MetadataContactDTO> getMetadataContacts() {
		return metadataContacts;
	}

	public void setMetadataContacts(List<MetadataContactDTO> metadataPointOfContacts) {
		this.metadataContacts = metadataPointOfContacts;
	}

	public List<MetadataContactDTO> getResourceContacts() {
		return resourceContacts;
	}

	public void setResourceContacts(List<MetadataContactDTO> resourceContacts) {
		this.resourceContacts = resourceContacts;
	}

	public void addResourceContacts(List<MetadataContactDTO> contacts) {
		resourceContacts.addAll(contacts);
	}

	public void addMetadataContacts(List<MetadataContactDTO> contacts) {
		metadataContacts.addAll(contacts);
	}

	public List<? extends HasIdentifier> getMetadataContactsByRole(String singleRole) {
		return filterListByRole(getMetadataContacts(), singleRole);
	}

	public List<? extends HasIdentifier> getResourceContactsByRole(String singleRole) {
		return filterListByRole(getResourceContacts(), singleRole);
	}

	protected List<? extends HasIdentifier> filterListByRole(List<? extends HasIdentifier> list, String role) {
		ArrayList<MetadataContactDTO> result = new ArrayList<MetadataContactDTO>();
		Iterator<? extends HasIdentifier> iterator = list.iterator();
		while (iterator.hasNext()) {
			MetadataContactDTO metadataContactDTO = (MetadataContactDTO) iterator.next();
			if (metadataContactDTO.containRole(role)) {
				result.add(metadataContactDTO);
			}
		}
		return result;
	}

}
