package fr.sedoo.metadata.client.ui.widget.dialog.contact;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.widget.ConfirmCallBack;
import fr.sedoo.commons.client.widget.dialog.DialogBoxContent;
import fr.sedoo.metadata.client.ui.view.presenter.EditingPresenter;
import fr.sedoo.metadata.client.ui.widget.table.contact.MetadataContactSelectionTable;
import fr.sedoo.metadata.shared.domain.dto.MetadataContactDTO;

public class ContactListDialogBoxContent extends Composite implements DialogBoxContent {

	private static ContactDialogBoxContentUiBinder uiBinder = GWT.create(ContactDialogBoxContentUiBinder.class);

	interface ContactDialogBoxContentUiBinder extends UiBinder<Widget, ContactListDialogBoxContent> {
	}

	@UiField(provided = true)
	MetadataContactSelectionTable contactTable;

	@UiField
	Button ok;

	@UiField
	Button cancel;

	private DialogBox dialog;

	private List<MetadataContactDTO> resultList;

	private ConfirmCallBack confirmCallback;

	public ContactListDialogBoxContent(ConfirmCallBack confirmCallback, EditingPresenter presenter, String role) {
		contactTable = new MetadataContactSelectionTable(role);
		initWidget(uiBinder.createAndBindUi(this));
		this.confirmCallback = confirmCallback;
		contactTable.setPresenter(presenter);
		resultList = new ArrayList<MetadataContactDTO>();
		contactTable.refresh();
	}

	@UiHandler("cancel")
	void onCancelClicked(ClickEvent event) {
		if (dialog != null) {
			dialog.hide();
		}
	}

	@UiHandler("ok")
	void onOkClicked(ClickEvent event) {
		if (dialog != null) {
			setResultList(contactTable.getSelectedItems());
			dialog.hide();
			confirmCallback.confirm(true);
		}
	}

	public void setDialogBox(DialogBox dialog) {
		this.dialog = dialog;
	}

	public List<MetadataContactDTO> getResultList() {
		return resultList;
	}

	public void setResultList(List<MetadataContactDTO> resultList) {
		this.resultList = resultList;
	}

	@Override
	public String getPreferredHeight() {
		return "400px";
	}

}
