package fr.sedoo.metadata.client.ui.label;

import java.util.LinkedHashMap;
import java.util.Map;

import fr.sedoo.metadata.client.message.MetadataMessage;

public class StatusLabelProvider extends PropertyLabelProvider {

	private static Map<String, String> labels = null;

	public static String getLabel(String key) {
		if ((key == null) || (key.trim().length() == 0)) {
			return "";
		}
		if (labels == null) {
			initLabels();
		}
		String label = labels.get(key);
		if (label != null) {
			return label;
		} else {
			return "";
		}
	}

	private static void initLabels() {
		labels = new LinkedHashMap<String, String>();
		String aux = MetadataMessage.INSTANCE.metadataEditingStatusItem();
		initLabels(aux, labels);
	}

	public static Map<String, String> getLabels() {
		if (labels == null) {
			initLabels();
		}
		return labels;
	}

	protected static void initLabels(String inputString, Map<String, String> labels) {
		String[] split = inputString.split("@");
		for (int i = 0; i < split.length; i++) {
			String[] split2 = split[i].split("\\|");
			labels.put(split2[0], split2[1]);
		}
	}

}
