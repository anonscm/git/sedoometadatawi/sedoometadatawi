package fr.sedoo.metadata.client.ui.widget.field.impl.keyword;

import java.util.ArrayList;
import java.util.List;

import fr.sedoo.commons.client.widget.dialog.OkCancelDialog;
import fr.sedoo.commons.client.widget.editing.EditionCallBack;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.shared.domain.thesaurus.SingleLevelThesaurus;
import fr.sedoo.metadata.shared.domain.thesaurus.ThesaurusItem;

public class SingleLevelThesaurusDialog extends OkCancelDialog {

	public SingleLevelThesaurusDialog(String language, SingleLevelThesaurus thesaurus, List<ThesaurusItem> ids, EditionCallBack<ArrayList<ThesaurusItem>> callback) {
		super(MetadataMessage.INSTANCE.keywordSelection(), new SingleLevelThesaurusDialogContent(language, thesaurus, ids, callback));
	}

}
