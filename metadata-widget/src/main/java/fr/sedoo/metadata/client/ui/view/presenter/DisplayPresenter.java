package fr.sedoo.metadata.client.ui.view.presenter;

public interface DisplayPresenter extends MetadataPresenter {

	void print(String uuid);

	void xml(String uuid);
}