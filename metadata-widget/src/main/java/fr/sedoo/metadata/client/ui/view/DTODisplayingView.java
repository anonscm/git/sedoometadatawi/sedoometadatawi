package fr.sedoo.metadata.client.ui.view;

import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.commons.shared.domain.AbstractDTO;

public interface DTODisplayingView extends IsWidget {

	void display(AbstractDTO dto);

	void reset();

}
