package fr.sedoo.metadata.client.ui.widget.field.impl;

import java.util.ArrayList;

import com.google.gwt.user.client.ui.HorizontalPanel;

import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.client.ui.widget.field.api.IsDisplay;
import fr.sedoo.metadata.client.ui.widget.field.api.IsEditor;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.client.ui.widget.table.children.AbstractSummaryTable;
import fr.sedoo.metadata.shared.domain.MetadataDTO;

public abstract class Children extends HorizontalPanel implements IsDisplay, IsEditor {
	private AbstractSummaryTable table;

	public Children() {
		super();
		setWidth(FieldConstant.HUNDRED_PERCENT);
		table = createTable();
		ElementUtil.hide(table);
		table.setVisible(false);
		add(table);
	}

	public abstract AbstractSummaryTable createTable();

	@Override
	public void display(MetadataDTO metadata) {
		table.enableDisplayMode();
		table.init(metadata.getChildrenPart().getChildren());
		table.hideToolBar();
		ElementUtil.show(table);
	}

	@Override
	public void reset() {
		table.init(new ArrayList<HasIdentifier>());
	}

	@Override
	public void flush(MetadataDTO metadata) {
		// Nothing is done via this method
	}

	@Override
	public void edit(MetadataDTO metadata) {
		table.enableEditMode();
		table.init(metadata.getChildrenPart().getChildren());
		ElementUtil.show(table);
	}

}
