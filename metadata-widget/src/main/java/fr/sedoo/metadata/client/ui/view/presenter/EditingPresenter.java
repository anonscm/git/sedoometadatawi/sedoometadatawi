package fr.sedoo.metadata.client.ui.view.presenter;

import fr.sedoo.metadata.shared.domain.MetadataDTO;

public interface EditingPresenter extends MetadataPresenter {
	void generateXML(MetadataDTO metadataDTO);

	void save(MetadataDTO dto);

	void validate(MetadataDTO dto);
}