package fr.sedoo.metadata.client.ui.widget.field.impl;

import java.util.ArrayList;

import fr.sedoo.metadata.client.ui.widget.field.primitive.I18nTextBox;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.metadata.shared.domain.dto.I18nString;

public class ResourceTitle extends I18nTextBox {

	public ResourceTitle(ArrayList<String> languages) {
		super(languages);
	}

	@Override
	public void display(MetadataDTO metadata) {
		super.display(metadata.getIdentificationPart().getResourceTitle().getDisplayValue(languages));
	}

	@Override
	public void flush(MetadataDTO metadata) {
		I18nString aux = new I18nString();
		aux.setI18nValues(getI18nValues());
		metadata.getIdentificationPart().setResourceTitle(aux);
	}

	@Override
	public void edit(MetadataDTO metadata) {
		super.edit(metadata.getIdentificationPart().getResourceTitle().getI18nValues());
	}
}
