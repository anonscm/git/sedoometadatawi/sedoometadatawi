package fr.sedoo.metadata.client.ui.editablechosen.event;

import com.google.gwt.event.shared.EventHandler;

import fr.sedoo.metadata.client.ui.editablechosen.FreeTextChosenImpl;

public class HidingDropDownEvent extends FreeTextChosenEvent<HidingDropDownEvent.HidingDropDownHandler> {

	public interface HidingDropDownHandler extends EventHandler {
		void onHidingDropdown(HidingDropDownEvent event);
	}

	public static Type<HidingDropDownHandler> TYPE = new Type<HidingDropDownHandler>();

	public static Type<HidingDropDownHandler> getType() {
		return TYPE;
	}

	public HidingDropDownEvent(FreeTextChosenImpl chosen) {
		super(chosen);
	}

	@Override
	public Type<HidingDropDownHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(HidingDropDownHandler handler) {
		handler.onHidingDropdown(this);
	}
}
