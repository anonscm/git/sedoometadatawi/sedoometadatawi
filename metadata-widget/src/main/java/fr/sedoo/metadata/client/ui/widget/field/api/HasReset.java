package fr.sedoo.metadata.client.ui.widget.field.api;

public interface HasReset {
	void reset();
}
