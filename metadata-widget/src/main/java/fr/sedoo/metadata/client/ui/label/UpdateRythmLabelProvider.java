package fr.sedoo.metadata.client.ui.label;

import java.util.LinkedHashMap;
import java.util.Map;

import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.metadata.client.message.MetadataMessage;

public class UpdateRythmLabelProvider extends PropertyLabelProvider {

	private static Map<String, String> labels = null;

	private static final String UNKNOWN_KEY = "unknown";

	/**
	 * @param key
	 * @return Le libellé internationalisé si la clé est trouvée dans la liste -
	 *         unknown sinon.
	 */
	public static String getLabel(String key) {
		String aux = key;
		if (labels == null) {
			initLabels();
		}
		if (StringUtil.isEmpty(key)) {
			aux = UNKNOWN_KEY;
		}
		String label = labels.get(aux);
		if (label != null) {
			return label;
		} else {
			return getLabel(UNKNOWN_KEY);
		}
	}

	private static void initLabels() {
		labels = new LinkedHashMap<String, String>();
		String aux = MetadataMessage.INSTANCE.metadataEditingUpdateRythmItem();
		initLabels(aux, labels);
	}

	public static Map<String, String> getLabels() {
		if (labels == null) {
			initLabels();
		}
		return labels;
	}

	protected static void initLabels(String inputString, Map<String, String> labels) {
		String[] split = inputString.split("@");
		for (int i = 0; i < split.length; i++) {
			String[] split2 = split[i].split("\\|");
			labels.put(split2[0], split2[1]);
		}
	}

}
