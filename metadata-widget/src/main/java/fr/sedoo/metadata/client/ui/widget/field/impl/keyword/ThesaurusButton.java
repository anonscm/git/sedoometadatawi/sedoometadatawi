package fr.sedoo.metadata.client.ui.widget.field.impl.keyword;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;

import fr.sedoo.commons.client.widget.editing.EditionCallBack;
import fr.sedoo.metadata.shared.domain.thesaurus.SingleLevelThesaurus;
import fr.sedoo.metadata.shared.domain.thesaurus.Thesaurus;
import fr.sedoo.metadata.shared.domain.thesaurus.ThesaurusItem;

public class ThesaurusButton extends Button implements ClickHandler, EditionCallBack<ArrayList<ThesaurusItem>> {

	private Thesaurus thesaurus;
	private KeywordContainer container;
	private String currentLanguage;

	public ThesaurusButton(String currentLanguage, Thesaurus thesaurus, KeywordContainer container) {
		super(thesaurus.getShortLabel());
		this.currentLanguage = currentLanguage;
		this.thesaurus = thesaurus;
		this.container = container;
		setWidth("100%");
		addClickHandler(this);
		if ((thesaurus instanceof SingleLevelThesaurus) == false) {
			setEnabled(false);
		}
	}

	@Override
	public void onClick(ClickEvent event) {
		if (thesaurus instanceof SingleLevelThesaurus) {

			SingleLevelThesaurusDialog dialog = new SingleLevelThesaurusDialog(currentLanguage, (SingleLevelThesaurus) thesaurus, container.getKeywordsByThesaurusId(thesaurus.getId()), this);
			dialog.show();
		}
	}

	@Override
	public void postEditProcess(boolean result, ArrayList<ThesaurusItem> newItems) {
		container.setThesaurusKeywords(thesaurus.getId(), newItems);
	}

}
