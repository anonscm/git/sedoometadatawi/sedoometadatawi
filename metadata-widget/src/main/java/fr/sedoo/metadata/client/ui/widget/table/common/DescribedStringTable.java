package fr.sedoo.metadata.client.ui.widget.table.common;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.cellview.client.Column;

import fr.sedoo.commons.client.widget.table.AnchorCell;
import fr.sedoo.commons.client.widget.table.WaterMarkCell;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.commons.shared.domain.IsUrl;
import fr.sedoo.metadata.shared.domain.misc.IdentifiedDescribedString;

public class DescribedStringTable extends StringTable {

	private String descriptionWatermark = "";
	private String valueColumnHeader = "";
	private String descriptionColumnHeader = "";
	private boolean editColumnInitialized = false;
	private boolean displayColumnInitialized = false;
	protected Column<HasIdentifier, String> descriptionColumn;

	@UiConstructor
	public DescribedStringTable(String waterMark, String descriptionWatermark, String addItemText, String valueColumnHeader, String descriptionColumnHeader) {
		super(waterMark, addItemText);
		this.setDescriptionWatermark(descriptionWatermark);
		if (valueColumnHeader != null) {
			this.setValueColumnHeader(valueColumnHeader);
		}

		if (descriptionColumnHeader != null) {
			this.setDescriptionColumnHeader(descriptionColumnHeader);
		}

	}

	public void addEmptyRow() {
		Long maxId = 0L;
		List<IdentifiedDescribedString> newValues = new ArrayList<IdentifiedDescribedString>();
		Iterator<? extends HasIdentifier> iterator = model.iterator();
		while (iterator.hasNext()) {
			IdentifiedDescribedString aux = (IdentifiedDescribedString) iterator.next();
			newValues.add(aux);
			if (aux.getId() > maxId) {
				maxId = aux.getId();
			}
		}
		newValues.add(getDefaultIdentifiedString(maxId + 1));
		init(newValues);
	}

	protected void localInitColumns() {

	}

	public void enableEditMode() {
		getToolBarPanel().setVisible(true);
		if (editColumnInitialized == false) {
			initEditColumns();
			editColumnInitialized = true;
		}
	}

	public void enableDisplayMode() {
		getToolBarPanel().setVisible(false);
		if (displayColumnInitialized == false) {
			initDisplayColumns();
			displayColumnInitialized = true;
		}
	}

	protected void initEditColumns() {

		valueColumn = new Column<HasIdentifier, String>(new WaterMarkCell(getWatermark(), "30", "120")) {
			@Override
			public String getValue(HasIdentifier identifiedString) {
				return ((IdentifiedDescribedString) identifiedString).getValue();
			}
		};

		valueColumn.setFieldUpdater(new FieldUpdater<HasIdentifier, String>() {
			@Override
			public void update(int index, HasIdentifier identifiedString, String value) {
				((IdentifiedDescribedString) identifiedString).setValue(value);
			}
		});

		descriptionColumn = new Column<HasIdentifier, String>(new WaterMarkCell(getDescriptionWatermark(), "30", "120")) {
			@Override
			public String getValue(HasIdentifier identifiedString) {
				return ((IdentifiedDescribedString) identifiedString).getDescription();
			}
		};

		descriptionColumn.setFieldUpdater(new FieldUpdater<HasIdentifier, String>() {
			@Override
			public void update(int index, HasIdentifier identifiedString, String description) {
				((IdentifiedDescribedString) identifiedString).setDescription(description);
			}
		});

		// valueColumn.setCellStyleNames("url");
		itemTable.addColumn(valueColumn, getValueColumnHeader());
		itemTable.setColumnWidth(valueColumn, 50.0, Unit.PCT);
		itemTable.addColumn(descriptionColumn, getDescriptionColumnHeader());
		itemTable.setColumnWidth(descriptionColumn, 50.0, Unit.PCT);
		itemTable.addColumn(deleteColumn);
		itemTable.setColumnWidth(deleteColumn, 30.0, Unit.PX);
		setAddButtonEnabled(true);
	}

	protected void initDisplayColumns() {

		Column<HasIdentifier, IsUrl> linkColumn = new Column<HasIdentifier, IsUrl>(new AnchorCell(false)) {
			@Override
			public IsUrl getValue(HasIdentifier identifiedDescribedString) {
				return (IsUrl) identifiedDescribedString;
			}
		};

		itemTable.addColumn(linkColumn);
		itemTable.setColumnWidth(linkColumn, 80.0, Unit.PX);
	}

	public void reset() {
		List<IdentifiedDescribedString> aux = new ArrayList<IdentifiedDescribedString>();
		init(aux);
	}

	protected IdentifiedDescribedString getDefaultIdentifiedString(Long id) {

		IdentifiedDescribedString string = new IdentifiedDescribedString();
		string.setValue("");
		string.setId(id);
		return string;
	}

	public String getDescriptionWatermark() {
		return descriptionWatermark;
	}

	public void setDescriptionWatermark(String descriptionWatermark) {
		this.descriptionWatermark = descriptionWatermark;
	}

	public String getDescriptionColumnHeader() {
		return descriptionColumnHeader;
	}

	public void setDescriptionColumnHeader(String descriptionColumnHeader) {
		this.descriptionColumnHeader = descriptionColumnHeader;
	}

	public String getValueColumnHeader() {
		return valueColumnHeader;
	}

	public void setValueColumnHeader(String valueColumnHeader) {
		this.valueColumnHeader = valueColumnHeader;
	}

}
