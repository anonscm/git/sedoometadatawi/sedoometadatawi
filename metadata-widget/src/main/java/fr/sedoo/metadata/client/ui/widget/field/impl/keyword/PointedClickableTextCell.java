package fr.sedoo.metadata.client.ui.widget.field.impl.keyword;

import com.google.gwt.cell.client.ClickableTextCell;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

public class PointedClickableTextCell extends ClickableTextCell {

	@Override
	protected void render(com.google.gwt.cell.client.Cell.Context context, SafeHtml value, SafeHtmlBuilder sb) {
		sb.appendHtmlConstant("<span style=\"cursor:pointer\">");
		super.render(context, value, sb);
		sb.appendHtmlConstant("</span>");
	}

}
