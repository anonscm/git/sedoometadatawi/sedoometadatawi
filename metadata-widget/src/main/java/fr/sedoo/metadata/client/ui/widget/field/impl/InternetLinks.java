package fr.sedoo.metadata.client.ui.widget.field.impl;

import java.util.List;

import com.google.gwt.user.client.ui.HorizontalPanel;

import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.widget.field.api.IsDisplay;
import fr.sedoo.metadata.client.ui.widget.field.api.IsEditor;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.client.ui.widget.table.link.InternetLinkTable;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.metadata.shared.domain.dto.InternetLink;

public class InternetLinks extends HorizontalPanel implements IsDisplay, IsEditor {
	private InternetLinkTable table;

	public InternetLinks() {
		super();
		setWidth(FieldConstant.HUNDRED_PERCENT);
		String linkWatermark = MetadataMessage.INSTANCE.metadataEditingURLDefaultValue();
		String labelWaterMark = MetadataMessage.INSTANCE.metadataEditingURLDescriptionDefaultValue();
		String linkHeader = MetadataMessage.INSTANCE.metadataEditingURLTableURLHeader();
		String labelHeader = MetadataMessage.INSTANCE.metadataEditingURLTableDescriptionHeader();
		String addItemText = MetadataMessage.INSTANCE.metadataEditingURLTableAddItemText();

		table = new InternetLinkTable(linkWatermark, labelWaterMark, addItemText, linkHeader, labelHeader);
		table.setProtocolVisible(true);
		table.setAskDeletionConfirmation(false);
		ElementUtil.hide(table);
		table.setVisible(false);
		add(table);
	}

	@Override
	public void display(MetadataDTO metadata) {
		table.enableDisplayMode();
		table.init(metadata.getIdentificationPart().getResourceURL());
		ElementUtil.show(table);
	}

	@Override
	public void reset() {
		table.reset();
	}

	@Override
	public void flush(MetadataDTO metadata) {
		metadata.getIdentificationPart().setResourceURL((List<InternetLink>) table.getModel());
	}

	@Override
	public void edit(MetadataDTO metadata) {
		table.enableEditMode();
		table.init(metadata.getIdentificationPart().getResourceURL());
		ElementUtil.show(table);
	}
	
	public void setProtocolVisible(boolean value)
	{
		table.setProtocolVisible(value);
	}
}
