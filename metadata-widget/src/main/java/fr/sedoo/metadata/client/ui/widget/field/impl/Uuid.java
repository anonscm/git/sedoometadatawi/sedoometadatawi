package fr.sedoo.metadata.client.ui.widget.field.impl;

import fr.sedoo.metadata.client.ui.widget.field.primitive.LabelField;
import fr.sedoo.metadata.shared.domain.MetadataDTO;

/**
 * Field devoted to the uuid. The uuid cannot be edited. the uuid is only
 * displayed, even in a edit context.
 * 
 */
public class Uuid extends LabelField {

	public Uuid() {
		super();
	}

	@Override
	public void display(MetadataDTO metadata) {
		super.display(metadata.getOtherPart().getUuid());
	}

	@Override
	public void edit(MetadataDTO metadata) {
		display(metadata);
	}
}
