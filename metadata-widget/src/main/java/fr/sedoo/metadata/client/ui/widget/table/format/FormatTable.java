package fr.sedoo.metadata.client.ui.widget.table.format;

import java.util.Iterator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;

import fr.sedoo.commons.client.callback.OperationCallBack;
import fr.sedoo.commons.client.widget.ConfirmCallBack;
import fr.sedoo.commons.client.widget.DialogBoxTools;
import fr.sedoo.commons.client.widget.table.AbstractListTable;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.view.FormatListView.Presenter;
import fr.sedoo.metadata.client.ui.widget.table.misc.HasIdentifierWrapper;
import fr.sedoo.metadata.client.ui.widget.table.misc.HasIdentifierWrapperList;
import fr.sedoo.metadata.shared.domain.dto.FormatDTO;

public class FormatTable extends AbstractListTable {

	private Presenter presenter;
	private FormatDialogBoxContent content;

	@Override
	public void addItem() {
		content = new FormatDialogBoxContent(new ConfirmCallBack() {

			@Override
			public void confirm(boolean choice) {
				if (choice == true) {
					FormatDTO resultValue = getContent().getResultValue();
					presenter.saveFormat(resultValue, new AddItemCallback());
				}
			}
		});
		DialogBoxTools.popUp(MetadataMessage.INSTANCE.formatTableAddItem(), content);
	}

	@Override
	public void presenterEdit(HasIdentifier hasId) {
		content = new FormatDialogBoxContent(new ConfirmCallBack() {
			;

			@Override
			public void confirm(boolean choice) {
				if (choice == true) {
					FormatDTO resultValue = getContent().getResultValue();
					presenter.saveFormat(resultValue, new EditItemCallback());
				}
			}
		});

		content.edit((FormatDTO) ((HasIdentifierWrapper) hasId).getSrc());
		DialogBoxTools.popUp(MetadataMessage.INSTANCE.metadataContactTableAddItemText(), content);

	}

	public FormatDialogBoxContent getContent() {
		return content;
	}

	@Override
	public String getAddItemText() {
		return MetadataMessage.INSTANCE.formatTableAddItem();
	}

	@Override
	public void presenterDelete(HasIdentifier hasId) {
		String id = hasId.getIdentifier();
		Long formatId = formatIdFromId(id);
		presenter.deleteFormat(formatId);
	}

	private Long formatIdFromId(String id) {
		List<? extends HasIdentifier> aux = getModel();
		Iterator<? extends HasIdentifier> iterator = aux.iterator();
		while (iterator.hasNext()) {
			HasIdentifier hasId = (HasIdentifier) iterator.next();
			if (hasId.getIdentifier().compareTo(id) == 0) {
				HasIdentifierWrapper wrapper = (HasIdentifierWrapper) hasId;
				return ((FormatDTO) wrapper.getSrc()).getId();
			}
		}
		return -1L;
	}

	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	protected void initColumns() {

		TextColumn<HasIdentifier> nameColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) {
				FormatDTO format = (FormatDTO) ((HasIdentifierWrapper) aux).getSrc();
				return format.getName();
			}
		};
		itemTable.addColumn(nameColumn, MetadataMessage.INSTANCE.formatTableNameHeader());
		itemTable.setColumnWidth(nameColumn, 100.0, Unit.PX);

		TextColumn<HasIdentifier> versionColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) {
				FormatDTO format = (FormatDTO) ((HasIdentifierWrapper) aux).getSrc();
				List<String> versions = format.getVersions();
				return toStringVersions(versions);
			}

		};
		itemTable.addColumn(versionColumn, MetadataMessage.INSTANCE.formatTableVersionHeader());
		itemTable.setColumnWidth(versionColumn, 100.0, Unit.PX);

		super.initColumns();
	}

	private String toStringVersions(List<String> versions) {
		StringBuffer sb = new StringBuffer();
		if (versions != null) {
			Iterator<String> iterator = versions.iterator();
			while (iterator.hasNext()) {
				String current = iterator.next();
				sb.append(current);
				if (iterator.hasNext()) {
					sb.append("-");
				}
			}
		}
		return sb.toString();
	}

	private class AddItemCallback implements OperationCallBack {

		@Override
		public void postExecution(boolean result, Object src) {
			Long maxId = 0L;
			FormatDTO savedObject = (FormatDTO) src;
			HasIdentifierWrapperList newValues = new HasIdentifierWrapperList();
			Iterator<? extends HasIdentifier> iterator = model.iterator();
			while (iterator.hasNext()) {
				HasIdentifierWrapper aux = (HasIdentifierWrapper) iterator.next();
				newValues.add(aux);
				if (new Long(aux.getIdentifier()) > maxId) {
					maxId = new Long(aux.getIdentifier());
				}
			}
			HasIdentifierWrapper wrapper = new HasIdentifierWrapper(savedObject, "" + (maxId + 1));
			newValues.add(wrapper);
			init(newValues);

		}

	}

	private class EditItemCallback implements OperationCallBack {

		@Override
		public void postExecution(boolean result, Object src) {
			FormatDTO savedObject = (FormatDTO) src;
			HasIdentifierWrapperList newValues = new HasIdentifierWrapperList();
			Iterator<? extends HasIdentifier> iterator = model.iterator();
			while (iterator.hasNext()) {
				HasIdentifierWrapper aux = (HasIdentifierWrapper) iterator.next();
				FormatDTO current = (FormatDTO) aux.getSrc();
				if (current.getId() == savedObject.getId()) {
					HasIdentifierWrapper wrapper = new HasIdentifierWrapper(savedObject, aux.getIdentifier());
					newValues.add(wrapper);
				} else {
					newValues.add(aux);
				}
			}
			init(newValues);
		}

	}

}
