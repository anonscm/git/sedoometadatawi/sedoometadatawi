package fr.sedoo.metadata.client.ui.widget.field.impl.keyword;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.WidgetCollection;

import fr.sedoo.commons.client.util.SeparatorUtil;
import fr.sedoo.metadata.shared.domain.thesaurus.ThesaurusItem;

public class KeywordContainer extends FlowPanel {

	private static final String BASIC_KEYWORD = "BASIC_KEYWORD";

	public KeywordContainer() {
		super();
		// setWidth("100%");
		// setHeight("100%");
		setStyleName("keywordContainer");
		// add(new Label("coucou"));
	}

	private Map<String, ArrayList<ThesaurusItem>> keywords = new HashMap<String, ArrayList<ThesaurusItem>>();

	public ArrayList<ThesaurusItem> getKeywordsByThesaurusId(String thesaurusId) {
		ArrayList<ThesaurusItem> result = keywords.get(thesaurusId);
		if (result == null) {
			return new ArrayList<ThesaurusItem>();
		} else {
			return result;
		}
	}

	public void setThesaurusKeywords(String thesaurusId, ArrayList<ThesaurusItem> newItems) {
		ArrayList<ThesaurusItem> oldItems = keywords.get(thesaurusId);
		if (oldItems == null) {
			oldItems = new ArrayList<ThesaurusItem>();
		}
		keywords.put(thesaurusId, newItems);

		// Mise à jour de la représentation graphique
		// On supprime les mots clés précédement présents mais désormais
		// supprimés
		for (ThesaurusItem thesaurusItem : oldItems) {
			if (newItems.contains(thesaurusItem) == false) {
				deleteThesaurusItem(thesaurusId, thesaurusItem.getId());
			}
		}

		// On ajoute en queue les mots clés nouvellement ajoutés
		for (ThesaurusItem thesaurusItem : newItems) {
			if (oldItems.contains(thesaurusItem) == false) {
				addThesaurusItem(thesaurusId, thesaurusItem);
			}
		}
	}

	private void addThesaurusItem(String thesaurusId, ThesaurusItem thesaurusItem) {
		String id = thesaurusId + SeparatorUtil.AROBAS_SEPARATOR + thesaurusItem.getId();
		Keyword keyword = new Keyword(id, thesaurusItem.getValue());
		keyword.setContainer(this);
		add(keyword);
	}

	public void addBasicKeyword(String basicValue, boolean isCrossVisible) {
		String id = BASIC_KEYWORD + SeparatorUtil.AROBAS_SEPARATOR + basicValue;
		Keyword keyword = new Keyword(id, basicValue, isCrossVisible);
		keyword.setContainer(this);
		add(keyword);
	}

	private void deleteThesaurusItem(String thesaurusId, String id) {
		deleteGraphicalElement(thesaurusId + SeparatorUtil.AROBAS_SEPARATOR + id);
	}

	public void deleteGraphicalElement(String id) {
		// Suppression graphique
		WidgetCollection childrens = getChildren();
		Iterator<Widget> iterator = childrens.iterator();
		while (iterator.hasNext()) {
			Widget widget = (Widget) iterator.next();
			if (widget instanceof Keyword) {
				if (((Keyword) widget).getId().compareTo(id) == 0) {
					remove(widget);
				}
			}
		}

		// Suppression dans le modele
		String[] split = id.split(SeparatorUtil.AROBAS_SEPARATOR);
		String thesaurusId = split[0];
		String keywordId = split[1];
		ArrayList<ThesaurusItem> aux = keywords.get(thesaurusId);
		if (aux != null) {
			for (ThesaurusItem thesaurusItem : aux) {
				if (thesaurusItem.getId().compareToIgnoreCase(keywordId) == 0) {
					aux.remove(thesaurusItem);
					break;
				}
			}
		}
	}

	public void reset() {
		clear();
		setWidth("100%");
		keywords = new HashMap<String, ArrayList<ThesaurusItem>>();

	}
}
