package fr.sedoo.metadata.client.ui.widget.table.link;

import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;

import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.table.AnchorCell;
import fr.sedoo.commons.shared.domain.IsUrl;
import fr.sedoo.metadata.shared.domain.dto.InternetLink;

public class InternetLinkCell extends AnchorCell {

	private boolean showProtocol;

	public InternetLinkCell(boolean showUrlWhenDescription, boolean showProtocol) {
		super(showUrlWhenDescription);
		this.showProtocol = showProtocol;
	}

	@Override
	public void render(com.google.gwt.cell.client.Cell.Context context, IsUrl s, SafeHtmlBuilder sb) {
		if (showProtocol == false) {
			super.render(context, s, sb);
		} else {
			super.render(context, s, sb);
			String protocol = ((InternetLink) s).getProtocol();
			if (StringUtil.isEmpty(protocol)) {
				protocol = InternetLink.DEFAULT_PROTOCOL;
			}
			String protocolLabel = InternetLinkTable.getProtocolLabelByCode(protocol);
			sb.append(SafeHtmlUtils.fromTrustedString("&nbsp;<span class=\"linkProtocol\" >" + protocolLabel + "</span>"));
		}

		// if (StringUtil.isNotEmpty(s.getLabel())) {
		// if (showUrlWhenDescription) {
		// sb.append(templateAvecDescription.hyperText(UriUtils.fromString(s.getLink()),
		// s.getLabel(), SafeHtmlUtils.fromString(s.getLink())));
		// } else {
		// sb.append(templateSansDescription.hyperText(UriUtils.fromString(s.getLink()),
		// SafeHtmlUtils.fromString(s.getLabel())));
		// }
		// } else {
		// sb.append(templateSansDescription.hyperText(UriUtils.fromString(s.getLink()),
		// SafeHtmlUtils.fromString(s.getLink())));
		// }
	}
}
