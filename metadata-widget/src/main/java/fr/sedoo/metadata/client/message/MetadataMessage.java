package fr.sedoo.metadata.client.message;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.LocalizableResource.DefaultLocale;
import com.google.gwt.i18n.client.LocalizableResource.Generate;
import com.google.gwt.i18n.client.Messages;

@Generate(format = { "com.google.gwt.i18n.rebind.format.PropertiesFormat" }, fileName = "Message", locales = { "fr", "en", "default" })
@DefaultLocale("en")
public interface MetadataMessage extends Messages {

	public static final MetadataMessage INSTANCE = GWT.create(MetadataMessage.class);

	@Key("metadataEditing.noValidationAlert")
	public String metadataEditingNoValidationAlert();

	@Key("metadataEditing.resourceTitle")
	public String metadataEditingResourceTitle();

	@Key("metadataEditing.resourceAlternateTitle")
	public String metadataEditingResourceAlternateTitle();

	@Key("metadataEditing.logo")
	public String metadataEditingLogo();

	@Key("metadataEditing.logourl")
	public String metadataEditingLogoUrl();

	@Key("metadataEditing.purpose")
	public String metadataEditingPurpose();

	@Key("metadataEditing.location")
	public String metadataEditingLocalisation();

	@Key("metadataEditing.resourceAbstract")
	public String metadataEditingResourceAbstract();

	@Key("metadataEditing.header")
	public String metadataEditingHeader();

	@Key("metadataCreating.title")
	public String metadataCreatingTitle();

	@Key("metadataDisplaying.title")
	public String metadataDisplayingTitle();

	@Key("observatoryDisplaying.title")
	public String observatoryDisplayingTitle();

	@Key("observatoryDisplaying.presentationSection")
	public String observatoryDisplayingPresentationSection();

	@Key("metadataEditing.resourceURL")
	public String metadataEditingResourceURL();

	@Key("metadataEditing.resourceStatus")
	public String metadataEditingResourceStatus();

	@Key("metadataEditing.statusItem")
	public String metadataEditingStatusItem();

	@Key("metadataEditing.statusItemTooltip")
	public String metadataEditingStatusItemTooltip();

	@Key("metadataEditing.resourceUpdateRythm")
	public String metadataEditingResourceUpdateRythm();

	@Key("metadataEditing.updateRythmItem")
	public String metadataEditingUpdateRythmItem();

	@Key("metadataEditing.updateRythmItemTooltip")
	public String metadataEditingUpdateRythmItemTooltip();

	@Key("metadataEditing.URLDefaultValue")
	public String metadataEditingURLDefaultValue();

	@Key("metadataEditing.URLTableAddItemText")
	public String metadataEditingURLTableAddItemText();

	@Key("metadataEditing.URLDescriptionDefaultValue")
	public String metadataEditingURLDescriptionDefaultValue();

	@Key("metadataEditing.URLTableURLHeader")
	public String metadataEditingURLTableURLHeader();

	@Key("metadataEditing.URLTableDescriptionHeader")
	public String metadataEditingURLTableDescriptionHeader();

	@Key("metadataEditing.SnapshotList")
	public String metadataEditingSnapshotList();

	@Key("metadataEditing.Snapshots")
	public String metadataEditingSnapshots();

	@Key("metadataEditing.SnapshotTableAddItemText")
	public String metadataEditingSnapshotTableAddItemText();

	@Key("metadataEditing.SnapshotDefaultValue")
	public String metadataEditingSnapshotDefaultValue();

	@Key("metadataEditing.SnapshotDescriptionDefaultValue")
	public String metadataEditingSnapshotDescriptionDefaultValue();

	@Key("metadataEditing.SnapshotTableURLHeader")
	public String metadataEditingSnapshotTableURLHeader();

	@Key("metadataEditing.SnapshotTableDescriptionHeader")
	public String metadataEditingSnapshotTableDescriptionHeader();

	@Key("metadataEditing.resourceIdentifier")
	public String metadataEditingResourceIdentifier();

	@Key("metadataEditing.resourceIdentifiers")
	public String metadataEditingResourceIdentifiers();

	@Key("metadataEditing.resourceNameSpace")
	public String metadataEditingResourceNameSpace();

	@Key("metadataEditing.resourceIdentifierAddItemText")
	public String metadataEditingResourceIdentifierAddItemText();

	@Key("metadataEditing.resourceIdentifierWaterMark")
	public String metadataEditingResourceIdentifierWaterMark();

	@Key("metadataEditing.resourceNameSpaceWaterMark")
	public String metadataEditingResourceNameSpaceWaterMark();

	@Key("metadataEditing.identificationTabHeader")
	public String metadataEditingIdentificationTabHeader();

	@Key("metadataEditing.measurementTabHeader")
	public String metadataEditingMeasurementTabHeader();

	@Key("metadataEditing.keywordTabHeader")
	public String metadataEditingKeywordTabHeader();

	@Key("metadataEditing.geographicalLocationTabHeader")
	public String metadataEditingGeographicalLocationTabHeader();

	@Key("metadataEditing.constraintTabHeader")
	public String metadataEditingConstraintTabHeader();

	@Key("metadataEditing.contactTabHeader")
	public String metadataEditingContactTabHeader();

	@Key("metadataEditing.temporalExtentHeader")
	public String metadataEditingTemporalExtentHeader();

	@Key("metadataEditing.metadataTabHeader")
	public String metadataEditingMetadataTabHeader();

	@Key("metadataEditing.otherTabHeader")
	public String metadataEditingOtherTabHeader();

	@Key("metadataEditing.observatoryTabHeader")
	public String metadataEditingObservatoryTabHeader();

	@Key("metadataEditing.observatoryDescription")
	public String metadataEditingObservatoryDescription();

	@Key("metadataEditing.metatadataUuid")
	public String metatadaEditingMetadataUuid();

	@Key("metadataEditing.metatadaEditingMetadataLastModificationDate")
	public String metatadaEditingMetadataLastModificationDate();

	@Key("metadataEditing.useConditions")
	public String metadataEditingUseConditions();

	@Key("metadataEditing.publicAccessLimitations")
	public String metadataEditingPublicAccessLimitations();

	@Key("metadataEtiting.metadataContactTable.addItemText")
	public String metadataContactTableAddItemText();

	@Key("metadataEtiting.metadataContactList")
	public String metadataContactList();

	@Key("metadataEtiting.resourceContactList")
	public String resourceContactList();

	@Key("metadataEditing.startDate")
	public String metadataEditingStartDate();

	@Key("metadataEditing.endDate")
	public String metadataEditingEndDate();

	@Key("metadataEditing.lastModificationDate")
	public String metadataEditingLastModificationDate();

	@Key("metadataEditing.creationDate")
	public String metadataEditingCreationDate();

	@Key("metadataEditing.lastRevisionDate")
	public String metadataEditingLastRevisionDate();

	@Key("metadataEditing.publicationDate")
	public String metadataEditingPublicationDate();

	@Key("metadataEditing.coordinateSystem")
	public String metadataEditingCoordinateSystem();

	@Key("metadataEditing.coordinateSystemTooltip")
	public String metadataEditingCoordinateSystemTooltip();

	@Key("metadataEditing.resourceLanguages")
	public String metadataEditingResourceLanguages();

	@Key("metadataEditing.resourceFormat")
	public String metadataEditingFormat();

	@Key("metadataEditing.resourceFormatVersion")
	public String metadataEditingFormatVersion();

	@Key("metadataEditing.resourceFormatName")
	public String metadataEditingFormatName();

	@Key("metadataEditing.resourceFormatNameIsMandatory")
	public String metadataEditingFormatNameIsMandatory();

	@Key("metadataEditing.resourceFormatGuideText")
	public String metadataEditingFormatGuideText();

	@Key("metadataEditing.maintenance")
	public String metadataEditingMaintenance();

	@Key("metadataEditing.state")
	public String metadataEditingState();

	@Key("metadataEditing.title")
	public String metadataEditingTitle();

	@Key("metadataEditing.charset")
	public String metadataEditingCharset();

	@Key("metadataEditing.abstract")
	public String metadataEditingAbstract();

	@Key("metadataEditing.linkWithEndDate")
	public String metadataEditingLinkWithEndDate();

	@Key("metadataEditing.observatoryName")
	public String metadataEditingObservatoryName();

	@Key("metadataEditing.drainageBasinName")
	public String metadataEditingDrainageBasinName();

	@Key("metadataEditing.coveredPeriods")
	public String metadataEditingCoveredPeriods();

	@Key("metadataEditing.temporalMilestones")
	public String metadataEditingTemporalMilestones();

	@Key("metadataEditing.keywords")
	public String metadataEditingKeywords();

	@Key("metadataEditing.technicalInformations")
	public String metadataEditingTechnichalInformations();

	@Key("metadataEditing.metadataInformations")
	public String metadataEditingMetadataInformations();

	@Key("metadataEditing.languages")
	public String metadataEditingLanguages();

	@Key("metadataEditing.dataLanguages")
	public String metadataEditingDataLanguages();

	@Key("metadataEditing.metadataLanguage")
	public String metadataEditingMetadataLanguage();

	@Key("headerView.notConnectedMessage")
	public String headerViewNotConnectedMessage();

	@Key("headerView.catalogLink")
	public String headerViewCatalogLink();

	@Key("headerView.informationSiteLink")
	public String headerViewInformationSiteLink();

	@Key("systemView.header")
	public String systemViewHeader();

	@Key("geoSummaryView.header")
	public String geoSummaryViewHeader();

	@Key("systemView.applicationVersion")
	public String systemViewApplicationVersion();

	@Key("systemView.javaVersion")
	public String systemViewJavaVersion();

	@Key("systemView.dataBaseInformations")
	public String systemViewDatabaseInformations();

	@Key("systemView.printServiceURL")
	public String systemViewPrintServiceURL();

	@Key("languageSwitching.title")
	public String languageSwitchingTitle();

	@Key("languageSwitching.message")
	public String languageSwitchingMessage();

	@Key("metadataListing.loading")
	public String metadataListingLoadingMessage();

	@Key("loginView.instructionMessage")
	public String loginViewInstructionMessage();

	@Key("loginView.loginFirstMessage")
	public String loginFirstMessage();

	@Key("loginView.login")
	public String loginViewLogin();

	@Key("loginView.password")
	public String loginViewPassword();

	@Key("loginView.connect")
	public String loginViewConnect();

	@Key("loginView.connectWithGoogle")
	public String loginViewConnectWithGoogle();

	@Key("loginView.connectWithLinkedIn")
	public String loginViewConnectWithLinkedIn();

	@Key("loginView.connectWithFacebook")
	public String loginViewConnectWithFacebook();

	@Key("loginView.connectSuccessMessage")
	public String loginViewConnectSuccessMessage();

	@Key("loginView.connectFailureMessage")
	public String loginViewConnectFailureMessage();

	@Key("loginView.header")
	public String loginViewHeader();

	@Key("observatoryManagementView.header")
	public String observatoryManagementViewHeader();

	@Key("observatoryEditingView.creationHeader")
	public String observatoryEditingViewCreationHeader();

	@Key("observatoryEditingView.modificationHeader")
	public String observatoryEditingViewModificationHeader();

	@Key("observatoryEditingView.deletionConfirmationMessage")
	public String drainageBasinDeletionConfirmationMessage();

	@Key("observatoryManagementView.observatoryList")
	public String observatoryManagementObservatoryList();

	@Key("observatoryEditingView.drainageBasinList")
	public String observatoryEditingDrainageBasinList();

	@Key("observatoryEditingView.contactList")
	public String observatoryEditingContactList();

	@Key("observatoryEditingView.datasetEditable")
	public String observatoryEditingDatasetEditable();

	@Key("observatoryEditingView.climateListText")
	public String climateListText();

	@Key("observatoryEditingView.climateItems")
	public String climateItems();

	@Key("observatoryEditingView.lithologyListText")
	public String lithologyListText();

	@Key("observatoryEditingView.lithologyItems")
	public String lithologyItems();

	@Key("drainageBasinEditingView.creationHeader")
	public String drainageBasinEditingViewCreationHeader();

	@Key("drainageBasinEditingView.modificationHeader")
	public String drainageBasinEditingViewModificationHeader();

	@Key("drainageBasinEditingView.siteName")
	public String drainageBasinEditingViewSiteName();

	@Key("drainageBasinEditingView.siteLongitude")
	public String drainageBasinEditingViewSiteLongitude();

	@Key("drainageBasinEditingView.siteLatitude")
	public String drainageBasinEditingViewSiteLatitude();

	@Key("drainageBasinEditingView.siteAltitude")
	public String drainageBasinEditingViewSiteAltitude();

	@Key("observatoryManagementView.deletionConfirmationMessage")
	public String observatoryDeletionConfirmationMessage();

	@Key("observatories")
	public String observatories();

	@Key("commons.emptyList")
	public String commonsEmptyList();

	@Key("commons.localization")
	public String localization();

	@Key("welcomeView.header")
	public String welcomeViewHeader();

	@Key("welcomeView.latestNews")
	public String welcomeViewLatestNews();

	@Key("welcomeView.addedOrModified")
	public String welcomeAddedOrModified();

	@Key("metadataSearchingView.header")
	public String metadataSearchingTitle();

	@Key("metadataSearchingView.eraseTitle")
	public String metadataSearchingEraseTitle();

	@Key("metadataSearchingView.drawTitle")
	public String metadataSearchingDrawTitle();

	@Key("metadataSearchingView.criteria")
	public String metadataSearchingViewCriteria();

	@Key("metadataSearchingView.keywords")
	public String metadataSearchingViewKeywords();

	@Key("metadataSearchingView.instruments")
	public String metadataSearchingViewInstruments();

	@Key("metadataSearchingView.observatory")
	public String metadataSearchingViewObservatory();

	@Key("metadataSearchingView.timePeriod")
	public String metadataSearchingViewTimePeriod();

	@Key("metadataSearchingView.geographicalZone")
	public String metadataSearchingViewGeographicalZone();

	@Key("metadataSearchingView.emptyCriteria")
	public String metadataSearchingEmptyCriteria();

	@Key("metadataSearchingView.emptyResult")
	public String metadataSearchingEmptyResult();

	@Key("metadataSearchingView.searchInProgress")
	public String metadataSearchingViewSearchInProgress();

	@Key("metadataSearchingView.searchWith")
	public String metadataSearchingViewSearchWith();

	@Key("metadataSearchingView.anyKeyword")
	public String metadataSearchingViewAnyKeyword();

	@Key("metadataSearchingView.allKeyword")
	public String metadataSearchingViewAllKeyword();

	@Key("metadataListingView.header")
	public String listingViewTitle();

	@Key("metadataEditing.generalInformations")
	public String metadataEditingGeneralInformations();

	@Key("metadataEditing.links")
	public String metadataEditingLinks();

	@Key("metadataEditing.otherRoles")
	public String metadataEditingOtherRoles();

	@Key("metadataEditing.metadataPointsOfContact")
	public String metadataEditingMetadataPointsOfContact();

	@Key("metadataEditing.owners")
	public String metadataEditingOwners();

	@Key("metadataEditing.dataPointsOfContact")
	public String metadataEditingDataPointsOfContact();

	@Key("metadataEditing.principalInvestigators")
	public String metadataEditingPrincipalInvestigators();

	@Key("label")
	public String label();

	@Key("edit")
	public String edit();

	@Key("print")
	public String print();

	@Key("view")
	public String view();

	@Key("search")
	public String search();

	@Key("reset")
	public String reset();

	@Key("confirm")
	public String confirm();

	@Key("information")
	public String information();

	@Key("error")
	public String error();

	@Key("help")
	public String help();

	@Key("loading")
	public String loading();

	@Key("anErrorOccurredWhileLoading")
	public String anErrorOccurredWhileLoading();

	@Key("now")
	public String now();

	@Key("anErrorHasHappened")
	public String anErrorHasHappened();

	@Key("observatory.shortLabel")
	public String observatoryShortLabel();

	@Key("observatory.longLabel")
	public String observatoryLongLabel();

	@Key("observatory.description")
	public String observatoryDescription();

	@Key("commons.save")
	public String save();

	@Key("commons.validate")
	public String validate();

	@Key("commons.back")
	public String back();

	@Key("commons.saving")
	public String saving();

	@Key("commons.deleting")
	public String deleting();

	@Key("commons.name")
	public String name();

	@Key("commons.iso8601Format")
	public String iso8601Format();

	@Key("commons.deletionConfirmMessage")
	public String deletionConfirmMessage();

	@Key("commons.mandatoryData")
	public String mandatoryData();

	@Key("commons.numericalData")
	public String numericalData();

	@Key("commons.emailData")
	public String emailData();

	@Key("commons.dateData")
	public String dateData();

	@Key("commons.dateUnconsistency")
	public String dateUnconsistency();

	@Key("commons.atLeastOneElementNeeded")
	public String atLeastOneElementNeeded();

	@Key("commons.isAdmin")
	public String isAdmin();

	@Key("mapSelector.northLatitude")
	public String mapSelectorNorthLatitude();

	@Key("mapSelector.southLatitude")
	public String mapSelectorSouthLatitude();

	@Key("mapSelector.eastLongitude")
	public String mapSelectorEastLongitude();

	@Key("mapSelector.westLongitude")
	public String mapSelectorWestLongitude();

	@Key("commons.generalDescription")
	public String generalDescription();

	@Key("drainageBasinTable.addItemText")
	public String DrainagaBasinTableAddItemText();

	@Key("siteTable.addItemText")
	public String SiteTableAddItemText();

	@Key("observatoryContactTable.addItemText")
	public String observatoryContactTableAddItemText();

	@Key("drainageBasinEditingView.SiteList")
	public String drainageBasinEditingSiteList();

	@Key("common.unsavedModificationsConfirmation")
	public String unsavedModificationsConfirmation();

	@Key("common.unsavedCreationConfirmation")
	public String unsavedCreationConfirmation();

	@Key("common.savedModifications")
	public String savedModifications();

	@Key("common.addedElement")
	public String addedElement();

	@Key("common.deletedElement")
	public String deletedElement();

	@Key("person.personName")
	public String personPersonName();

	@Key("person.organisationName")
	public String personOrganisationName();

	@Key("person.address")
	public String personAddress();

	@Key("person.street")
	public String personStreet();

	@Key("person.city")
	public String personCity();

	@Key("person.zipCode")
	public String personZipCode();

	@Key("person.country")
	public String personCountry();

	@Key("person.email")
	public String personEmail();

	@Key("person.roles")
	public String personRoles();

	@Key("person.roleItems")
	public String personRoleItems();

	@Key("observatoryContactEditingView.creationHeader")
	public String observatoryContactEditingViewCreationHeader();

	@Key("observatoryContactEditingView.modificationHeader")
	public String observatoryContactEditingViewModificationHeader();

	@Key("commons.observatory")
	public String observatory();

	@Key("commons.dataset")
	public String dataSet();

	@Key("commons.experimentalsite")
	public String experimentalSite();

	@Key("commons.experimentalsites")
	public String experimentalSites();

	@Key("commons.drainageBasin")
	public String drainageBasin();

	@Key("metadataListing.addMetadataEntry")
	public String addMetadataEntry();

	@Key("metadataEditing.addContactFromDirectory")
	public String metadataEditingAddContactFromDirectory();

	@Key("metadataEditing.selectContactFromList")
	public String metadataEditingSelectContactFromList();

	@Key("mapSelector.drawDrainageBasinButtonTooltip")
	public String drawDrainageBasinButtonTooltip();

	@Key("mapSelector.eraseDrainageBasinButtonTooltip")
	public String eraseDrainageBasinButtonTooltip();

	@Key("mapSelector.eraseSiteButtonTooltip")
	public String eraseSiteButtonTooltip();

	@Key("mapSelector.drawSiteButtonTooltip")
	public String drawSiteButtonTooltip();

	@Key("mapSelector.dragPanButtonTooltip")
	public String dragPanButtonTooltip();

	@Key("commons.filter")
	public String filter();

	@Key("metadataListing.entryList")
	public String metadataListingEntryList();

	@Key("metadataManaging.rudMetadata")
	public String metadataManagingRudMetadata();

	@Key("metadataManaging.createMetadata")
	public String metadataManagingCreateMetadata();

	@Key("metadataManaging.menu")
	public String metadataManagingMenu();

	@Key("metadataManaging.existingMetadata")
	public String existingMetadata();

	@Key("metadataManaging.observatoryTooltip")
	public String metadataManagingObservatoryTooltip();

	@Key("metadataManaging.addNewMetadata")
	public String metadataManagingAddNewMetadata();

	@Key("metadataListing.noDrainageBasinSelected")
	public String metadataListinNoDrainageBasinSelected();

	@Key("commons.selectItem")
	public String selectItem();

	@Key("commons.all")
	public String all();

	@Key("metadataProviderMenu.manageMetadata")
	public String metadataProviderMenuManageMetadata();

	@Key("metadataUserMenu")
	public String metadataUserMenu();

	@Key("metadataUserMenu.catalogBrowseLink")
	public String metadataUserMenuCatalogBrowseLink();

	@Key("metadataUserMenu.catalogByObservatoryLink")
	public String metadataUserMenuCatalogByObservatoryLink();

	@Key("metadataUserMenu.catalogByGeologyLink")
	public String metadataUserMenuCatalogByGeologyLink();

	@Key("metadataUserMenu.catalogByClimateLink")
	public String metadataUserMenuCatalogByClimateLink();

	@Key("metadataUserMenu.advancedSearchLink")
	public String metadataUserMenuAdvancedSearchLink();

	@Key("metadataUserMenu.quickSearchTooltip")
	public String metadataUserMenuQuickSearchTooltip();

	@Key("metadataUserMenu.keywordTooltip")
	public String metadataUserMenuKeywordTooltip();

	@Key("metadataUserMenu.quickSearchPlaceholder")
	public String metadataUserMenuQuickSearchPlaceholder();

	@Key("metadataUserMenu.quickSearchErrorMessage")
	public String metadataUserMenuQuickSearchErrorMessage();

	@Key("metadataUserMenu.locateLink")
	public String metadataUserMenuLocateLink();

	@Key("metadataProviderMenu")
	public String metadataProviderMenu();

	@Key("administrationMenu")
	public String administrationMenu();

	@Key("role.authorRoleTooltip")
	public String authorRoleTooltip();

	@Key("role.originatorRoleTooltip")
	public String originatorRoleTooltip();

	@Key("role.distributorRoleTooltip")
	public String distributorRoleTooltip();

	@Key("role.resourceProviderRoleTooltip")
	public String resourceProviderRoleTooltip();

	@Key("role.custodianRoleTooltip")
	public String custodianRoleTooltip();

	@Key("role.processorRoleTooltip")
	public String processorRoleTooltip();

	@Key("role.principalInvestigatorRoleTooltip")
	public String principalInvestigatorRoleTooltip();

	@Key("role.pointOfContactRoleTooltip")
	public String pointOfContactRoleTooltip();

	@Key("role.metadataPointOfContactRoleTooltip")
	public String metadataPointOfContactRoleTooltip();

	@Key("role.ownerRoleTooltip")
	public String ownerRoleTooltip();

	@Key("role.userRoleTooltip")
	public String userRoleTooltip();

	@Key("role.publisherRoleTooltip")
	public String publisherRoleTooltip();

	@Key("role.moreRoles")
	public String moreRoles();

	@Key("formatView.header")
	public String formatViewHeader();

	@Key("formatTable.nameHeader")
	public String formatTableNameHeader();

	@Key("formatTable.versionHeader")
	public String formatTableVersionHeader();

	@Key("formatTable.addItem")
	public String formatTableAddItem();

	@Key("resultView.header")
	public String resultViewHeader();

	@Key("resultView.resultFound")
	public String resultViewResultFound();

	@Key("areaSelectorWidget.drawRectangularAreaTooltip")
	public String drawRectangularAreaTooltip();

	@Key("areaSelectorWidget.eraseRectangularAreaTooltip")
	public String eraseRectangularAreaTooltip();

	@Key("geoSummaryView.loadingObsvervatory")
	public String geoSummaryViewLoadingObsvervatory();

	@Key("geoSummaryView.seeDetails")
	public String geoSummaryViewSeeDetails();

	@Key("userManagementView.header")
	public String userManagementViewHeader();

	@Key("userTable.editDialogTitle")
	public String userTableEditDialogTitle();

	@Key("userTable.createDialogTitle")
	public String userTableCreateDialogTitle();

	@Key("userTable.addItemText")
	public String userTableAddItemText();

	@Key("messageManagement.author")
	public String messageManagementAuthor();

	@Key("messageManagement.frenchContent")
	public String messageManagementFrenchContent();

	@Key("messageManagement.englishContent")
	public String messageManagementEnglishContent();

	@Key("messageManagement.header")
	public String messageManagementViewHeader();

	@Key("messageManagement.editDialogTitle")
	public String messageTableEditDialogTitle();

	@Key("messageManagement.createDialogTitle")
	public String messageTableCreateDialogTitle();

	@Key("messageManagement.addItemText")
	public String messageTableAddItemText();

	@Key("logConsultation.header")
	public String logConsultationViewHeader();

	@Key("logConsultation.category")
	public String logConsultationViewCategory();

	@Key("logConsultation.action")
	public String logConsultationViewAction();

	@Key("logConsultation.user")
	public String logConsultationViewUser();

	@Key("logConsultation.detail")
	public String logConsultationViewDetail();

	@Key("metadataEditing.epsgGuideText")
	public String metadataEditingEpsgGuideText();

	@Key("availableThesauri")
	public String availableThesauri();

	@Key("keywordSelection")
	public String keywordSelection();

	@Key("urlAreMandatory")
	public String urlAreMandatory();

	@Key("downloadProtocol")
	public String downloadProtocol();

	@Key("linkProtocol")
	public String linkProtocol();

	@Key("wfsProtocol")
	public String wfsProtocol();

	@Key("wmsProtocol")
	public String wmsProtocol();

	@Key("genealogy")
	public String metadataEditingGenealogy();

}
