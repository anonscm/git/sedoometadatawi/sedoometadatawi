package fr.sedoo.metadata.client.ui.widget.field.primitive;

import java.util.ArrayList;

import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBoxBase;

public abstract class I18nTextArea extends I18nField {

	public I18nTextArea(ArrayList<String> languages) {
		super(languages);
	}

	@Override
	protected TextBoxBase createEditor() {
		TextArea aux = new TextArea();
		aux.setVisibleLines(10);
		aux.setWidth("90%");
		return aux;
	}

}
