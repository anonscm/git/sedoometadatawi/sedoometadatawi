package fr.sedoo.metadata.client.ui.widget.table.misc;

import java.util.ArrayList;
import java.util.List;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.HasIdentifier;

public class HasIdentifierWrapper implements HasIdentifier {

	private Object src;

	public HasIdentifierWrapper(Object src, String id) {
		this.src = src;
		this.id = id;
	}

	private String id;

	public Object getSrc() {
		return src;
	}

	@Override
	public String getIdentifier() {
		return id;
	}

	@Override
	public List<ValidationAlert> validate() {
		return new ArrayList<ValidationAlert>();
	}

}
