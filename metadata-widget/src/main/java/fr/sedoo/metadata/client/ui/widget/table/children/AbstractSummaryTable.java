package fr.sedoo.metadata.client.ui.widget.table.children;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Image;

import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.widget.table.AbstractListTable;
import fr.sedoo.commons.client.widget.table.ImagedActionCell;
import fr.sedoo.commons.client.widget.table.TableBundle;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;

public abstract class AbstractSummaryTable extends AbstractListTable {

	private final Image viewImage = new Image(TableBundle.INSTANCE.view());

	@Override
	public void addItem() {

	}

	protected void initColumns() {

	}

	public void enableEditMode() {
		clearColumns();
		addNameColumn();
		addAbstractColumn();
		addEditColumn();
		addDeleteColumn();
	}

	public void enableDisplayMode() {
		clearColumns();
		addNameColumn();
		addAbstractColumn();
		addViewColum();
	}

	protected void addViewColum() {
		Cell<HasIdentifier> viewActionCell = new ImagedActionCell<HasIdentifier>(viewAction, viewImage, CommonMessages.INSTANCE.view());
		Column<HasIdentifier, String> viewColumn = new Column(viewActionCell) {

			@Override
			public HasIdentifier getValue(Object object) {
				return (HasIdentifier) object;
			}
		};
		itemTable.addColumn(viewColumn);
		itemTable.setColumnWidth(viewColumn, 30.0, Unit.PX);
	}

	protected ImagedActionCell.Delegate<HasIdentifier> viewAction = new ImagedActionCell.Delegate<HasIdentifier>() {

		@Override
		public void execute(final HasIdentifier hasId) {
			presenterView(hasId);
		}

	};

	public abstract void presenterView(HasIdentifier hasId);

	protected void addNameColumn() {
		TextColumn<HasIdentifier> nameColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) {
				return ((MetadataSummaryDTO) aux).getName();
			}
		};
		itemTable.addColumn(nameColumn, CommonMessages.INSTANCE.name());
		itemTable.setColumnWidth(nameColumn, 100.0, Unit.PX);
	}

	protected void addAbstractColumn() {
		TextColumn<HasIdentifier> abstractColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) {
				return ((MetadataSummaryDTO) aux).getDisplayAbstract();
			}
		};
		itemTable.addColumn(abstractColumn, CommonMessages.INSTANCE.description());
		itemTable.setColumnWidth(abstractColumn, 200.0, Unit.PX);
	}
	/*
	 * 
	 * 
	 * TextColumn<HasIdentifier> nameColumn = new TextColumn<HasIdentifier>() {
	 * 
	 * @Override public String getValue(HasIdentifier aux) { return
	 * ((MetadataSummaryDTO) aux).getName(); } };
	 * itemTable.addColumn(nameColumn, CommonMessages.INSTANCE.name());
	 * itemTable.setColumnWidth(nameColumn, 100.0, Unit.PX);
	 * 
	 * TextColumn<HasIdentifier> abstractColumn = new
	 * TextColumn<HasIdentifier>() {
	 * 
	 * @Override public String getValue(HasIdentifier aux) { return
	 * ((MetadataSummaryDTO) aux).getDisplayAbstract(); } };
	 * itemTable.addColumn(abstractColumn,
	 * Message.INSTANCE.observatoryDescription());
	 * itemTable.setColumnWidth(abstractColumn, 200.0, Unit.PX);
	 */

}
