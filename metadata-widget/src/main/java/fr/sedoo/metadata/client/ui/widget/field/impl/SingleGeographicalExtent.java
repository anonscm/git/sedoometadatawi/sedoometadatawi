package fr.sedoo.metadata.client.ui.widget.field.impl;

import java.util.ArrayList;

import com.google.gwt.user.client.ui.HorizontalPanel;

import fr.sedoo.commons.client.util.ListUtil;
import fr.sedoo.commons.client.widget.map.impl.AreaSelectorWidget;
import fr.sedoo.commons.client.widget.map.impl.SingleAreaSelectorWidget;
import fr.sedoo.commons.shared.domain.GeographicBoundingBoxDTO;
import fr.sedoo.metadata.client.ui.widget.field.api.IsDisplay;
import fr.sedoo.metadata.client.ui.widget.field.api.IsEditor;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.shared.domain.MetadataDTO;

public class SingleGeographicalExtent extends HorizontalPanel implements IsDisplay, IsEditor {
	protected SingleAreaSelectorWidget areaSelector;

	public SingleGeographicalExtent() {
		this(AreaSelectorWidget.DEFAULT_MAP_LAYER);
	}

	public SingleGeographicalExtent(String mapLayer) {
		super();
		setWidth(FieldConstant.HUNDRED_PERCENT);
		areaSelector = new SingleAreaSelectorWidget(mapLayer);
		add(areaSelector);
	}

	@Override
	public void display(MetadataDTO metadata) {
		areaSelector.enableDisplayMode();
		areaSelector.reset();
		ArrayList<GeographicBoundingBoxDTO> boxes = metadata.getGeographicalLocationPart().getBoxes();
		if (!boxes.isEmpty()) {
			areaSelector.setGeographicBoundingBoxDTO(boxes.get(ListUtil.FIRST_INDEX));
		}
	}

	@Override
	public void reset() {
		areaSelector.reset();
	}

	@Override
	public void flush(MetadataDTO metadata) {
		GeographicBoundingBoxDTO box = areaSelector.getGeographicBoundingBoxDTO();
		ArrayList<GeographicBoundingBoxDTO> boxes = new ArrayList<GeographicBoundingBoxDTO>();
		if (box.isValid()) {
			boxes.add(box);
		}
		metadata.getGeographicalLocationPart().setBoxes(boxes);
	}

	@Override
	public void edit(MetadataDTO metadata) {
		areaSelector.reset();
		areaSelector.enableEditMode();
		ArrayList<GeographicBoundingBoxDTO> boxes = metadata.getGeographicalLocationPart().getBoxes();
		if (!boxes.isEmpty()) {
			areaSelector.setGeographicBoundingBoxDTO(boxes.get(ListUtil.FIRST_INDEX));
		}
	}
}
