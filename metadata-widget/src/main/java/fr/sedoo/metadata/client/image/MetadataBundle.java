package fr.sedoo.metadata.client.image;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface MetadataBundle extends ClientBundle {

	@Source("darkCross.png")
	ImageResource darkCross();

	public static final MetadataBundle INSTANCE = GWT.create(MetadataBundle.class);

}