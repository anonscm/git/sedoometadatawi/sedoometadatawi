package fr.sedoo.metadata.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.metadata.shared.domain.dto.FormatDTO;

public interface FormatServiceAsync {

	void delete(Long id, AsyncCallback<Void> callback);

	void findAll(AsyncCallback<ArrayList<FormatDTO>> callback);

	void save(FormatDTO format, AsyncCallback<Long> callback);

}
