package fr.sedoo.metadata.client.ui.widget.date;

import com.google.gwt.user.datepicker.client.DateBox;

import fr.sedoo.commons.client.widget.datepicker.DateBoxWithYearSelector;
import fr.sedoo.metadata.client.util.Iso19115DateUtil;

public class Iso19115DateBox extends DateBoxWithYearSelector {

	public Iso19115DateBox() {
		super(new DateBox.DefaultFormat(Iso19115DateUtil.getDateFormater()));
	}

}
