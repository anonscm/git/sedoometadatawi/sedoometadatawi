package fr.sedoo.metadata.client.ui.widget.field.primitive;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;

import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.metadata.client.ui.widget.field.api.IsDisplay;
import fr.sedoo.metadata.client.ui.widget.field.api.IsEditor;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.shared.domain.MetadataDTO;

public abstract class LabelField extends HorizontalPanel implements IsDisplay, IsEditor {
	protected Label label;

	public LabelField() {
		super();
		setWidth(FieldConstant.HUNDRED_PERCENT);
		label = new Label();
		add(label);
	}

	public void display(String text) {
		label.setText(StringUtil.trimToEmpty(text));
	}

	@Override
	public void reset() {
		label.setText("");
	}

	@Override
	public void flush(MetadataDTO metadata) {
	}

	public void edit(String text) {
		display(text);
	}
}
