package fr.sedoo.metadata.client.ui.widget.field.impl;

import fr.sedoo.commons.metadata.utils.pdf.labelprovider.RoleLabelProvider;
import fr.sedoo.metadata.client.ui.widget.field.impl.contact.AbstractSingleRoleResourceContact;

public class PrincipalInvestigators extends AbstractSingleRoleResourceContact {

	public PrincipalInvestigators() {
		super();
	}

	public String getSingleRole() {
		return RoleLabelProvider.PRINCIPAL_INVESTIGATOR;
	}

}
