package fr.sedoo.metadata.client.ui.view;

import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.commons.shared.domain.AbstractDTO;

public interface DTOEditingView extends IsWidget {

	AbstractDTO flush();

	void edit(AbstractDTO dto);

	void setMode(String mode);

}
