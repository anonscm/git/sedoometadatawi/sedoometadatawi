package fr.sedoo.metadata.client.ui.widget.field.impl;

import fr.sedoo.commons.client.util.StringUtil;

public class KeyComparator {
	protected KeyComparator() {

	}

	public static boolean compareLosely(String str1, String str2) {
		String aux1 = StringUtil.trimToEmpty(str1).toLowerCase().replace("_", "");
		String aux2 = StringUtil.trimToEmpty(str2).toLowerCase().replace("_", "");

		return (aux1.compareTo(aux2) == 0);
	}

}
