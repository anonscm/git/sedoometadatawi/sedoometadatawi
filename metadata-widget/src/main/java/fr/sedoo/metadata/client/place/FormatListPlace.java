package fr.sedoo.metadata.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.mvp.place.AuthenticatedPlace;

public class FormatListPlace extends Place implements AuthenticatedPlace {

	public static FormatListPlace instance;

	public static class Tokenizer implements PlaceTokenizer<FormatListPlace> {
		@Override
		public String getToken(FormatListPlace place) {
			return "";
		}

		@Override
		public FormatListPlace getPlace(String token) {
			if (instance == null) {
				instance = new FormatListPlace();
			}
			return instance;
		}
	}

}
