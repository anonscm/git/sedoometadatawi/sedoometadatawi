package fr.sedoo.metadata.client.ui.view;

import fr.sedoo.metadata.client.ui.view.presenter.EditingPresenter;

public interface MetadataEditingView extends DTOEditingView {
	void setEditingPresenter(EditingPresenter presenter);

}
