package fr.sedoo.metadata.client.ui.widget.field.primitive;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.watopi.chosen.client.gwt.ChosenListBox;

import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.metadata.client.ui.widget.field.api.IsDisplay;
import fr.sedoo.metadata.client.ui.widget.field.api.IsEditor;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;

public abstract class ChosenListBoxField extends HorizontalPanel implements IsDisplay, IsEditor {

	protected static final int UNSELECTED_ID_INDEX = 0;

	protected Label label;
	protected ChosenListBox listBox;

	public ChosenListBoxField() {
		super();
		setWidth(FieldConstant.HUNDRED_PERCENT);
		label = new Label();
		listBox = new ChosenListBox();
		listBox.setMultipleSelect(true);
		listBox.setPlaceholderText("");
		listBox.setWidth("500px");
		ElementUtil.hide(label);
		ElementUtil.hide(listBox);
		add(listBox);
		add(label);
	}

	@Override
	public void reset() {
		label.setText("");
		int itemCount = listBox.getItemCount();
		for (int i = 0; i < itemCount; i++) {
			listBox.setItemSelected(i, false);
		}

		listBox.update();
	}

}
