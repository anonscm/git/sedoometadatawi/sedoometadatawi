package fr.sedoo.metadata.client.ui.editablechosen.event;

import com.google.gwt.event.shared.EventHandler;

import fr.sedoo.metadata.client.ui.editablechosen.FreeTextChosenImpl;

public class NewValueEvent extends FreeTextChosenEvent<NewValueEvent.NewValueHandler> {

	private String value;

	public interface NewValueHandler extends EventHandler {
		void onNewValue(NewValueEvent event);
	}

	public static Type<NewValueHandler> TYPE = new Type<NewValueHandler>();

	public static Type<NewValueHandler> getType() {
		return TYPE;
	}

	public NewValueEvent(FreeTextChosenImpl chosen, String value) {
		super(chosen);
		this.value = value;
	}

	@Override
	public Type<NewValueHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(NewValueHandler handler) {
		handler.onNewValue(this);
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
