package fr.sedoo.metadata.client.ui.widget.field.impl;

import java.util.Iterator;
import java.util.List;

import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.widget.field.api.IsDisplay;
import fr.sedoo.metadata.client.ui.widget.field.api.IsEditor;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.client.ui.widget.field.impl.keyword.ClientThesaurusList;
import fr.sedoo.metadata.client.ui.widget.field.impl.keyword.KeywordContainer;
import fr.sedoo.metadata.client.ui.widget.field.impl.keyword.ThesaurusButton;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.metadata.shared.domain.misc.Iso6392LanguageProvider;
import fr.sedoo.metadata.shared.domain.thesaurus.Thesaurus;

public class Keywords extends HorizontalPanel implements IsDisplay, IsEditor, LoadCallBack {
	private KeywordContainer keywordContainer;
	private VerticalPanel thesaurusPanel;
	private String currentLanguage;

	public Keywords(String currentLanguage) {
		super();

		// On convertit ISO6392->Locale
		this.currentLanguage = Iso6392LanguageProvider.convertIso6392Tolocale(currentLanguage);
		setWidth(FieldConstant.HUNDRED_PERCENT);
		keywordContainer = new KeywordContainer();

		keywordContainer.setWidth(FieldConstant.HUNDRED_PERCENT);
		keywordContainer.setHeight(FieldConstant.HUNDRED_PERCENT);
		ElementUtil.hide(keywordContainer);
		add(keywordContainer);
		setCellWidth(keywordContainer, FieldConstant.HUNDRED_PERCENT);
		setCellHeight(keywordContainer, "200px");
	}

	@Override
	public void display(MetadataDTO metadata) {
	}

	@Override
	public void reset() {
		keywordContainer.reset();
	}

	@Override
	public void flush(MetadataDTO metadata) {
	}

	@Override
	public void edit(MetadataDTO metadata) {
		if (thesaurusPanel == null) {
			thesaurusPanel = new VerticalPanel();
			thesaurusPanel.setSpacing(3);
			thesaurusPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
			thesaurusPanel.add(new Label(MetadataMessage.INSTANCE.availableThesauri()));
			add(thesaurusPanel);
			ClientThesaurusList.getThesauri(this);
		}
		Iterator<String> iterator = metadata.getKeywordPart().getKeywords().iterator();
		while (iterator.hasNext()) {
			String keyword = iterator.next();
			keywordContainer.addBasicKeyword(keyword, true);
		}
		ElementUtil.show(keywordContainer);
	}

	@Override
	public void postLoadProcess(Object result) {
		List<Thesaurus> thesauri = (List<Thesaurus>) result;
		for (Thesaurus thesaurus : thesauri) {
			ThesaurusButton thesaurusButton = new ThesaurusButton(currentLanguage, thesaurus, keywordContainer);
			thesaurusPanel.add(thesaurusButton);
		}
	}
}
