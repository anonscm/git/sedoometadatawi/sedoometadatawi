package fr.sedoo.metadata.client.ui.widget.field.impl.keyword;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.util.ElementUtil;

public class Keyword extends Composite {

	@UiField
	Label value;

	@UiField
	Image cross;

	private static KeywordUiBinder uiBinder = GWT.create(KeywordUiBinder.class);

	private String id;

	private KeywordContainer keywordContainer;

	interface KeywordUiBinder extends UiBinder<Widget, Keyword> {
	}

	public Keyword(String id, String value) {
		this.setId(id);
		initWidget(uiBinder.createAndBindUi(this));
		this.value.setText(value);
	}

	public Keyword(String id, String value, boolean isCrossVisible) {
		this(id, value);
		if (isCrossVisible) {
			ElementUtil.show(cross);
		} else {
			ElementUtil.hide(cross);
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setContainer(KeywordContainer keywordContainer) {
		this.keywordContainer = keywordContainer;
	}

	@UiHandler("cross")
	void onCrossClicked(ClickEvent event) {
		keywordContainer.deleteGraphicalElement(id);
	}
}
