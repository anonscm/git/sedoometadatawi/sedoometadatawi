package fr.sedoo.metadata.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.metadata.shared.domain.dto.FormatDTO;

@RemoteServiceRelativePath("format")
public interface FormatService extends RemoteService {

	ArrayList<FormatDTO> findAll();

	void delete(Long id) throws Exception;

	Long save(FormatDTO format) throws Exception;

}