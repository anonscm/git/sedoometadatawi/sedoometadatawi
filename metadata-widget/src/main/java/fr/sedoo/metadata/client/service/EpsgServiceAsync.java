package fr.sedoo.metadata.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.metadata.shared.domain.entity.EpsgCode;

public interface EpsgServiceAsync {

	void findAll(AsyncCallback<ArrayList<EpsgCode>> callback);

}
