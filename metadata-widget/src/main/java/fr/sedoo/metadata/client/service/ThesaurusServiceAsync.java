package fr.sedoo.metadata.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.metadata.shared.domain.thesaurus.Thesaurus;
import fr.sedoo.metadata.shared.domain.thesaurus.ThesaurusItem;

public interface ThesaurusServiceAsync {

	void getItems(String thesaurusId, String language, AsyncCallback<ArrayList<ThesaurusItem>> callback);

	void getThesaurus(String language, AsyncCallback<ArrayList<Thesaurus>> callback);

}
