package fr.sedoo.metadata.client.ui.widget.field.impl.contact;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.metadata.shared.domain.dto.MetadataContactDTO;

public abstract class AbstractSingleRoleResourceContact extends AbstractSingleRoleContact {

	public List<? extends HasIdentifier> getDatas(MetadataDTO metadata) {
		return metadata.getContactPart().getResourceContactsByRole(getSingleRole());
	}

	@Override
	public void flush(MetadataDTO metadata) {
		List<MetadataContactDTO> aux = new ArrayList<MetadataContactDTO>();
		Iterator<? extends HasIdentifier> iterator = table.getModel().iterator();
		while (iterator.hasNext()) {
			aux.add((MetadataContactDTO) iterator.next());
		}
		metadata.getContactPart().addResourceContacts(aux);
	}
}
