package fr.sedoo.metadata.client.ui.widget.field.impl;

import fr.sedoo.commons.metadata.utils.pdf.labelprovider.RoleLabelProvider;
import fr.sedoo.metadata.client.ui.widget.field.impl.contact.AbstractSingleRoleResourceContact;

public class ResourcePointOfContacts extends AbstractSingleRoleResourceContact {

	public ResourcePointOfContacts() {
		super();
	}

	public String getSingleRole() {
		return RoleLabelProvider.POINT_OF_CONTACT;
	}

}
