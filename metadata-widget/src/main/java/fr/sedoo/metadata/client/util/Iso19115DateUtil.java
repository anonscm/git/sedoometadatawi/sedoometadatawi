package fr.sedoo.metadata.client.util;

import com.google.gwt.i18n.client.DateTimeFormat;

import fr.sedoo.commons.client.util.DateUtil;

public class Iso19115DateUtil extends DateUtil {

	private static final DateTimeFormat ISO_19115_FORMATER = DateTimeFormat.getFormat("yyyy-MM-dd");

	protected Iso19115DateUtil() {

	}

	public static DateTimeFormat getDateFormater() {
		return ISO_19115_FORMATER;
	}

}
