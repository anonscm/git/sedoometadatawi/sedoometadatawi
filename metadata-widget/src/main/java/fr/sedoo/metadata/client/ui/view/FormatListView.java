package fr.sedoo.metadata.client.ui.view;

import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.commons.client.callback.OperationCallBack;
import fr.sedoo.metadata.shared.domain.dto.FormatDTO;

public interface FormatListView extends IsWidget {

	void setFormats(List<FormatDTO> formats);

	public interface Presenter {
		void deleteFormat(Long id);

		void saveFormat(FormatDTO format, OperationCallBack callback);
	}

	void setPresenter(Presenter presenter);

	public void broadcastFormatDeletion(Long id, boolean result);
}
