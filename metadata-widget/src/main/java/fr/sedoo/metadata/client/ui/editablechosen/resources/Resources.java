package fr.sedoo.metadata.client.ui.editablechosen.resources;

import com.google.gwt.core.client.GWT;
import com.watopi.chosen.client.resources.ChozenCss;

public interface Resources extends com.watopi.chosen.client.resources.Resources {

	public static final Resources INSTANCE = GWT.create(Resources.class);

	@Source("chozen.css")
	ChozenCss css();

}
