package fr.sedoo.metadata.client.ui.widget.field.impl;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.ConfirmCallBack;
import fr.sedoo.commons.client.widget.DialogBoxTools;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.widget.dialog.epsg.ClientEspgList;
import fr.sedoo.metadata.client.ui.widget.dialog.epsg.EpsgDialogBoxContent;
import fr.sedoo.metadata.client.ui.widget.field.api.IsDisplay;
import fr.sedoo.metadata.client.ui.widget.field.api.IsEditor;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.shared.domain.MetadataDTO;

public class Epsg extends HorizontalPanel implements IsDisplay, IsEditor, ClickHandler, LoadCallBack {
	private static final int EPSG_MAX_LENGTH = 10;
	private Label label;
	private VerticalPanel editPanel;
	private TextBox textArea;
	private EpsgDialogBoxContent epsgContent;
	private Label epsgLabelText;

	public Epsg() {
		super();
		setWidth(FieldConstant.HUNDRED_PERCENT);
		label = new Label();
		editPanel = new VerticalPanel();
		editPanel.setWidth(FieldConstant.HUNDRED_PERCENT);
		HorizontalPanel firstLinePanel = new HorizontalPanel();
		firstLinePanel.setHorizontalAlignment(ALIGN_LEFT);
		firstLinePanel.setVerticalAlignment(ALIGN_MIDDLE);
		firstLinePanel.add(new Label("EPSG:"));
		textArea = new TextBox();
		textArea.setVisibleLength(EPSG_MAX_LENGTH);
		Button guide = new Button("...");
		guide.setTitle(MetadataMessage.INSTANCE.metadataEditingEpsgGuideText());
		guide.addClickHandler(this);
		firstLinePanel.add(textArea);
		firstLinePanel.add(guide);

		epsgLabelText = new Label();

		editPanel.add(firstLinePanel);
		editPanel.add(epsgLabelText);
		ElementUtil.hide(label);
		ElementUtil.hide(editPanel);
		add(editPanel);
		add(label);
	}

	@Override
	public void display(MetadataDTO metadata) {
		ElementUtil.show(label);
		ElementUtil.hide(editPanel);
		label.setText(StringUtil.trimToEmpty(metadata.getOtherPart().getCoordinateSystem()));
	}

	@Override
	public void reset() {
		textArea.setText("");
		label.setText("");
		ElementUtil.hide(epsgLabelText);
	}

	@Override
	public void flush(MetadataDTO metadata) {
		metadata.getOtherPart().setCoordinateSystem(StringUtil.trimToEmpty(textArea.getText()));
	}

	@Override
	public void edit(MetadataDTO metadata) {
		ElementUtil.show(editPanel);
		ElementUtil.hide(label);
	}

	@Override
	public void onClick(ClickEvent event) {
		ClientEspgList.getCodes(this);
	}

	@Override
	public void postLoadProcess(Object result) {
		epsgContent = new EpsgDialogBoxContent((ListBox) result, new ConfirmCallBack() {

			@Override
			public void confirm(boolean choice) {
				if (choice == true) {
					textArea.setText(epsgContent.getCode());
					String aux = epsgContent.getLabel();
					aux = aux.substring(aux.indexOf("("));
					epsgLabelText.setText(aux);
					ElementUtil.show(epsgLabelText);
				}
			}
		});

		DialogBoxTools.popUp(MetadataMessage.INSTANCE.metadataEditingEpsgGuideText(), epsgContent);
	}
}
