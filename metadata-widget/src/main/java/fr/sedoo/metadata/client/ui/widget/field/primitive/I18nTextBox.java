package fr.sedoo.metadata.client.ui.widget.field.primitive;

import java.util.ArrayList;

import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.TextBoxBase;

public abstract class I18nTextBox extends I18nField {

	public I18nTextBox(ArrayList<String> languages) {
		super(languages);
	}

	@Override
	protected TextBoxBase createEditor() {
		TextBox aux = new TextBox();
		aux.setVisibleLength(80);
		return aux;
	}

}
