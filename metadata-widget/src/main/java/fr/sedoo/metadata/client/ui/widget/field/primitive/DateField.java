package fr.sedoo.metadata.client.ui.widget.field.primitive;

import org.gwtbootstrap3.client.ui.Popover;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;

import fr.sedoo.commons.client.image.CommonBundle;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.widget.date.Iso19115DateBox;
import fr.sedoo.metadata.client.ui.widget.field.api.IsDisplay;
import fr.sedoo.metadata.client.ui.widget.field.api.IsEditor;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.shared.domain.MetadataDTO;

public abstract class DateField extends HorizontalPanel implements IsDisplay, IsEditor {
	protected Label label;
	protected Iso19115DateBox dateBox;
	protected Panel editPanel;

	public DateField() {
		super();
		setWidth(FieldConstant.HUNDRED_PERCENT);
		label = new Label();
		dateBox = new Iso19115DateBox();
		editPanel = new HorizontalPanel();
		((HorizontalPanel) editPanel).setHorizontalAlignment(ALIGN_LEFT);
		((HorizontalPanel) editPanel).setVerticalAlignment(ALIGN_MIDDLE);
		Popover popover = new Popover();
		Image helpImage = new Image(CommonBundle.INSTANCE.help());
		popover.add(helpImage);
		popover.setContent(MetadataMessage.INSTANCE.iso8601Format());
		popover.setTitle("");
		editPanel.add(dateBox);
		editPanel.add(popover);
		ElementUtil.hide(label);
		ElementUtil.hide(editPanel);
		add(editPanel);
		add(label);
	}

	@Override
	public void display(MetadataDTO metadata) {
		ElementUtil.show(label);
		ElementUtil.hide(editPanel);
	}

	@Override
	public void reset() {
		dateBox.setValue(null);
		label.setText("");
	}

	@Override
	public void edit(MetadataDTO metadata) {
		ElementUtil.show(editPanel);
		ElementUtil.hide(label);
	}
}
