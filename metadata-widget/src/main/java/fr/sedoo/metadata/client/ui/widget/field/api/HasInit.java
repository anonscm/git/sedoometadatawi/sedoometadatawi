package fr.sedoo.metadata.client.ui.widget.field.api;

public interface HasInit {

	void init();
}
