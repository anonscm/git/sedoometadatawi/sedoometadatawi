package fr.sedoo.metadata.client.ui.label;

import java.util.Map;

public abstract class PropertyLabelProvider {

	protected static void initLabels(String inputString, Map<String, String> labels) {
		String[] split = inputString.split("@");
		for (int i = 0; i < split.length; i++) {
			String[] split2 = split[i].split("\\|");
			labels.put(split2[0], split2[1]);
			labels.put(split2[0].toLowerCase().replace("_", ""), split2[1]);
		}
	}

}
