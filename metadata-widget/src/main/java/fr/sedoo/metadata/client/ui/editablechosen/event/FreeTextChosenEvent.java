package fr.sedoo.metadata.client.ui.editablechosen.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import fr.sedoo.metadata.client.ui.editablechosen.FreeTextChosenImpl;

public abstract class FreeTextChosenEvent<H extends EventHandler> extends GwtEvent<H> {

	private FreeTextChosenImpl chosen;

	public FreeTextChosenEvent(FreeTextChosenImpl chosen) {
		this.chosen = chosen;
	}

	public FreeTextChosenImpl getChosen() {
		return chosen;
	}
}
