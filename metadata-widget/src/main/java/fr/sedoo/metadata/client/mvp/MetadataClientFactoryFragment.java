package fr.sedoo.metadata.client.mvp;

import java.util.ArrayList;
import java.util.List;

import fr.sedoo.commons.client.mvp.BasicClientFactory;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.metadata.client.ui.widget.dialog.epsg.ClientEspgList;
import fr.sedoo.metadata.client.ui.widget.dialog.format.ClientFormatList;
import fr.sedoo.metadata.client.ui.widget.field.impl.keyword.ClientThesaurusList;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.metadata.shared.domain.misc.Iso6392LanguageProvider;

public class MetadataClientFactoryFragment {

	private BasicClientFactory clientFactory;

	public MetadataClientFactoryFragment(BasicClientFactory clientFactory) {
		this.clientFactory = clientFactory;
		ClientEspgList.setEventBus(clientFactory.getEventBus());
		ClientFormatList.setEventBus(clientFactory.getEventBus());
		ClientThesaurusList.setClientFactory(clientFactory);
	}

	/**
	 * Retourne uniquement l'anglais comme langue de métadonnées
	 */
	public List<String> getMetadataLanguages() {
		ArrayList<String> aux = new ArrayList<String>();
		aux.add(Iso6392LanguageProvider.ENGLISH);
		return aux;
	}

	/**
	 * Retourne uniquement l'anglais comme langue de données
	 */
	public List<String> getDataLanguages() {
		ArrayList<String> aux = new ArrayList<String>();
		aux.add(Iso6392LanguageProvider.ENGLISH);
		return aux;
	}

	public MetadataDTO createMetadata() {
		MetadataDTO metadataDTO = new MetadataDTO();
		String currentLanguage = Iso6392LanguageProvider.convertLocaleToIso6392(LocaleUtil.getCurrentLanguage(clientFactory));
		metadataDTO.getOtherPart().setMetadataLanguage(currentLanguage);
		return metadataDTO;
	}

}
