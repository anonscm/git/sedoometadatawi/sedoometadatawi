package fr.sedoo.metadata.client.ui.view;

import fr.sedoo.metadata.client.ui.view.presenter.DisplayPresenter;

public interface MetadataDisplayingView extends DTODisplayingView {

	void setDisplayPresenter(DisplayPresenter presenter);

}
