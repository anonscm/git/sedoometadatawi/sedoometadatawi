package fr.sedoo.metadata.client.ui.widget.field.api;

import fr.sedoo.metadata.shared.domain.MetadataDTO;

public interface IsDisplay extends HasReset {

	public final static String DISPLAY_MODE = "display";

	void display(MetadataDTO metadata);
}
