package fr.sedoo.metadata.client.ui.widget.table.misc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class HasIdentifierWrapperList extends ArrayList<HasIdentifierWrapper> {
	public HasIdentifierWrapperList() {
		this(new ArrayList<Object>());
	}

	public HasIdentifierWrapperList(List<? extends Object> src) {
		Long index = 0L;
		if (src != null) {
			Iterator<? extends Object> iterator = src.iterator();
			while (iterator.hasNext()) {
				Object object = iterator.next();
				add(new HasIdentifierWrapper(object, "" + index));
				index++;
			}
		}
	}

}
