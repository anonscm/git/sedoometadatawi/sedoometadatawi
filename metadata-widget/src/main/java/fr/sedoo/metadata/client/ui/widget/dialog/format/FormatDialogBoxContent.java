package fr.sedoo.metadata.client.ui.widget.dialog.format;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.widget.ConfirmCallBack;
import fr.sedoo.commons.client.widget.dialog.DialogBoxContent;

public class FormatDialogBoxContent extends Composite implements DialogBoxContent {

	private static UsualFormatDialogBoxContentUiBinder uiBinder = GWT.create(UsualFormatDialogBoxContentUiBinder.class);

	interface UsualFormatDialogBoxContentUiBinder extends UiBinder<Widget, FormatDialogBoxContent> {
	}

	@UiField(provided = true)
	ListBox formatListBox;

	@UiField
	Button ok;

	@UiField
	Button cancel;

	private DialogBox dialog;

	private ConfirmCallBack confirmCallback;

	private String resultValue;

	public FormatDialogBoxContent(ListBox listBox, ConfirmCallBack confirmCallback) {
		this.formatListBox = listBox;
		initWidget(uiBinder.createAndBindUi(this));
		this.confirmCallback = confirmCallback;
	}

	@UiHandler("cancel")
	void onCancelClicked(ClickEvent event) {
		if (dialog != null) {
			dialog.hide();
		}
	}

	@UiHandler("ok")
	void onOkClicked(ClickEvent event) {

		if (dialog != null) {
			dialog.hide();
			flush();
			confirmCallback.confirm(true);
		}
	}

	private void flush() {
		resultValue = formatListBox.getValue(formatListBox.getSelectedIndex());
	}

	public void setDialogBox(DialogBox dialog) {
		this.dialog = dialog;
	}

	@Override
	public String getPreferredHeight() {
		return "200px";
	}

	public String getResultValue() {
		return resultValue;
	}

}
