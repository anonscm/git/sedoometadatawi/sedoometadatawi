package fr.sedoo.metadata.client.ui.widget.field.impl.identifier;

import java.util.List;

import fr.sedoo.metadata.shared.domain.dto.IdentifiedResourceIdentifier;

public interface IdentifierComplementaryEditor {

	List<IdentifiedResourceIdentifier> filter(List<IdentifiedResourceIdentifier> identifiers);

	List<IdentifiedResourceIdentifier> flush(List<IdentifiedResourceIdentifier> identifiers);

}
