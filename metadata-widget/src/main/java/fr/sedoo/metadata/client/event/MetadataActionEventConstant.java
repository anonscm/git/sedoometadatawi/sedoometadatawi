package fr.sedoo.metadata.client.event;

public interface MetadataActionEventConstant 
{
	public final int METADATA_LIST_LOADING_EVENT = 1;
	public final int METADATA_PDF_LOADING_EVENT = 2;
	public final int METADATA_LOADING_EVENT = 3;
	public final int USER_LOGIN_EVENT = 4;
	public final int OBSERVATORIES_LOADING_EVENT = 5;
	public final int OBSERVATORY_SAVING_EVENT = 6;
	public final int DRAINAGE_BASIN_SAVING_EVENT = 7;
	public final int OBSERVATORY_CONTACT_SAVING_EVENT = 8;
	public final int DRAINAGE_BASIN_LOADING_EVENT = 9;
	public final int METADATA_DELETING_EVENT = 10;
	public final int METADATA_SAVING_EVENT = 11;
	public final int FORMAT_DELETING_EVENT = 12;
	public final int FORMAT_SAVING_EVENT = 13;
	public final int FORMAT_ADDING_EVENT = 14;
	public final int FORMAT_LOADING_EVENT = 15;
	public final int NEWS_LOADING_EVENT = 16;
	public final int METADATA_SEARCH_EVENT = 17;
	public final int MARKER_LOADING_EVENT = 18;
	public final int USER_SAVING_EVENT = 19;
	public final int USER_DELETING_EVENT = 20;
	public final int USER_LOADING_EVENT = 21;
	public final int MESSAGE_LOADING_EVENT = 22;
	public final int MESSAGE_SAVING_EVENT = 23;
	public final int MESSAGE_DELETING_EVENT = 24;
	public final int LOG_LOADING_EVENT = 25;
	public final int EPSG_LOADING_EVENT = 26;
	public final int OBSERVATORY_LOADING_EVENT = 27;
}
