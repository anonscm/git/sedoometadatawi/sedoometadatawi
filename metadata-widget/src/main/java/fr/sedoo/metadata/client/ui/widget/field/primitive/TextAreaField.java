package fr.sedoo.metadata.client.ui.widget.field.primitive;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;

import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.metadata.client.ui.widget.field.api.IsDisplay;
import fr.sedoo.metadata.client.ui.widget.field.api.IsEditor;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.shared.domain.MetadataDTO;

public abstract class TextAreaField extends HorizontalPanel implements IsDisplay, IsEditor {
	private Label label;
	private TextArea textArea;

	public TextAreaField() {
		super();
		setWidth(FieldConstant.HUNDRED_PERCENT);
		label = new Label();
		textArea = new TextArea();
		textArea.setVisibleLines(10);
		textArea.setWidth("90%");
		ElementUtil.hide(label);
		ElementUtil.hide(textArea);
		textArea.setVisible(false);
		label.setVisible(false);
		add(textArea);
		add(label);
	}

	@Override
	public void display(MetadataDTO metadata) {
		ElementUtil.show(label);
		ElementUtil.hide(textArea);
	}

	@Override
	public void reset() {
		textArea.setText("");
		label.setText("");
	}

	@Override
	public void flush(MetadataDTO metadata) {
	}

	@Override
	public void edit(MetadataDTO metadata) {
		ElementUtil.show(textArea);
		ElementUtil.hide(label);
	}
}
