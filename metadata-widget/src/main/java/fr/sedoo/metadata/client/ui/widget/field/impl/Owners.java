package fr.sedoo.metadata.client.ui.widget.field.impl;

import fr.sedoo.commons.metadata.utils.pdf.labelprovider.RoleLabelProvider;
import fr.sedoo.metadata.client.ui.widget.field.impl.contact.AbstractSingleRoleResourceContact;

public class Owners extends AbstractSingleRoleResourceContact {

	public Owners() {
		super();
	}

	public String getSingleRole() {
		return RoleLabelProvider.OWNER;
	}

}
