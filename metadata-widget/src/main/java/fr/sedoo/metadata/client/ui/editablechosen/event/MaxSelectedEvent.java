package fr.sedoo.metadata.client.ui.editablechosen.event;

import com.google.gwt.event.shared.EventHandler;

import fr.sedoo.metadata.client.ui.editablechosen.FreeTextChosenImpl;

public class MaxSelectedEvent extends FreeTextChosenEvent<MaxSelectedEvent.MaxSelectedHandler> {

	public interface MaxSelectedHandler extends EventHandler {
		void onMaxSelected(MaxSelectedEvent event);
	}

	public static Type<MaxSelectedHandler> TYPE = new Type<MaxSelectedHandler>();

	public static Type<MaxSelectedHandler> getType() {
		return TYPE;
	}

	public MaxSelectedEvent(FreeTextChosenImpl chosen) {
		super(chosen);
	}

	@Override
	public Type<MaxSelectedHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(MaxSelectedHandler handler) {
		handler.onMaxSelected(this);
	}
}