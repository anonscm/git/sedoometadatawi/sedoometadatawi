package fr.sedoo.metadata.client.ui.widget.dialog.epsg;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.widget.ConfirmCallBack;
import fr.sedoo.commons.client.widget.dialog.DialogBoxContent;

public class EpsgDialogBoxContent extends Composite implements DialogBoxContent {

	private static UsualFormatDialogBoxContentUiBinder uiBinder = GWT.create(UsualFormatDialogBoxContentUiBinder.class);

	interface UsualFormatDialogBoxContentUiBinder extends UiBinder<Widget, EpsgDialogBoxContent> {
	}

	@UiField(provided = true)
	ListBox epsgListBox;

	@UiField
	Button ok;

	@UiField
	Button cancel;

	private DialogBox dialog;

	private String label;

	private String code;

	private ConfirmCallBack confirmCallback;

	public EpsgDialogBoxContent(ListBox listBox, ConfirmCallBack confirmCallback) {
		this.epsgListBox = listBox;
		initWidget(uiBinder.createAndBindUi(this));
		this.confirmCallback = confirmCallback;
	}

	@UiHandler("cancel")
	void onCancelClicked(ClickEvent event) {
		if (dialog != null) {
			dialog.hide();
		}
	}

	@UiHandler("ok")
	void onOkClicked(ClickEvent event) {

		if (dialog != null) {
			dialog.hide();
			flush();
			confirmCallback.confirm(true);
		}
	}

	private void flush() {
		label = epsgListBox.getItemText(epsgListBox.getSelectedIndex());
		code = epsgListBox.getValue(epsgListBox.getSelectedIndex());
	}

	public void setDialogBox(DialogBox dialog) {
		this.dialog = dialog;
	}

	public String getCode() {
		return code;
	}

	public String getLabel() {
		return label;
	}

	@Override
	public String getPreferredHeight() {
		return "200px";
	}

}
