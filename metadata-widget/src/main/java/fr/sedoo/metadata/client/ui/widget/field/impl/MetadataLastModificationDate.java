package fr.sedoo.metadata.client.ui.widget.field.impl;

import fr.sedoo.metadata.client.ui.widget.field.primitive.LabelField;
import fr.sedoo.metadata.shared.domain.MetadataDTO;

/**
 * Field devoted to the uuid. The uuid cannot be edited. the uuid is only
 * displayed, even in a edit context.
 * 
 */
public class MetadataLastModificationDate extends LabelField {

	public MetadataLastModificationDate() {
		super();
	}

	@Override
	public void display(MetadataDTO metadata) {
		super.display(metadata.getOtherPart().getMetadataLastModificationDate());
	}

	@Override
	public void edit(MetadataDTO metadata) {
		display(metadata);
	}
}
