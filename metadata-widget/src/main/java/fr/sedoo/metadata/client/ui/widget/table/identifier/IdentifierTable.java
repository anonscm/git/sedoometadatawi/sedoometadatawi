package fr.sedoo.metadata.client.ui.widget.table.identifier;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;

import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.table.AbstractListTable;
import fr.sedoo.commons.client.widget.table.WaterMarkCell;
import fr.sedoo.commons.metadata.shared.ResourceIdentifier;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.shared.domain.dto.IdentifiedResourceIdentifier;

public class IdentifierTable extends AbstractListTable {

	private boolean editColumnInitialized = false;
	private boolean displayColumnInitialized = false;

	public IdentifierTable() {
		super();
		if (addItemLabel != null) {
			addItemLabel.setText(MetadataMessage.INSTANCE.metadataEditingResourceIdentifierAddItemText());
		}
	}

	protected void initColumns() {
	}

	public void enableEditMode() {
		getToolBarPanel().setVisible(true);
		if (editColumnInitialized == false) {
			initEditColumns();
			editColumnInitialized = true;
		}
	}

	public void enableDisplayMode() {
		getToolBarPanel().setVisible(false);
		if (displayColumnInitialized == false) {
			initDisplayColumns();
			displayColumnInitialized = true;
		}
	}

	@Override
	public void addItem() {
		addEmptyRow();
	}

	protected void initEditColumns() {
		Column<HasIdentifier, String> valueColumn = new Column<HasIdentifier, String>(new WaterMarkCell(MetadataMessage.INSTANCE.metadataEditingResourceIdentifierWaterMark(), "30", "120")) {
			@Override
			public String getValue(HasIdentifier identifiedString) {
				return ((IdentifiedResourceIdentifier) identifiedString).getCode();
			}
		};

		valueColumn.setFieldUpdater(new FieldUpdater<HasIdentifier, String>() {
			@Override
			public void update(int index, HasIdentifier identifiedString, String value) {
				((IdentifiedResourceIdentifier) identifiedString).setCode(value);
			}
		});

		Column<HasIdentifier, String> nameSpaceColumn = new Column<HasIdentifier, String>(new WaterMarkCell(MetadataMessage.INSTANCE.metadataEditingResourceNameSpaceWaterMark(), "30", "120")) {
			@Override
			public String getValue(HasIdentifier identifiedString) {
				return ((IdentifiedResourceIdentifier) identifiedString).getNameSpace();
			}
		};

		nameSpaceColumn.setFieldUpdater(new FieldUpdater<HasIdentifier, String>() {
			@Override
			public void update(int index, HasIdentifier identifiedString, String nameSpace) {
				((IdentifiedResourceIdentifier) identifiedString).setNameSpace(nameSpace);
			}
		});

		itemTable.addColumn(valueColumn, MetadataMessage.INSTANCE.metadataEditingResourceIdentifier());
		itemTable.setColumnWidth(valueColumn, 30.0, Unit.PX);
		itemTable.addColumn(nameSpaceColumn, MetadataMessage.INSTANCE.metadataEditingResourceNameSpace());
		itemTable.setColumnWidth(nameSpaceColumn, 30.0, Unit.PX);
		itemTable.addColumn(deleteColumn);
		itemTable.setColumnWidth(deleteColumn, 30.0, Unit.PX);
		setAddButtonEnabled(true);
	}

	protected void initDisplayColumns() {

		TextColumn<HasIdentifier> displayColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier identifier) {
				String aux = StringUtil.trimToEmpty(((IdentifiedResourceIdentifier) identifier).getNameSpace());
				if (StringUtil.isNotEmpty(aux)) {
					aux = " (" + aux + ")";
				}
				return "- " + ((ResourceIdentifier) identifier).getCode() + aux;
			}
		};
		itemTable.addColumn(displayColumn);
		itemTable.setColumnWidth(displayColumn, 100.0, Unit.PX);

	}

	protected IdentifiedResourceIdentifier getDefaultIdentifiedString() {

		IdentifiedResourceIdentifier identifier = new IdentifiedResourceIdentifier();
		identifier.setCode("");
		identifier.setNameSpace("");
		return identifier;
	}

	public void reset() {
		List<IdentifiedResourceIdentifier> aux = new ArrayList<IdentifiedResourceIdentifier>();
		init(aux);
	}

	public void addEmptyRow() {
		List<HasIdentifier> newValues = new ArrayList<HasIdentifier>();
		Iterator<? extends HasIdentifier> iterator = model.iterator();
		while (iterator.hasNext()) {
			HasIdentifier aux = (HasIdentifier) iterator.next();
			newValues.add(aux);
		}
		newValues.add(getDefaultIdentifiedString());
		init(newValues);
	}

	@Override
	public void presenterDelete(HasIdentifier hasId) {
		String id = hasId.getIdentifier();
		removeRow(id);
	}

	@Override
	public void presenterEdit(HasIdentifier hasId) {
		// Nothing to do in this case, the edition is done via updater
	}

	@Override
	public String getAddItemText() {
		return MetadataMessage.INSTANCE.metadataEditingResourceIdentifierAddItemText();
	}

}
