package fr.sedoo.metadata.client.ui.view;

import com.google.gwt.user.client.ui.ResizeComposite;

import fr.sedoo.commons.client.image.CommonBundle;

public class AbstractStyledResizeComposite extends ResizeComposite {

	public AbstractStyledResizeComposite() {
		CommonBundle.INSTANCE.css().ensureInjected();
		// CustomResources.RESOURCES.css().ensureInjected();
	}

	protected void applyCommonStyle() {
		getElement().getStyle().setProperty("backgroundColor", "#ffffff");

	}

}
