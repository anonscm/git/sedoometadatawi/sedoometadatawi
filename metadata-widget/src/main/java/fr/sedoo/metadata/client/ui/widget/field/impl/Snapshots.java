package fr.sedoo.metadata.client.ui.widget.field.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.user.client.ui.HorizontalPanel;

import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.widget.field.api.IsDisplay;
import fr.sedoo.metadata.client.ui.widget.field.api.IsEditor;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.client.ui.widget.field.impl.snapshot.SnapshotComplementaryEditor;
import fr.sedoo.metadata.client.ui.widget.table.snapshot.SnapshotTable;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.metadata.shared.domain.misc.IdentifiedDescribedString;

public class Snapshots extends HorizontalPanel implements IsDisplay, IsEditor {
	private SnapshotTable table;
	private List<SnapshotComplementaryEditor> complementaryEditors = new ArrayList<SnapshotComplementaryEditor>();

	public Snapshots() {
		super();
		setWidth(FieldConstant.HUNDRED_PERCENT);
		String linkWatermark = MetadataMessage.INSTANCE.metadataEditingSnapshotDefaultValue();
		String labelWaterMark = MetadataMessage.INSTANCE.metadataEditingSnapshotDescriptionDefaultValue();
		String linkHeader = MetadataMessage.INSTANCE.metadataEditingURLTableURLHeader();
		String labelHeader = MetadataMessage.INSTANCE.metadataEditingSnapshotTableDescriptionHeader();
		String addItemText = MetadataMessage.INSTANCE.metadataEditingSnapshotTableAddItemText();

		table = new SnapshotTable(linkWatermark, labelWaterMark, addItemText, linkHeader, labelHeader);
		table.setAskDeletionConfirmation(false);
		ElementUtil.hide(table);
		table.setVisible(false);
		add(table);
	}

	public Snapshots(List<SnapshotComplementaryEditor> complementaryEditors) {
		this();
		this.complementaryEditors = complementaryEditors;
	}

	@Override
	public void display(MetadataDTO metadata) {
		table.enableDisplayMode();
		table.init(filter(metadata.getIdentificationPart().getSnapshots()));
		ElementUtil.show(table);
	}

	@Override
	public void reset() {
		table.reset();
	}

	@Override
	public void flush(MetadataDTO metadata) {
		// we clone the list
		List<IdentifiedDescribedString> currentSnapshots = new ArrayList<IdentifiedDescribedString>();
		currentSnapshots.addAll((List<IdentifiedDescribedString>) table.getModel());

		Iterator<SnapshotComplementaryEditor> iterator = complementaryEditors.iterator();
		while (iterator.hasNext()) {
			SnapshotComplementaryEditor currentEditor = (SnapshotComplementaryEditor) iterator.next();
			currentSnapshots = currentEditor.flush(currentSnapshots);
		}
		metadata.getIdentificationPart().setSnapshots(currentSnapshots);
	}

	@Override
	public void edit(MetadataDTO metadata) {
		table.enableEditMode();
		table.init(filter(metadata.getIdentificationPart().getSnapshots()));
		ElementUtil.show(table);
	}

	private List<IdentifiedDescribedString> filter(List<IdentifiedDescribedString> snapshots) {
		// we clone the list
		List<IdentifiedDescribedString> currentSnapshots = new ArrayList<IdentifiedDescribedString>();
		currentSnapshots.addAll(snapshots);
		Iterator<SnapshotComplementaryEditor> iterator = complementaryEditors.iterator();
		while (iterator.hasNext()) {
			SnapshotComplementaryEditor currentEditor = (SnapshotComplementaryEditor) iterator.next();
			currentSnapshots = currentEditor.filter(currentSnapshots);
		}
		return currentSnapshots;
	}
}
