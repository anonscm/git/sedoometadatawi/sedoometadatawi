package fr.sedoo.metadata.client.ui.widget.dialog.contact;

import fr.sedoo.metadata.shared.domain.dto.PersonDTO;

public interface PersonDTOEditor {

	PersonDTO getValue();

}
