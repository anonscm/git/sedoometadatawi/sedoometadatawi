package fr.sedoo.metadata.client.ui.widget.field.impl.keyword;

import com.google.gwt.user.cellview.client.Column;

public abstract class ClickableTextColumn<T> extends Column<T, String> {

	/**
	 * Construct a new TextColumn.
	 */
	public ClickableTextColumn() {
		super(new PointedClickableTextCell());
	}

}
