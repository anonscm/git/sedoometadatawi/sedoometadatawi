package fr.sedoo.metadata.client.ui.widget.field.impl.snapshot;

import java.util.List;

import fr.sedoo.metadata.shared.domain.misc.IdentifiedDescribedString;

public interface SnapshotComplementaryEditor {

	List<IdentifiedDescribedString> filter(List<IdentifiedDescribedString> snapshots);

	List<IdentifiedDescribedString> flush(List<IdentifiedDescribedString> snapshots);

}
