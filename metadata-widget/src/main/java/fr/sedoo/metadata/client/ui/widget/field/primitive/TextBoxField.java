package fr.sedoo.metadata.client.ui.widget.field.primitive;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;

import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.metadata.client.ui.widget.field.api.IsDisplay;
import fr.sedoo.metadata.client.ui.widget.field.api.IsEditor;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.shared.domain.MetadataDTO;

public abstract class TextBoxField extends HorizontalPanel implements IsDisplay, IsEditor {
	protected Label label;
	protected TextBox textBox;

	public TextBoxField() {
		super();
		setWidth(FieldConstant.HUNDRED_PERCENT);
		label = new Label();
		textBox = new TextBox();
		textBox.setVisibleLength(80);
		ElementUtil.hide(label);
		ElementUtil.hide(textBox);
		textBox.setVisible(false);
		label.setVisible(false);
		add(textBox);
		add(label);
	}

	@Override
	public void display(MetadataDTO metadata) {
		ElementUtil.show(label);
		ElementUtil.hide(textBox);
	}

	@Override
	public void reset() {
		textBox.setText("");
		label.setText("");
	}

	@Override
	public void flush(MetadataDTO metadata) {
	}

	@Override
	public void edit(MetadataDTO metadata) {
		ElementUtil.show(textBox);
		ElementUtil.hide(label);
	}
}
