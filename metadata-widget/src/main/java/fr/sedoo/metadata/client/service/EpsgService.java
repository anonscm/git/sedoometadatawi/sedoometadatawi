package fr.sedoo.metadata.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.metadata.shared.domain.entity.EpsgCode;

@RemoteServiceRelativePath("epsg")
public interface EpsgService extends RemoteService {

	ArrayList<EpsgCode> findAll() throws ServiceException;
}
