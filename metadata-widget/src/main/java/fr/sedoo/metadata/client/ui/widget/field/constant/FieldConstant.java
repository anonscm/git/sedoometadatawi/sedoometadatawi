package fr.sedoo.metadata.client.ui.widget.field.constant;

public interface FieldConstant {

	String HUNDRED_PERCENT = "100%";
	int DISPLAY_MODE = 1;
	int EDIT_MODE = 2;
	String TITLE = "TITLE";
	String ABSTRACT = "ABSTRACT";
	String IDENTIFIERS = "IDENTIFIERS";
	String STATUS = "STATUS";
	String STARTING_DATE = "STARTING_DATE";
	String ENDING_DATE = "ENDING_DATE";
	String UPDATE_RYTHM = "UPDATE_RYTHM";
	String CREATION_DATE = "CREATION_DATE";
	String LAST_REVISION_DATE = "LAST_REVISION_DATE";
	String PUBLICATION_DATE = "PUBLICATION_DATE";
	String PUBLIC_ACCESS_LIMITATIONS = "PUBLIC_ACCESS_LIMITATIONS";
	String USE_CONDITIONS = "USE_CONDITIONS";
	String UUID = "UUID";
	String METADATA_LAST_MODIFICATION_DATE = "METADATA_LAST_MODIFICATION_DATE";
	String DATA_LANGUAGES = "DATA_LANGUAGES";
	String METADATA_MAIN_LANGUAGE = "METADATA_MAIN_LANGUAGE";
	String CHARSET = "CHARSET";
	String COORDINATE_SYSTEM = "COORDINATE_SYSTEM";
	String FORMAT = "FORMAT";
	String KEYWORDS = "KEYWORDS";
	String SNAPSHOTS = "SNAPSHOTS";
	String INTERNET_LINKS = "INTERNET_LINKS";
	String GENEALOGY = "GENEALOGY";
	String PURPOSE = "PURPOSE";
	String ALTERNATE_TITLE = "ALTERNATE_TITLE";
	String LOGO = "LOGO";

	String UNSELECTED_ID = "NONE";

	String METADATA_SECTION_STYLENAME = "metadataSection";
	String METADATA_LEFT_COLUMN_STYLENAME = "metadataLeftColumn";
	String METADATA_RIGHT_COLUMN_STYLENAME = "metadataRightColumn";
	String METADATA_ROW_SPACING_STYLENAME = "metadataRowSpacing";
	String METADATA_FULL_LINE_COLUMN_STYLENAME = "metadataFullLineColumn";

}
