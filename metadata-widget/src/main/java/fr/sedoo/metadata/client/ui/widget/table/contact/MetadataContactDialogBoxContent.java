package fr.sedoo.metadata.client.ui.widget.table.contact;

import java.util.List;

import fr.sedoo.metadata.client.ui.widget.dialog.contact.PersonDialogBoxContent;
import fr.sedoo.metadata.shared.domain.dto.MetadataContactDTO;
import fr.sedoo.metadata.shared.domain.dto.PersonDTO;

public class MetadataContactDialogBoxContent extends PersonDialogBoxContent {

	public MetadataContactDialogBoxContent(List<String> roles) {
		super(roles);
	}

	@Override
	public PersonDTO createDTO() {
		return new MetadataContactDTO();
	}

}
