package fr.sedoo.metadata.client.mvp;

import java.util.ArrayList;

import fr.sedoo.commons.client.mvp.BasicClientFactory;
import fr.sedoo.metadata.client.ui.view.FormatListView;
import fr.sedoo.metadata.shared.domain.MetadataDTO;

public interface MetadataClientFactory extends BasicClientFactory {

	/**
	 * This is the list of all languages that the application allows for
	 * metadata editing. <br>
	 * The values must respect ISO-639-2 standard (e.g. <i>eng</i>, <i>fre</i>,
	 * ...)
	 * 
	 * @return
	 */
	ArrayList<String> getMetadataLanguages();

	ArrayList<String> getDataLanguages();

	MetadataDTO createMetadata();

	FormatListView getFormatListView();
}
