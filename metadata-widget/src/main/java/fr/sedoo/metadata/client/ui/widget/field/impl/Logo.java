package fr.sedoo.metadata.client.ui.widget.field.impl;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.TextBox;

import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.DialogBoxTools;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.widget.field.api.IsDisplay;
import fr.sedoo.metadata.client.ui.widget.field.api.IsEditor;
import fr.sedoo.metadata.client.ui.widget.field.impl.snapshot.SnapshotComplementaryEditor;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.metadata.shared.domain.misc.IdentifiedDescribedString;

public class Logo extends HorizontalPanel implements IsDisplay, IsEditor, SnapshotComplementaryEditor, ClickHandler {
	public final static String LOGO = "logo";

	private static final String DEFAULT_WIDTH = "50px";
	private static final String DEFAULT_HEIGHT = "50px";

	protected Image image;
	protected TextBox textBox;
	protected String width = DEFAULT_WIDTH;
	protected String height = DEFAULT_HEIGHT;

	private Button viewButton;

	public Logo() {
		this(DEFAULT_HEIGHT, DEFAULT_WIDTH);
	}

	public Logo(String height, String width) {
		super();
		setHorizontalAlignment(ALIGN_LEFT);
		setVerticalAlignment(ALIGN_MIDDLE);
		image = new Image();
		image.setWidth(width);
		image.setHeight(height);
		textBox = new TextBox();
		textBox.setVisibleLength(80);
		ElementUtil.hide(image);
		ElementUtil.hide(textBox);
		viewButton = new Button(CommonMessages.INSTANCE.view());
		viewButton.addClickHandler(this);
		viewButton.addStyleName("5pxspace");
		add(textBox);
		add(viewButton);
		add(image);
	}

	@Override
	public void reset() {
		textBox.setText("");
		ElementUtil.hide(image);
	}

	@Override
	public void flush(MetadataDTO metadata) {
		// Nothing is flushed this way

	}

	@Override
	public void edit(MetadataDTO metadata) {
		ElementUtil.show(textBox);
		ElementUtil.show(viewButton);
		ElementUtil.hide(image);
		textBox.setText("");
		Iterator<IdentifiedDescribedString> iterator = metadata.getIdentificationPart().getSnapshots().iterator();
		while (iterator.hasNext()) {
			IdentifiedDescribedString current = (IdentifiedDescribedString) iterator.next();
			if (isLogo(current)) {
				textBox.setText(current.getLink());
				return;
			}
		}
	}

	@Override
	public void display(MetadataDTO metadata) {
		ElementUtil.show(image);
		ElementUtil.hide(textBox);
		ElementUtil.hide(viewButton);
		Iterator<IdentifiedDescribedString> iterator = metadata.getIdentificationPart().getSnapshots().iterator();
		while (iterator.hasNext()) {
			IdentifiedDescribedString current = (IdentifiedDescribedString) iterator.next();
			if (isLogo(current)) {
				if (StringUtil.isNotEmpty(current.getLink())) {
					image.setUrl(current.getLink());
					image.getElement().getStyle().setCursor(Cursor.POINTER);
					final String aux = current.getLink();
					image.addClickHandler(new ClickHandler() {
						@Override
						public void onClick(ClickEvent event) {
							DialogBoxTools.popUpImage(MetadataMessage.INSTANCE.metadataEditingLogo(), aux);
						}
					});
					ElementUtil.show(image);
				} else {
					ElementUtil.hide(image);
				}
				return;
			}
		}
		// No logo found
		ElementUtil.hide(image);
	}

	private boolean isLogo(IdentifiedDescribedString current) {
		return StringUtil.trimToEmpty(current.getLabel()).toLowerCase().contains(LOGO);
	}

	@Override
	public List<IdentifiedDescribedString> filter(List<IdentifiedDescribedString> snapshots) {
		ListIterator<IdentifiedDescribedString> iterator = snapshots.listIterator();
		while (iterator.hasNext()) {
			IdentifiedDescribedString current = (IdentifiedDescribedString) iterator.next();
			if (isLogo(current)) {
				iterator.remove();
			}
		}
		return snapshots;
	}

	@Override
	public List<IdentifiedDescribedString> flush(List<IdentifiedDescribedString> snapshots) {
		String url = StringUtil.trimToEmpty(textBox.getText());
		ListIterator<IdentifiedDescribedString> iterator = snapshots.listIterator();
		while (iterator.hasNext()) {
			IdentifiedDescribedString current = (IdentifiedDescribedString) iterator.next();
			if (isLogo(current)) {

				if (StringUtil.isEmpty(url)) {
					// No value indicated, we delete the line
					iterator.remove();
				} else {
					// We update the line
					current.setValue(url);
				}
				return snapshots;
			}
		}
		// No existing logo line
		if (StringUtil.isNotEmpty(url)) {
			snapshots.add(new IdentifiedDescribedString(new Long(snapshots.size()), url, LOGO));
		}
		return snapshots;
	}

	@Override
	public void onClick(ClickEvent event) {
		String aux = textBox.getText().trim();
		if (StringUtil.isNotEmpty(aux)) {
			DialogBoxTools.popUpImage(CommonMessages.INSTANCE.view(), aux);
		}
	}

}
