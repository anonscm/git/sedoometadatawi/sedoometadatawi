package fr.sedoo.metadata.client.ui.widget.field.impl;

import java.util.ArrayList;

import fr.sedoo.metadata.client.ui.widget.field.primitive.I18nTextArea;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.metadata.shared.domain.dto.I18nString;

public class PublicAccessLimitations extends I18nTextArea {

	public PublicAccessLimitations(ArrayList<String> languages) {
		super(languages);
	}

	@Override
	public void display(MetadataDTO metadata) {
		super.display(metadata.getConstraintPart().getPublicAccessLimitations().getDisplayValue(languages));
	}

	@Override
	public void flush(MetadataDTO metadata) {

		I18nString aux = new I18nString();
		aux.setI18nValues(getI18nValues());
		metadata.getConstraintPart().setPublicAccessLimitations(aux);
	}

	@Override
	public void edit(MetadataDTO metadata) {
		super.edit(metadata.getConstraintPart().getPublicAccessLimitations().getI18nValues());

	}
}
