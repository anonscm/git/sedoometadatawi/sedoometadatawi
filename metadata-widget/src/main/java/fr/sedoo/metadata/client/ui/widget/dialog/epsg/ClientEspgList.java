package fr.sedoo.metadata.client.ui.widget.dialog.epsg;

import java.util.ArrayList;
import java.util.Iterator;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;

import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.event.ActionEventConstants;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.widget.DialogBoxTools;
import fr.sedoo.metadata.client.service.EpsgService;
import fr.sedoo.metadata.client.service.EpsgServiceAsync;
import fr.sedoo.metadata.shared.domain.entity.EpsgCode;

public class ClientEspgList {

	private final static EpsgServiceAsync EPSG_SERVICE = GWT.create(EpsgService.class);
	private static ListBox listBox = new ListBox();
	private static boolean loaded = false;
	private static ArrayList<LoadCallBack> callBacks = new ArrayList<LoadCallBack>();
	private static EventBus eventBus;

	public static void setEventBus(EventBus eventBus) {
		ClientEspgList.eventBus = eventBus;
	}

	public static void getCodes(LoadCallBack callBack) {
		if (loaded) {
			callBack.postLoadProcess(listBox);
		} else {
			callBacks.add(callBack);
			loadCodes();
		}
	}

	public static void loadCodes() {
		final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventBus.fireEvent(e);

		EPSG_SERVICE.findAll(new AsyncCallback<ArrayList<EpsgCode>>() {

			@Override
			public void onSuccess(ArrayList<EpsgCode> codes) {

				Iterator<EpsgCode> iterator = codes.iterator();
				while (iterator.hasNext()) {
					EpsgCode current = (EpsgCode) iterator.next();
					listBox.addItem("EPSG:" + current.getCode() + " (" + current.getLabel() + ")", "" + current.getCode());
				}
				eventBus.fireEvent(e.getEndingEvent());
				loaded = true;
				Iterator<LoadCallBack> secondIterator = callBacks.iterator();
				while (secondIterator.hasNext()) {
					getCodes(secondIterator.next());
				}
				callBacks.clear();
			}

			@Override
			public void onFailure(Throwable caught) {
				DialogBoxTools.modalAlert(CommonMessages.INSTANCE.error(), CommonMessages.INSTANCE.anErrorHasOccurred() + " : " + caught.getMessage());
				// PortailRBV.getClientFactory().getEventBus().fireEvent(startEvent.getEndingEvent());
			}
		});

	}

}
