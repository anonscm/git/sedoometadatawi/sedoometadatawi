package fr.sedoo.metadata.client.ui.widget.field.impl;

import org.gwtbootstrap3.client.ui.Popover;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.sedoo.commons.client.image.CommonBundle;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.metadata.shared.constant.Iso19139Constants;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.widget.date.Iso19115DateBox;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.client.ui.widget.field.primitive.DateField;
import fr.sedoo.metadata.client.util.Iso19115DateUtil;
import fr.sedoo.metadata.shared.domain.MetadataDTO;

public class EndDate extends DateField implements ClickHandler {

	private RadioButton explicitEndDate;
	private RadioButton nowEndDate;

	public EndDate() {
		super();
		clear();
		VerticalPanel aux = new VerticalPanel();
		aux.setWidth(FieldConstant.HUNDRED_PERCENT);
		HorizontalPanel firstLinePanel = new HorizontalPanel();
		firstLinePanel.setHorizontalAlignment(ALIGN_LEFT);
		firstLinePanel.setVerticalAlignment(ALIGN_MIDDLE);
		explicitEndDate = new RadioButton("toggle");
		firstLinePanel.add(explicitEndDate);

		dateBox = new Iso19115DateBox();
		editPanel = new HorizontalPanel();
		Popover popover = new Popover();
		Image helpImage = new Image(CommonBundle.INSTANCE.help());
		popover.add(helpImage);
		popover.setContent(MetadataMessage.INSTANCE.iso8601Format());
		popover.setTitle("");
		firstLinePanel.add(dateBox);
		firstLinePanel.add(popover);

		HorizontalPanel secondLinePanel = new HorizontalPanel();
		secondLinePanel.setHorizontalAlignment(ALIGN_LEFT);
		secondLinePanel.setVerticalAlignment(ALIGN_MIDDLE);
		nowEndDate = new RadioButton("toggle");
		secondLinePanel.add(nowEndDate);
		secondLinePanel.add(new Label(MetadataMessage.INSTANCE.now()));

		aux.add(firstLinePanel);
		aux.add(secondLinePanel);
		editPanel = aux;
		label = new Label();
		add(label);
		add(aux);

		ElementUtil.hide(label);
		ElementUtil.hide(editPanel);

		nowEndDate.addClickHandler(this);
		explicitEndDate.addClickHandler(this);

	}

	@Override
	public void reset() {
		super.reset();
		explicitEndDate.setValue(true);
		explicitEndDate.setFormValue("checked");
		dateBox.setEnabled(true);
	}

	@Override
	public void display(MetadataDTO metadata) {
		super.display(metadata);
		String aux = StringUtil.trimToEmpty(metadata.getTemporalExtentPart().getEndDate());
		if (aux.compareTo(Iso19139Constants.NOW) == 0) {
			label.setText(MetadataMessage.INSTANCE.now());
		} else {
			label.setText(aux);
		}
	}

	@Override
	public void flush(MetadataDTO metadata) {
		if (nowEndDate.getValue() == true) {
			metadata.getTemporalExtentPart().setEndDate(Iso19139Constants.NOW);
		} else {
			if (dateBox.getValue() == null) {
				metadata.getTemporalExtentPart().setEndDate(null);
			} else {
				metadata.getTemporalExtentPart().setEndDate(Iso19115DateUtil.getDateFormater().format(dateBox.getValue()));
			}
		}
	}

	@Override
	public void edit(MetadataDTO metadata) {
		super.edit(metadata);

		String aux = StringUtil.trimToEmpty(metadata.getTemporalExtentPart().getEndDate());
		if (StringUtil.isNotEmpty(aux)) {
			if (aux.compareTo(Iso19139Constants.NOW) == 0) {
				dateBox.setEnabled(false);
				nowEndDate.setValue(true);
				nowEndDate.setChecked(true);
				explicitEndDate.setValue(false);
			} else {
				dateBox.setEnabled(true);
				explicitEndDate.setValue(true);
				try {
					dateBox.setValue(Iso19115DateUtil.getDateFormater().parse(aux));
				} catch (IllegalArgumentException e) {
					dateBox.setValue(null);
				}
			}
		} else {
			dateBox.setEnabled(true);
			explicitEndDate.setValue(true);
			explicitEndDate.setChecked(true);
			dateBox.setValue(null);
		}
	}

	@Override
	public void onClick(ClickEvent event) {
		if (event.getSource() == nowEndDate) {
			dateBox.setEnabled(false);
		} else if (event.getSource() == explicitEndDate) {
			dateBox.setEnabled(true);
		}
	}

}
