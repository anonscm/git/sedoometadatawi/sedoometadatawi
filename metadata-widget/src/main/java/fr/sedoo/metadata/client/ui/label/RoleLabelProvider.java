package fr.sedoo.metadata.client.ui.label;

import java.util.HashMap;

import fr.sedoo.metadata.client.message.MetadataMessage;

public class RoleLabelProvider extends PropertyLabelProvider {

	public static final String AUTHOR = "author";
	public static final String ORIGINATOR = "originator";
	public static final String DISTRIBUTOR = "distributor";
	public static final String OWNER = "owner";
	public static final String USER = "user";
	public static final String POINT_OF_CONTACT = "pointOfContact";
	public static final String RESOURCE_PROVIDER = "resourceProvider";
	public static final String CUSTODIAN = "custodian";
	public static final String PROCESSOR = "processor";
	public static final String PRINCIPAL_INVESTIGATOR = "principalInvestigator";
	public static final String PUBLISHER = "publisher";

	private static HashMap<String, String> labels = null;
	private static HashMap<String, String> tooltips = new HashMap<String, String>();
	static {
		tooltips.put(AUTHOR.toLowerCase().replace("_", ""), MetadataMessage.INSTANCE.authorRoleTooltip());
		tooltips.put(ORIGINATOR.toLowerCase().replace("_", ""), MetadataMessage.INSTANCE.originatorRoleTooltip());
		tooltips.put(RESOURCE_PROVIDER.toLowerCase().replace("_", ""), MetadataMessage.INSTANCE.resourceProviderRoleTooltip());
		tooltips.put(DISTRIBUTOR.toLowerCase().replace("_", ""), MetadataMessage.INSTANCE.distributorRoleTooltip());
		tooltips.put(CUSTODIAN.toLowerCase().replace("_", ""), MetadataMessage.INSTANCE.custodianRoleTooltip());
		tooltips.put(PROCESSOR.toLowerCase().replace("_", ""), MetadataMessage.INSTANCE.processorRoleTooltip());
		tooltips.put(USER.toLowerCase().replace("_", ""), MetadataMessage.INSTANCE.userRoleTooltip());
		tooltips.put(PUBLISHER.toLowerCase().replace("_", ""), MetadataMessage.INSTANCE.publisherRoleTooltip());
		tooltips.put(POINT_OF_CONTACT.toLowerCase().replace("_", ""), MetadataMessage.INSTANCE.pointOfContactRoleTooltip());
		tooltips.put(OWNER.toLowerCase().replace("_", ""), MetadataMessage.INSTANCE.ownerRoleTooltip());
		tooltips.put(PRINCIPAL_INVESTIGATOR.toLowerCase().replace("_", ""), MetadataMessage.INSTANCE.principalInvestigatorRoleTooltip());
	}

	public static String getLabel(String key) {
		if (labels == null) {
			labels = new HashMap<String, String>();
			String aux = MetadataMessage.INSTANCE.personRoleItems();
			initLabels(aux, labels);
		}
		return labels.get(key.toLowerCase().replace("_", ""));
	}

	public static String getTooltip(String role) {
		return tooltips.get(role.toLowerCase().replace("_", ""));
	}

}
