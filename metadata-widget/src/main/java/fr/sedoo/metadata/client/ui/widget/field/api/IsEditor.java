package fr.sedoo.metadata.client.ui.widget.field.api;

import fr.sedoo.metadata.shared.domain.MetadataDTO;

public interface IsEditor extends HasReset {

	public final static String EDIT_MODE = "edit";

	void edit(MetadataDTO metadata);

	void flush(MetadataDTO metadata);
}
