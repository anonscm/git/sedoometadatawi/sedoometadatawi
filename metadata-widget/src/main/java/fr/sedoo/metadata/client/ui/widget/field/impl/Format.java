package fr.sedoo.metadata.client.ui.widget.field.impl;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.util.SeparatorUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.ConfirmCallBack;
import fr.sedoo.commons.client.widget.DialogBoxTools;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.widget.dialog.format.ClientFormatList;
import fr.sedoo.metadata.client.ui.widget.dialog.format.FormatDialogBoxContent;
import fr.sedoo.metadata.client.ui.widget.field.api.IsDisplay;
import fr.sedoo.metadata.client.ui.widget.field.api.IsEditor;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.metadata.shared.domain.dto.FormatDTO;

public class Format extends HorizontalPanel implements IsDisplay, IsEditor, ClickHandler, LoadCallBack {
	private Label label;
	private VerticalPanel editPanel;
	private TextBox nameTextBox;
	private TextBox versionTextBox;
	private FormatDialogBoxContent formatContent;

	public Format() {
		super();
		setWidth(FieldConstant.HUNDRED_PERCENT);
		label = new Label();
		editPanel = new VerticalPanel();
		editPanel.setWidth(FieldConstant.HUNDRED_PERCENT);

		Grid formatEditingGrid = new Grid(2, 3);
		nameTextBox = new TextBox();
		nameTextBox.setVisibleLength(40);
		versionTextBox = new TextBox();
		versionTextBox.setVisibleLength(20);
		Button guide = new Button("...");
		guide.setTitle(MetadataMessage.INSTANCE.metadataEditingFormatGuideText());
		guide.addClickHandler(this);
		formatEditingGrid.setWidget(0, 0, new Label(MetadataMessage.INSTANCE.metadataEditingFormatName()));
		formatEditingGrid.setWidget(1, 0, new Label(MetadataMessage.INSTANCE.metadataEditingFormatVersion()));
		formatEditingGrid.setWidget(0, 1, nameTextBox);
		formatEditingGrid.setWidget(1, 1, versionTextBox);
		formatEditingGrid.setWidget(0, 2, guide);

		editPanel.add(formatEditingGrid);
		ElementUtil.hide(label);
		ElementUtil.hide(editPanel);
		add(editPanel);
		add(label);
	}

	@Override
	public void display(MetadataDTO metadata) {
		ElementUtil.show(label);
		ElementUtil.hide(editPanel);
		FormatDTO format = metadata.getOtherPart().getFormat();
		if (format != null) {
			StringBuilder sb = new StringBuilder();
			sb.append(StringUtil.trimToEmpty(format.getName()));
			if (format.getVersions().isEmpty() == false) {
				sb.append(" (" + format.getVersions().iterator().next() + ")");
			}
			label.setText(sb.toString().trim());
		} else {
			label.setText("");
		}

	}

	@Override
	public void reset() {
		nameTextBox.setText("");
		versionTextBox.setText("");
		label.setText("");
	}

	@Override
	public void flush(MetadataDTO metadata) {
		if (StringUtil.isNotEmpty(nameTextBox.getText())) {
			FormatDTO format = new FormatDTO();
			format.setName(StringUtil.trimToEmpty(nameTextBox.getText()));
			if (StringUtil.isNotEmpty(versionTextBox.getText())) {
				format.setStringVersions(StringUtil.trimToEmpty(versionTextBox.getText()));
			}
			metadata.getOtherPart().setFormat(format);
		} else {
			metadata.getOtherPart().setFormat(null);
		}

	}

	@Override
	public void edit(MetadataDTO metadata) {
		ElementUtil.show(editPanel);
		ElementUtil.hide(label);
		FormatDTO format = metadata.getOtherPart().getFormat();
		if (format == null) {
			nameTextBox.setText("");
			versionTextBox.setText("");
		} else {
			nameTextBox.setText(format.getName());
			if (format.getVersions().isEmpty() == false) {
				versionTextBox.setText(format.getVersions().iterator().next());
			}
		}
	}

	@Override
	public void onClick(ClickEvent event) {
		ClientFormatList.getFormats(this);
	}

	@Override
	public void postLoadProcess(Object result) {
		formatContent = new FormatDialogBoxContent((ListBox) result, new ConfirmCallBack() {
			@Override
			public void confirm(boolean choice) {
				if (choice == true) {

					String format = formatContent.getResultValue();
					if (format.length() > 0) {
						nameTextBox.setText("");
						versionTextBox.setText("");
						String[] split = format.split(SeparatorUtil.AROBAS_SEPARATOR);
						nameTextBox.setText(split[0]);
						if (split.length > 1) {
							versionTextBox.setText(split[1]);
						}
					}
				}
			}
		});

		DialogBoxTools.popUp(MetadataMessage.INSTANCE.metadataEditingFormatGuideText(), formatContent);
	}
}
