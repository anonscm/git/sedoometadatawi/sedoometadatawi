package fr.sedoo.metadata.client.ui.widget.field.impl;

import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.metadata.client.ui.widget.field.primitive.DateField;
import fr.sedoo.metadata.client.util.Iso19115DateUtil;
import fr.sedoo.metadata.shared.domain.MetadataDTO;

public class LastRevisionDate extends DateField {

	@Override
	public void display(MetadataDTO metadata) {
		super.display(metadata);
		String lastRevisionDate = metadata.getTemporalExtentPart().getLastRevisionDate();
		if (!(StringUtil.isEmpty(lastRevisionDate))) {
			try {
				label.setText(lastRevisionDate);
			} catch (IllegalArgumentException e) {
				label.setText("");
			}
		} else {
			label.setText("");
		}
	}

	@Override
	public void flush(MetadataDTO metadata) {
		if (dateBox.getValue() == null) {
			metadata.getTemporalExtentPart().setLastRevisionDate("");
		} else {
			metadata.getTemporalExtentPart().setLastRevisionDate(Iso19115DateUtil.getDateFormater().format(dateBox.getValue()));
		}
	}

	@Override
	public void edit(MetadataDTO metadata) {

		super.edit(metadata);
		String lastRevisionDate = metadata.getTemporalExtentPart().getLastRevisionDate();
		if (!(StringUtil.isEmpty(lastRevisionDate))) {
			try {
				dateBox.setValue(Iso19115DateUtil.getDateFormater().parse(lastRevisionDate));
			} catch (IllegalArgumentException e) {
				dateBox.setValue(null);
			}
		}
	}

}
