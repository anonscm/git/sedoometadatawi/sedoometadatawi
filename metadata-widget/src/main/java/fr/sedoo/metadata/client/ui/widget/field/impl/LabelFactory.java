package fr.sedoo.metadata.client.ui.widget.field.impl;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.user.client.ui.Label;

import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;

public class LabelFactory {

	private static Map<String, String> labels;

	public static Label getLabelByKey(String key) {
		initLabels();
		return new Label(StringUtil.trimToEmpty(labels.get(key)));
	}

	private static void initLabels() {
		if (labels == null) {
			labels = new HashMap<String, String>();
			labels.put(FieldConstant.TITLE, MetadataMessage.INSTANCE.metadataEditingResourceTitle());
			labels.put(FieldConstant.ABSTRACT, MetadataMessage.INSTANCE.metadataEditingResourceAbstract());
			labels.put(FieldConstant.IDENTIFIERS, MetadataMessage.INSTANCE.metadataEditingResourceIdentifiers());
			labels.put(FieldConstant.STATUS, MetadataMessage.INSTANCE.metadataEditingResourceStatus());
			labels.put(FieldConstant.STARTING_DATE, MetadataMessage.INSTANCE.metadataEditingStartDate());
			labels.put(FieldConstant.ENDING_DATE, MetadataMessage.INSTANCE.metadataEditingEndDate());
			labels.put(FieldConstant.UPDATE_RYTHM, MetadataMessage.INSTANCE.metadataEditingResourceUpdateRythm());
			labels.put(FieldConstant.CREATION_DATE, MetadataMessage.INSTANCE.metadataEditingCreationDate());
			labels.put(FieldConstant.LAST_REVISION_DATE, MetadataMessage.INSTANCE.metadataEditingLastModificationDate());
			labels.put(FieldConstant.PUBLICATION_DATE, MetadataMessage.INSTANCE.metadataEditingPublicationDate());
			labels.put(FieldConstant.PUBLIC_ACCESS_LIMITATIONS, MetadataMessage.INSTANCE.metadataEditingPublicAccessLimitations());
			labels.put(FieldConstant.USE_CONDITIONS, MetadataMessage.INSTANCE.metadataEditingUseConditions());
			labels.put(FieldConstant.UUID, MetadataMessage.INSTANCE.metatadaEditingMetadataUuid());
			labels.put(FieldConstant.METADATA_LAST_MODIFICATION_DATE, MetadataMessage.INSTANCE.metatadaEditingMetadataLastModificationDate());
			labels.put(FieldConstant.METADATA_MAIN_LANGUAGE, MetadataMessage.INSTANCE.metadataEditingMetadataLanguage());
			labels.put(FieldConstant.DATA_LANGUAGES, MetadataMessage.INSTANCE.metadataEditingDataLanguages());
			labels.put(FieldConstant.CHARSET, MetadataMessage.INSTANCE.metadataEditingCharset());
			labels.put(FieldConstant.COORDINATE_SYSTEM, MetadataMessage.INSTANCE.metadataEditingCoordinateSystem());
			labels.put(FieldConstant.FORMAT, MetadataMessage.INSTANCE.metadataEditingFormat());
			labels.put(FieldConstant.KEYWORDS, MetadataMessage.INSTANCE.metadataEditingKeywords());
			labels.put(FieldConstant.SNAPSHOTS, MetadataMessage.INSTANCE.metadataEditingSnapshotList());
			labels.put(FieldConstant.INTERNET_LINKS, MetadataMessage.INSTANCE.metadataEditingResourceURL());
			labels.put(FieldConstant.GENEALOGY, MetadataMessage.INSTANCE.metadataEditingGenealogy());
			labels.put(FieldConstant.ALTERNATE_TITLE, MetadataMessage.INSTANCE.metadataEditingResourceAlternateTitle());
			labels.put(FieldConstant.PURPOSE, MetadataMessage.INSTANCE.metadataEditingPurpose());
			labels.put(FieldConstant.LOGO, MetadataMessage.INSTANCE.metadataEditingLogoUrl());
		}
	}

}
