package fr.sedoo.metadata.client.ui.widget.field.impl.contact;

import java.util.List;

import com.google.gwt.user.client.ui.HorizontalPanel;

import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.client.ui.widget.field.api.IsDisplay;
import fr.sedoo.metadata.client.ui.widget.field.api.IsEditor;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.client.ui.widget.table.contact.MetadataContactTable;
import fr.sedoo.metadata.shared.domain.MetadataDTO;

public abstract class AbstractSingleRoleContact extends HorizontalPanel implements IsDisplay, IsEditor {
	protected MetadataContactTable table;

	public AbstractSingleRoleContact() {
		super();
		setWidth(FieldConstant.HUNDRED_PERCENT);
		table = new MetadataContactTable(getSingleRole());
		table.setAskDeletionConfirmation(false);
		ElementUtil.hide(table);
		table.setVisible(false);
		add(table);
	}

	public abstract String getSingleRole();

	@Override
	public void display(MetadataDTO metadata) {
		table.enableDisplayMode();
		ElementUtil.show(table);
		table.hideToolBar();
		table.init(getDatas(metadata));
	}

	public abstract List<? extends HasIdentifier> getDatas(MetadataDTO metadata);

	@Override
	public void reset() {
		table.reset();
	}

	@Override
	public void edit(MetadataDTO metadata) {
		table.showToolBar();
		table.enableEditMode();
		ElementUtil.show(table);
		table.init(getDatas(metadata));
	}

}
