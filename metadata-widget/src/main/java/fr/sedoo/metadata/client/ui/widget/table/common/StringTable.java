package fr.sedoo.metadata.client.ui.widget.table.common;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.cellview.client.Column;

import fr.sedoo.commons.client.widget.table.AbstractListTable;
import fr.sedoo.commons.client.widget.table.WaterMarkCell;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.shared.domain.misc.IdentifiedString;

public class StringTable extends AbstractListTable {

	private String addItemText = "";
	private String watermark = "";

	protected Column<HasIdentifier, String> valueColumn;
	private boolean editColumnInitialized = false;
	private boolean displayColumnInitialized = false;

	@UiConstructor
	public StringTable(String waterMark, String addItemText) {
		super();
		this.addItemText = addItemText;
		// getAddItemText() est appellé lors du super(), il est donc nécessaire
		// de repositionner addItemLabel
		if (addItemLabel != null) {
			addItemLabel.setText(addItemText);
		}
		this.watermark = waterMark;
		localInitColumns();
	}

	@Override
	public void addItem() {
		addEmptyRow();
	}

	public void addEmptyRow() {
		Long maxId = 0L;
		List<IdentifiedString> newValues = new ArrayList<IdentifiedString>();
		Iterator<? extends HasIdentifier> iterator = model.iterator();
		while (iterator.hasNext()) {
			IdentifiedString aux = (IdentifiedString) iterator.next();
			newValues.add(aux);
			if (aux.getId() > maxId) {
				maxId = aux.getId();
			}
		}
		newValues.add(getDefaultIdentifiedString(maxId + 1));
		init(newValues);
	}

	@Override
	public String getAddItemText() {
		return addItemText;
	}

	@Override
	public void presenterDelete(HasIdentifier hasId) {
		String id = hasId.getIdentifier();
		removeRow(id);
	}

	@Override
	public void presenterEdit(HasIdentifier hasId) {
	}

	@Override
	protected void initColumns() {
		// Nothing is done there
	}

	protected void localInitColumns() {
		valueColumn = new Column<HasIdentifier, String>(new WaterMarkCell(getWatermark(), "80", "120")) {
			@Override
			public String getValue(HasIdentifier identifiedString) {
				return ((IdentifiedString) identifiedString).getValue();
			}
		};

		valueColumn.setFieldUpdater(new FieldUpdater<HasIdentifier, String>() {
			@Override
			public void update(int index, HasIdentifier identifiedString, String value) {
				((IdentifiedString) identifiedString).setValue(value);
			}
		});

		// valueColumn.setCellStyleNames("url");
		itemTable.addColumn(valueColumn);
		itemTable.setColumnWidth(valueColumn, 30.0, Unit.PX);
		itemTable.addColumn(deleteColumn);
		itemTable.setColumnWidth(deleteColumn, 30.0, Unit.PX);
		setAddButtonEnabled(true);
	}

	public void reset() {
		List<IdentifiedString> aux = new ArrayList<IdentifiedString>();
		init(aux);
	}

	protected IdentifiedString getDefaultIdentifiedString(Long id) {

		IdentifiedString string = new IdentifiedString();
		string.setValue("");
		string.setId(id);
		return string;
	}

	public void setAddItemText(String addItemText) {
		this.addItemText = addItemText;
		if (addItemLabel != null) {
			addItemLabel.setText(addItemText);
		}
	}

	public String getWatermark() {
		return watermark;
	}

	public void setWatermark(String watermark) {
		this.watermark = watermark;
	}

	protected void initEditColumns() {
		// TODO Auto-generated method stub

	}

	public void enableEditMode() {
		getToolBarPanel().setVisible(true);
		if (editColumnInitialized == false) {
			initEditColumns();
			editColumnInitialized = true;
		}
	}

	public void enableDisplayMode() {
		getToolBarPanel().setVisible(false);
		if (displayColumnInitialized == false) {
			initDisplayColumns();
			displayColumnInitialized = true;
		}
	}

	protected void initDisplayColumns() {
		// TODO Auto-generated method stub

	}

}
