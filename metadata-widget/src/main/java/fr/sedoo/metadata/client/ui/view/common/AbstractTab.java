package fr.sedoo.metadata.client.ui.view.common;

import java.util.ArrayList;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.metadata.client.ui.widget.field.api.HasReset;
import fr.sedoo.metadata.client.ui.widget.field.api.IsDisplay;
import fr.sedoo.metadata.client.ui.widget.field.api.IsEditor;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.shared.domain.MetadataDTO;

public class AbstractTab extends ScrollPanel implements IsDisplay, IsEditor {

	private static final int LEFT_COLUMN = 0;
	private static final int RIGHT_COLUMN = 1;

	protected VerticalPanel mainPanel;

	protected ArrayList<Widget> components = new ArrayList<Widget>();

	FlexTable currentSubPanel;

	private int currentLineIndex;

	public AbstractTab() {
		super();
		mainPanel = new VerticalPanel();
		mainPanel.setStyleName("metadataTabContent");
		mainPanel.setWidth("100%");
		add(mainPanel);
	}

	// protected void selectByValue(ListBox listBox, String value) {
	// if ((value != null) && (value.trim().length() > 0)) {
	// for (int index = 0; index < listBox.getItemCount(); index++) {
	// if (KeyComparator.compare(
	// listBox.getValue(index).toLowerCase(),
	// value.toLowerCase())) {
	// listBox.setSelectedIndex(index);
	// break;
	// }
	// }
	// }

	// }

	@Override
	public void display(MetadataDTO metadata) {
		for (Widget component : components) {
			if (component instanceof IsDisplay) {
				((IsDisplay) component).display(metadata);
			}
		}

	}

	@Override
	public void edit(MetadataDTO metadata) {
		for (Widget component : components) {
			if (component instanceof IsEditor) {
				((IsEditor) component).edit(metadata);
			}
		}

	}

	@Override
	public void flush(MetadataDTO metadata) {
		for (Widget component : components) {
			if (component instanceof IsEditor) {
				((IsEditor) component).flush(metadata);
			}
		}

	}

	@Override
	public void reset() {
		for (Widget component : components) {
			if (component instanceof HasReset) {
				((HasReset) component).reset();
			}
		}
	}

	protected Label addSection(String sectionTitle) {
		currentSubPanel = null;
		Label label = new Label(sectionTitle);
		label.addStyleName(FieldConstant.METADATA_SECTION_STYLENAME);
		mainPanel.add(label);
		return label;

	}

	protected void addComponent(Widget component) {
		components.add(component);
	}

	protected void addRightComponent(Widget component) {
		addComponent(component);
		currentSubPanel.setWidget(currentLineIndex, RIGHT_COLUMN, component);
		currentSubPanel.getFlexCellFormatter().setStyleName(currentLineIndex, RIGHT_COLUMN, FieldConstant.METADATA_RIGHT_COLUMN_STYLENAME);
		currentSubPanel.getFlexCellFormatter().addStyleName(currentLineIndex, RIGHT_COLUMN, FieldConstant.METADATA_ROW_SPACING_STYLENAME);
	}

	protected void addFullLineComponent(Widget component) {
		addComponent(component);
		if (currentSubPanel == null) {
			currentSubPanel = createSubPanel();
			currentLineIndex = 0;
		} else {
			currentLineIndex++;
		}
		currentSubPanel.setWidget(currentLineIndex, LEFT_COLUMN, component);
		currentSubPanel.getFlexCellFormatter().setColSpan(currentLineIndex, LEFT_COLUMN, 2);
		currentSubPanel.getFlexCellFormatter().setStyleName(currentLineIndex, LEFT_COLUMN, FieldConstant.METADATA_FULL_LINE_COLUMN_STYLENAME);
		currentSubPanel.getFlexCellFormatter().addStyleName(currentLineIndex, LEFT_COLUMN, FieldConstant.METADATA_ROW_SPACING_STYLENAME);
	}

	protected void addLeftComponent(Widget component) {
		addComponent(component);
		if (currentSubPanel == null) {
			currentSubPanel = createSubPanel();
			currentLineIndex = 0;
		} else {
			currentLineIndex++;
		}
		currentSubPanel.setWidget(currentLineIndex, LEFT_COLUMN, component);
		currentSubPanel.getFlexCellFormatter().setStyleName(currentLineIndex, LEFT_COLUMN, FieldConstant.METADATA_LEFT_COLUMN_STYLENAME);
		currentSubPanel.getFlexCellFormatter().addStyleName(currentLineIndex, LEFT_COLUMN, FieldConstant.METADATA_ROW_SPACING_STYLENAME);
	}

	private FlexTable createSubPanel() {
		VerticalPanel aux = new VerticalPanel();
		aux.setSpacing(5);
		aux.setWidth(FieldConstant.HUNDRED_PERCENT);
		FlexTable flexTable = new FlexTable();
		flexTable.setWidth(FieldConstant.HUNDRED_PERCENT);
		flexTable.setCellPadding(0);
		flexTable.setCellSpacing(0);
		aux.add(flexTable);
		mainPanel.add(aux);
		return flexTable;
	}

	/**
	 * This method indicates that a tab is selected. It's mainly used to correct
	 * a google map API bug which doesn't display well when created on a non
	 * visible element
	 */
	public void isSelected(String mode) {
	}

}
