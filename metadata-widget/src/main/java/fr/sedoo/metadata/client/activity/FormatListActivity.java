package fr.sedoo.metadata.client.activity;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.callback.OperationCallBack;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.NotificationEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.mvp.activity.AdministrationActivity;
import fr.sedoo.commons.client.widget.DialogBoxTools;
import fr.sedoo.metadata.client.event.MetadataActionEventConstant;
import fr.sedoo.metadata.client.mvp.MetadataClientFactory;
import fr.sedoo.metadata.client.place.FormatListPlace;
import fr.sedoo.metadata.client.service.FormatService;
import fr.sedoo.metadata.client.service.FormatServiceAsync;
import fr.sedoo.metadata.client.ui.view.FormatListView;
import fr.sedoo.metadata.client.ui.view.FormatListView.Presenter;
import fr.sedoo.metadata.shared.domain.dto.FormatDTO;

public abstract class FormatListActivity extends AdministrationActivity implements Presenter {

	private FormatListView formatListView = null;
	public final static FormatServiceAsync FORMAT_SERVICE = GWT.create(FormatService.class);

	public FormatListActivity(FormatListPlace place, AuthenticatedClientFactory clientFactory) {
		super(clientFactory, place);
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {

		if (isValidUser() == false) {
			goToLoginPlace();
			return;
		}
		sendActivityStartEvent();
		setFormatListView(((MetadataClientFactory) clientFactory).getFormatListView());
		getFormatListView().setPresenter(this);
		containerWidget.setWidget(getFormatListView().asWidget());

		final ActionStartEvent startEvent = new ActionStartEvent(CommonMessages.INSTANCE.loading(), MetadataActionEventConstant.FORMAT_LOADING_EVENT, true);
		clientFactory.getEventBus().fireEvent(startEvent);

		FORMAT_SERVICE.findAll(new AsyncCallback<ArrayList<FormatDTO>>() {

			@Override
			public void onSuccess(ArrayList<FormatDTO> formats) {
				getFormatListView().setFormats(formats);
				clientFactory.getEventBus().fireEvent(startEvent.getEndingEvent());
			}

			@Override
			public void onFailure(Throwable caught) {
				DialogBoxTools.modalAlert(CommonMessages.INSTANCE.error(), CommonMessages.INSTANCE.anErrorHasOccurred() + " : " + caught.getMessage());
				clientFactory.getEventBus().fireEvent(startEvent.getEndingEvent());
			}
		});
	}

	@Override
	public void deleteFormat(final Long id) {
		final ActionStartEvent startEvent = new ActionStartEvent(CommonMessages.INSTANCE.deleting(), MetadataActionEventConstant.FORMAT_DELETING_EVENT, true);
		clientFactory.getEventBus().fireEvent(startEvent);
		FORMAT_SERVICE.delete(id, new AsyncCallback<Void>() {

			@Override
			public void onSuccess(Void result) {
				getFormatListView().broadcastFormatDeletion(id, true);
				clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.deletedElement()));
				clientFactory.getEventBus().fireEvent(startEvent.getEndingEvent());
			}

			@Override
			public void onFailure(Throwable caught) {
				getFormatListView().broadcastFormatDeletion(id, false);
				clientFactory.getEventBus().fireEvent(startEvent.getEndingEvent());
			}
		});

	}

	@Override
	public void saveFormat(final FormatDTO format, final OperationCallBack callBack) {
		final boolean newElement = (format.getId() == null);
		final ActionStartEvent startEvent;
		if (newElement) {
			startEvent = new ActionStartEvent(CommonMessages.INSTANCE.saving(), MetadataActionEventConstant.FORMAT_ADDING_EVENT, true);

		} else {
			startEvent = new ActionStartEvent(CommonMessages.INSTANCE.saving(), MetadataActionEventConstant.FORMAT_SAVING_EVENT, true);
		}
		clientFactory.getEventBus().fireEvent(startEvent);
		FORMAT_SERVICE.save(format, new AsyncCallback<Long>() {

			@Override
			public void onSuccess(Long result) {
				format.setId(result);
				if (callBack != null) {
					callBack.postExecution(true, format);
				}
				if (newElement) {
					clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.addedOrModified()));
					clientFactory.getEventBus().fireEvent(startEvent.getEndingEvent());
				} else {
					clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.savedElement()));
					clientFactory.getEventBus().fireEvent(startEvent.getEndingEvent());
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				DialogBoxTools.modalAlert(CommonMessages.INSTANCE.error(), CommonMessages.INSTANCE.anErrorHasOccurred() + " : " + caught.getMessage());
				clientFactory.getEventBus().fireEvent(startEvent.getEndingEvent());
			}
		});
	}

	public FormatListView getFormatListView() {
		return formatListView;
	}

	public void setFormatListView(FormatListView formatListView) {
		this.formatListView = formatListView;
	}
}
