package fr.sedoo.metadata.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.metadata.shared.domain.thesaurus.Thesaurus;
import fr.sedoo.metadata.shared.domain.thesaurus.ThesaurusItem;

@RemoteServiceRelativePath("thesaurus")
public interface ThesaurusService extends RemoteService {

	ArrayList<ThesaurusItem> getItems(String thesaurusId, String language) throws ServiceException;

	ArrayList<Thesaurus> getThesaurus(String language) throws ServiceException;

}
