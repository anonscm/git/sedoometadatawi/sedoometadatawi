package fr.sedoo.metadata.client.ui.view;

import fr.sedoo.commons.client.image.CommonBundle;

public class AbstractSection extends AbstractStyledResizeComposite {

	public AbstractSection() {
		CommonBundle.INSTANCE.css().ensureInjected();
	}
}
