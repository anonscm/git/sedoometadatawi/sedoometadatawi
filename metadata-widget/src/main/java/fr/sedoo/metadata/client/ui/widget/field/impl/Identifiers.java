package fr.sedoo.metadata.client.ui.widget.field.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.user.client.ui.HorizontalPanel;

import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.metadata.client.ui.widget.field.api.IsDisplay;
import fr.sedoo.metadata.client.ui.widget.field.api.IsEditor;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.client.ui.widget.field.impl.identifier.IdentifierComplementaryEditor;
import fr.sedoo.metadata.client.ui.widget.table.identifier.IdentifierTable;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.metadata.shared.domain.dto.IdentifiedResourceIdentifier;

public class Identifiers extends HorizontalPanel implements IsDisplay, IsEditor {
	private IdentifierTable table;
	private List<IdentifierComplementaryEditor> complementaryEditors = new ArrayList<IdentifierComplementaryEditor>();

	public Identifiers() {
		super();
		setWidth(FieldConstant.HUNDRED_PERCENT);

		table = new IdentifierTable();
		table.setAskDeletionConfirmation(false);
		ElementUtil.hide(table);
		table.setVisible(false);
		add(table);
	}

	public Identifiers(List<IdentifierComplementaryEditor> complementaryEditors) {
		this();
		this.complementaryEditors = complementaryEditors;
	}

	@Override
	public void display(MetadataDTO metadata) {
		table.enableDisplayMode();
		table.init(filter(metadata.getIdentificationPart().getResourceIdentifiers()));
		ElementUtil.show(table);
	}

	@Override
	public void reset() {
		table.reset();
	}

	@Override
	public void flush(MetadataDTO metadata) {
		// we clone the list
		List<IdentifiedResourceIdentifier> currentIdentifiers = new ArrayList<IdentifiedResourceIdentifier>();
		currentIdentifiers.addAll((List<IdentifiedResourceIdentifier>) table.getModel());

		Iterator<IdentifierComplementaryEditor> iterator = complementaryEditors.iterator();
		while (iterator.hasNext()) {
			IdentifierComplementaryEditor currentEditor = (IdentifierComplementaryEditor) iterator.next();
			currentIdentifiers = currentEditor.flush(currentIdentifiers);
		}

		List<IdentifiedResourceIdentifier> filteredIdentifiers = new ArrayList<IdentifiedResourceIdentifier>();
		// We filter empty identifiers
		for (IdentifiedResourceIdentifier currentIdentifier : currentIdentifiers) {
			if (!((StringUtil.isEmpty(currentIdentifier.getCode())) && (StringUtil.isEmpty(currentIdentifier.getNameSpace())))) {
				filteredIdentifiers.add(currentIdentifier);
			}
		}
		metadata.getIdentificationPart().setResourceIdentifiers(filteredIdentifiers);
	}

	@Override
	public void edit(MetadataDTO metadata) {
		table.enableEditMode();
		table.init(filter(metadata.getIdentificationPart().getResourceIdentifiers()));
		ElementUtil.show(table);
	}

	private List<IdentifiedResourceIdentifier> filter(List<IdentifiedResourceIdentifier> identifiers) {
		// we clone the list
		List<IdentifiedResourceIdentifier> currentIdentifiers = new ArrayList<IdentifiedResourceIdentifier>();
		currentIdentifiers.addAll(identifiers);
		Iterator<IdentifierComplementaryEditor> iterator = complementaryEditors.iterator();
		while (iterator.hasNext()) {
			IdentifierComplementaryEditor currentEditor = (IdentifierComplementaryEditor) iterator.next();
			currentIdentifiers = currentEditor.filter(currentIdentifiers);
		}
		return currentIdentifiers;
	}
}
